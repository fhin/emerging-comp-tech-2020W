﻿using OptimizerGenerator.ParserEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace OptimizerTests
{
    public static class Utils
    {
        public static Module CreateNormalModule(string moduleName, IEnumerable<string> paramNames, IEnumerable<ParameterType> paramTypes)
        {
            IEnumerable<int> signalParamWidth = Enumerable.Repeat(Constants.DEFAULT_SIGNAL_WIDTH, paramNames.Count());
            IDictionary<string, int> arrayTypeParamDict = new Dictionary<string, int>();
            return CreateModule(moduleName, paramNames, paramTypes, signalParamWidth, arrayTypeParamDict);
        }

        public static Module CreateCustomSignalModule(string moduleName, IEnumerable<string> paramNames, IEnumerable<ParameterType> paramTypes,
            IEnumerable<int> signalParamWidths)
        {
            return CreateModule(moduleName, paramNames, paramTypes, signalParamWidths, new Dictionary<string, int>());
        }

        public static Module CreateMixedModule(string moduleName, IEnumerable<string> paramNames, IEnumerable<ParameterType> paramTypes,
            IEnumerable<int> signalParamWidths, IDictionary<string, int> arrayTypeParamDict)
        {
            return CreateModule(moduleName, paramNames, paramTypes, signalParamWidths, arrayTypeParamDict);
        }

        private static Module CreateModule(string moduleName, IEnumerable<string> paramNames, IEnumerable<ParameterType> paramTypes,
            IEnumerable<int> signalParamWidths, IDictionary<string, int> arrayTypeParamDict)
        {
            Module module = new Module(moduleName);
            IEnumerator<ParameterType> paramTypeEnumerator = paramTypes.GetEnumerator();
            IEnumerator<string> paramNamesEnumerator = paramNames.GetEnumerator();
            if (!signalParamWidths.Any())
            {
                signalParamWidths = Enumerable.Repeat(Constants.DEFAULT_SIGNAL_WIDTH, paramTypes.Count());
            }
            IEnumerator<int> paramWidth = signalParamWidths.GetEnumerator();
            while (paramTypeEnumerator.MoveNext() && paramNamesEnumerator.MoveNext() && paramWidth.MoveNext())
            {
                Parameter param = new Parameter(paramNamesEnumerator.Current)
                {
                    ParameterType = paramTypeEnumerator.Current,
                    Literal = CreateSignalLiteral(paramNamesEnumerator.Current, LiteralValueType.SIGNAL, 0, paramWidth.Current, 0, 0, null,
                    (paramTypeEnumerator.Current == ParameterType.WIRE || paramTypeEnumerator.Current == ParameterType.OUT))
                };
                param.Literal.LiteralType = LiteralType.PARAMETER;
                if (arrayTypeParamDict.ContainsKey(param.Name))
                {
                    param.Literal.IsArrayType = true;
                    param.Literal.Dimension = arrayTypeParamDict[param.Name];
                }
                switch (paramTypeEnumerator.Current)
                {
                    case ParameterType.IN:
                        module.FormalInputParameter.Add(param);
                        break;
                    case ParameterType.OUT:
                        if (param.Literal.IsArrayType)
                        {
                            object zeroObj = 0;
                            param.Literal.Values = new List<object>(Enumerable.Repeat(zeroObj, param.Literal.Dimension).ToList()).ToArray();
                        }
                        else
                        {
                            param.Literal.Values[0] = 0;
                        }
                        module.FormalOutputParameter.Add(param);
                        break;
                    case ParameterType.INOUT:
                        module.FormalInputParameter.Add(param);
                        module.FormalOutputParameter.Add(param);
                        break;
                    case ParameterType.WIRE:
                        param.Literal.LiteralType = LiteralType.VARIABLE; 
                        if (param.Literal.IsArrayType)
                        {
                            object zeroObj = 0;
                            param.Literal.Values = new List<object>(Enumerable.Repeat(zeroObj, param.Literal.Dimension).ToList()).ToArray();
                        }
                        else
                        {
                            param.Literal.Values[0] = 0;
                        }
                        module.AdditionalSignals.Add(param);
                        break;
                    case ParameterType.STATE:
                        param.Literal.LiteralType = LiteralType.VARIABLE;
                        module.AdditionalSignals.Add(param);
                        break;
                }
            }
            module.Body = new Block(BlockType.MODULE);
            return module;
        }

        public static Expression CreateConstantNumericExpression(int value)
        {
            return new Expression(Operand.NONE)
            {
                HasNumericResult = true,
                Literal = new Literal(value.ToString(), LiteralValueType.NUMERIC_CONSTANT, value)
            };
        }
        public static Expression CreateConstantBooleanExpression(bool value)
        {
            return new Expression(Operand.NONE)
            {
                HasNumericResult = true,
                Literal = new Literal(value.ToString(), LiteralValueType.BOOL, value)
            };
        }

        public static Expression CreateExpression(Operand operand, string leftLiteral, string rightLiteral,
            bool isLeftNumeric, bool isRightNumeric)
        {
            if (operand != Operand.NONE)
            {
                Assert.Equal(isLeftNumeric, isRightNumeric);
            }
            Literal lLiteral = new Literal(leftLiteral, LiteralValueType.SIGNAL, null)
            {
                SignalWidth = Constants.DEFAULT_SIGNAL_WIDTH
            };
            Literal rLiteral = new Literal(rightLiteral, LiteralValueType.SIGNAL, null)
            {
                SignalWidth = Constants.DEFAULT_SIGNAL_WIDTH
            };

            Expression lExpr = new Expression(Operand.NONE)
            {
                Literal = lLiteral,
                HasNumericResult = isLeftNumeric,
            };
            Expression rExpr;
            if (string.IsNullOrWhiteSpace(rightLiteral))
            {
                return new Expression(operand)
                {
                    Literal = lLiteral,
                    HasNumericResult = isLeftNumeric
                };
            }
            else
            {
                rExpr = new Expression(Operand.NONE)
                {
                    Literal = rLiteral,
                    HasNumericResult = isRightNumeric
                };
            }
            Expression expr = new Expression(operand)
            {
                LeftExpression = lExpr,
                RightExpression = rExpr
            };
            return expr;
        }

        public static Expression CreateExpression(Operand operand, Expression leftExpr, Expression rightExpr)
        {
            return new Expression(operand)
            {
                LeftExpression = leftExpr,
                RightExpression = rightExpr
            };
        }

        public static Expression CreateExpression(Operand operand, Literal leftLiteral, Literal rightLiteral)
        {
            Expression newExpression = new Expression(operand);
            if (newExpression.RequiresBooleanOperands && (leftLiteral.LiteralValueType != LiteralValueType.BOOL || rightLiteral.LiteralValueType != LiteralValueType.BOOL))
            {
                throw new ArgumentException(Constants.BuildMessage(Constants.ERR_TYPE_MISSMATCH, new object[2] { operand, "BOOLEAN" }));
            }
            else if (!newExpression.RequiresBooleanOperands && (leftLiteral.LiteralValueType == LiteralValueType.BOOL || rightLiteral.LiteralValueType == LiteralValueType.BOOL))
            {
                throw new ArgumentException(Constants.BuildMessage(Constants.ERR_TYPE_MISSMATCH, new object[2] { operand, "NUMERIC" }));
            }

            Expression leftExpr = new Expression(Operand.NONE)
            {
                Literal = leftLiteral,
                HasNumericResult = leftLiteral.LiteralValueType != LiteralValueType.BOOL
            };
            Expression rightExpr = new Expression(Operand.NONE)
            {
                Literal = rightLiteral,
                HasNumericResult = rightLiteral.LiteralValueType != LiteralValueType.BOOL
            };
            newExpression.LeftExpression = leftExpr;
            newExpression.RightExpression = rightExpr;
            return newExpression;
        }

        public static Expression CreateExpression(Literal literal)
        {
            return new Expression(Operand.NONE)
            {
                RequiresBooleanOperands = false,
                HasNumericResult = true,
                Literal = literal
            };
        }

        public static Literal CreateSignalLiteral(string literalName, LiteralValueType lType, int dimension, int signalWidth,
            int startIdx, int endIdx, Expression dimensionEpression, bool initValuesAsZero)
        {
            if (lType != LiteralValueType.RANGE && lType != LiteralValueType.SIGNAL)
            {
                // TODO
                throw new ArgumentException("");
            }

            Literal literal = new Literal(literalName, LiteralValueType.SIGNAL, null);
            literal.SignalWidth = signalWidth > 0 ? signalWidth : Constants.DEFAULT_SIGNAL_WIDTH;
            if (dimension > 0)
            {
                literal.IsArrayType = true;
                literal.Dimension = dimension;
                literal.DimensionExpr = dimensionEpression;
                literal.Values = new object[dimension];
                if (initValuesAsZero)
                {
                    for (int i = 0; i < dimension; i++)
                    {
                        literal.Values[i] = 0;
                    }
                }
                //literal.SignalWidth = signalWidth;
            }
            if (lType == LiteralValueType.RANGE)
            {
                literal.LiteralValueType = LiteralValueType.RANGE;
                literal.StartIdx = startIdx;
                literal.EndIdx = endIdx;
            }
            return literal;
        }
    
        public static Block CreateLoopBlock(string loopVarName, int startVal, int endVal, int stepSize, bool positiveStepSize,
            IEnumerable<Expression> bodyExpressions)
        {
            Literal loopVariable = new Literal(loopVarName, LiteralValueType.NUMERIC_CONSTANT, startVal)
            {
                StartValue = startVal,
                EndValue = endVal,
                StepSize = stepSize,
                PositiveStepSize = positiveStepSize,
                LiteralType = LiteralType.LOOP_VARIABLE
            };
            Block loopBlock = new Block(BlockType.LOOP)
            {
                LoopVariable = loopVariable,
                Expressions = bodyExpressions.ToList()
            };
            return loopBlock;
        }

        public static Block CreateNonNestedConditionBlock(Expression guardExpr, IEnumerable<Expression> ifStatements, 
            IEnumerable<Expression> elseStatements, Expression otherExpr)
        {
            return new Block(BlockType.CONDITION)
            {
                GuardExpr = guardExpr,
                Expressions = ifStatements.ToList(),
                ElseExpressions = elseStatements.ToList(),
                OtherExpr = otherExpr
            };
        }

        public static Expression CreateSkipExpression()
        {
            Expression skipExpr = new Expression(Operand.SKIP);
            return skipExpr;
        }
    }
}
