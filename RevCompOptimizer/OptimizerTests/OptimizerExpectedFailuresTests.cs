using OptimizerGenerator;
using OptimizerGenerator.ParserEntities;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace OptimizerTests
{
    public class OptimizerExpectedFailuresTests
    {
        private readonly QueryParser _userQueryParser;

        public OptimizerExpectedFailuresTests()
        {
            _userQueryParser = new QueryParser();
        }

        [Theory]
        [InlineData(new string[] { "a", "b", "a" }, new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.IN },
            new int[3] { 1, 1, 1 }, "a")]
        [InlineData(new string[] { "a", "b", "a" }, new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.OUT },
            new int[3] { 1, 1, 1 }, "a")]
        [InlineData(new string[] { "a", "b", "a" }, new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.OUT },
            new int[3] { Constants.DEFAULT_SIGNAL_WIDTH, 1, Constants.DEFAULT_SIGNAL_WIDTH }, "a")]
        [InlineData(new string[] { "a", "b", "a" }, new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.WIRE },
            new int[3] { 1, 1, 1 }, "a")]
        [InlineData(new string[] { "a", "b", "a" }, new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.WIRE },
            new int[3] { Constants.DEFAULT_SIGNAL_WIDTH, 1, Constants.DEFAULT_SIGNAL_WIDTH }, "a")]
        [InlineData(new string[] { "a", "b", "a" }, new ParameterType[] { ParameterType.WIRE, ParameterType.IN, ParameterType.WIRE },
            new int[3] { 1, 1, 1 }, "a")]
        [InlineData(new string[] { "a", "b", "a" }, new ParameterType[] { ParameterType.WIRE, ParameterType.IN, ParameterType.WIRE },
            new int[3] { Constants.DEFAULT_SIGNAL_WIDTH, 1, Constants.DEFAULT_SIGNAL_WIDTH }, "a")]
        public void Duplicate_Parameter(IEnumerable<string> paramNames, IEnumerable<ParameterType> paramTypes,
            IEnumerable<int> signalParamWidths, string duplicateParameterName)
        {
            Module module = Utils.CreateCustomSignalModule("adder", paramNames, paramTypes, signalParamWidths);
            _userQueryParser.ParseQuery(module.ToString());
            Assert.False(_userQueryParser.IsValidQuery);
            Assert.Single(_userQueryParser.Errors);
            string expectedErrMsg = Constants.BuildMessage(Constants.ERR_DUPLICATE_PARAMETER, duplicateParameterName);
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }

        // TODO: Could refactor tests to be exhaustive by testing for all possible valid operations

        [Theory]
        [InlineData(new string[] { "a", "b", "c" }, new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.OUT })]
        public void Negating_NonBooleanExpression(IEnumerable<string> paramNames, IEnumerable<ParameterType> paramTypes)
        {
            Module module = Utils.CreateNormalModule("adder", paramNames, paramTypes);
            Expression leftChildExpr = Utils.CreateExpression(Operand.ADD, "a", "b", true, true);
            Expression negationExpr = Utils.CreateExpression(Operand.NEG, leftChildExpr, null);
            Expression headExpr = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(Operand.NONE, "c", null, true, true), negationExpr);
            module.Body.Expressions.Add(headExpr);
            _userQueryParser.ParseQuery(module.ToString());

            Assert.False(_userQueryParser.IsValidQuery);
            Assert.Single(_userQueryParser.Errors);
            string expectedErrMsg = Constants.BuildMessage(Constants.ERR_TYPE_MISSMATCH, new object[2] { Operand.NEG, "BOOLEAN" });
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }

        [Theory]
        [InlineData(new string[] { "a", "b", "c" }, new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.OUT })]
        public void BitWiseNegating_BooleanExpression(IEnumerable<string> paramNames, IEnumerable<ParameterType> paramTypes)
        {
            Module module = Utils.CreateNormalModule("adder", paramNames, paramTypes);
            Expression leftChildExpr = Utils.CreateExpression(Operand.GREATER, "a", "b", true, true);
            Expression negationExpr = Utils.CreateExpression(Operand.BIT_NEG, leftChildExpr, null);
            Expression headExpr = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(Operand.NONE, "c", null, true, true), negationExpr);
            module.Body.Expressions.Add(headExpr);
            _userQueryParser.ParseQuery(module.ToString());

            Assert.False(_userQueryParser.IsValidQuery);
            Assert.Single(_userQueryParser.Errors);
            string expectedErrMsg = Constants.BuildMessage(Constants.ERR_TYPE_MISSMATCH, new object[2] { Operand.BIT_NEG, "NUMERIC" });
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }

        [Theory]
        [InlineData(new string[] { "a", "b", "c" }, new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.OUT }, Operand.SHIFT_LEFT)]
        [InlineData(new string[] { "a", "b", "c" }, new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.OUT }, Operand.SHIFT_RIGHT)]
        public void Shifting_NonNumericExpression(IEnumerable<string> paramNames, IEnumerable<ParameterType> paramTypes,
            Operand shiftOperand)
        {
            Module module = Utils.CreateNormalModule("adder", paramNames, paramTypes);
            Expression faultyExpr = Utils.CreateExpression(Operand.GREATER, "a", "b", true, true);
            Expression shiftExpr = Utils.CreateExpression(shiftOperand, faultyExpr, Utils.CreateConstantNumericExpression(2));
            Expression headExpr = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(Operand.NONE, "c", null, true, true), shiftExpr);
            module.Body.Expressions.Add(headExpr);
            _userQueryParser.ParseQuery(module.ToString());

            Assert.False(_userQueryParser.IsValidQuery);
            Assert.Single(_userQueryParser.Errors);
            string expectedErrMsg = Constants.BuildMessage(Constants.ERR_TYPE_MISSMATCH, new object[2] { shiftOperand, "NUMERIC" });
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }

        [Theory]
        [InlineData(new string[] { "a", "b", "c" }, new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.OUT }, Operand.ADD)]
        [InlineData(new string[] { "a", "b", "c" }, new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.OUT }, Operand.SUB)]
        [InlineData(new string[] { "a", "b", "c" }, new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.OUT }, Operand.MUL)]
        [InlineData(new string[] { "a", "b", "c" }, new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.OUT }, Operand.DIV)]
        [InlineData(new string[] { "a", "b", "c" }, new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.OUT }, Operand.MOD)]
        [InlineData(new string[] { "a", "b", "c" }, new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.OUT }, Operand.BIT_AND)]
        [InlineData(new string[] { "a", "b", "c" }, new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.OUT }, Operand.BIT_OR)]
        public void TypeMissmatch_ArithmeticOperand(IEnumerable<string> paramNames, IEnumerable<ParameterType> paramTypes, Operand arithmeticOperand)
        {
            Module module = Utils.CreateNormalModule("adder", paramNames, paramTypes);
            Expression faultyExpr = Utils.CreateExpression(Operand.GREATER, "a", "b", true, true);
            Expression arithExpr = Utils.CreateExpression(arithmeticOperand, faultyExpr, Utils.CreateExpression(Operand.NONE, "2", null, true, true));
            Expression headExpr = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(Operand.NONE, "c", null, true, true), arithExpr);
            module.Body.Expressions.Add(headExpr);
            _userQueryParser.ParseQuery(module.ToString());

            Assert.False(_userQueryParser.IsValidQuery);
            Assert.Single(_userQueryParser.Errors);
            string expectedErrMsg = Constants.BuildMessage(Constants.ERR_TYPE_MISSMATCH, new object[2] { arithmeticOperand, "NUMERIC" });
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }

        [Theory]
        [InlineData(new string[] { "a", "b", "c", "d", "e" },
            new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.IN, ParameterType.IN, ParameterType.OUT }, Operand.AND)]
        [InlineData(new string[] { "a", "b", "c", "d", "e" },
            new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.IN, ParameterType.IN, ParameterType.OUT }, Operand.OR)]
        public void TypeMissmatch_BooleanOperand(IEnumerable<string> paramNames, IEnumerable<ParameterType> paramTypes, Operand booleanOperand)
        {
            Module module = Utils.CreateNormalModule("adder", paramNames, paramTypes);
            Expression faultyExpr = Utils.CreateExpression(Operand.ADD, "a", "b", true, true);
            Expression booleanChildExpr = Utils.CreateExpression(booleanOperand, "c", "d", false, false);
            Expression booleanExpr = Utils.CreateExpression(booleanOperand, faultyExpr, booleanChildExpr);
            Expression headExpr = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(Operand.NONE, "e", null, true, true), booleanExpr);
            module.Body.Expressions.Add(headExpr);
            _userQueryParser.ParseQuery(module.ToString());

            Assert.False(_userQueryParser.IsValidQuery);
            Assert.Single(_userQueryParser.Errors);
            string expectedErrMsg = Constants.BuildMessage(Constants.ERR_TYPE_MISSMATCH, new object[2] { booleanOperand, "BOOLEAN" });
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }

        [Theory]
        [InlineData(new string[] { "a", "b", "c", "d", "e" },
            new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.IN, ParameterType.IN, ParameterType.OUT },
            Operand.GREATER)]
        [InlineData(new string[] { "a", "b", "c", "d", "e" },
            new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.IN, ParameterType.IN, ParameterType.OUT },
            Operand.GREATER_EQ)]
        [InlineData(new string[] { "a", "b", "c", "d", "e" },
            new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.IN, ParameterType.IN, ParameterType.OUT },
            Operand.LESS)]
        [InlineData(new string[] { "a", "b", "c", "d", "e" },
            new ParameterType[] { ParameterType.IN, ParameterType.IN, ParameterType.IN, ParameterType.IN, ParameterType.OUT },
            Operand.LESS_EQ)]
        public void TypeMissmatch_RelationalOperand(IEnumerable<string> paramNames, IEnumerable<ParameterType> paramTypes, Operand relationalOperand)
        {
            Module module = Utils.CreateNormalModule("adder", paramNames, paramTypes);
            Expression arithmeticExpr = Utils.CreateExpression(Operand.ADD, "a", "b", true, true);
            Expression booleanExpr = Utils.CreateExpression(Operand.GREATER, "c", "d", false, false);
            Expression relationalExpr = Utils.CreateExpression(relationalOperand, arithmeticExpr, booleanExpr);
            Expression headExpr = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(Operand.NONE, "e", null, true, true), relationalExpr);
            module.Body.Expressions.Add(headExpr);
            _userQueryParser.ParseQuery(module.ToString());

            Assert.False(_userQueryParser.IsValidQuery);
            Assert.Single(_userQueryParser.Errors);
            string expectedErrMsg = Constants.BuildMessage(Constants.ERR_TYPE_MISSMATCH, new object[2] { relationalOperand, "NUMERIC" });
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }
    
        [Theory]
        [InlineData(Operand.BIT_NEG, false)]
        [InlineData(Operand.NEG, true)]
        public void TypeMissmatch_UnaryOperand(Operand unaryOperand, bool requiresBooleanOperands)
        {
            string moduleName = "adder";
            IList<string> paramNames = new List<string>(2) { "a", "b", "c" };
            IList<ParameterType> parameterTypes = new List<ParameterType>(1) { ParameterType.IN, ParameterType.IN };
            Module module = Utils.CreateNormalModule(moduleName, paramNames, parameterTypes);

            Expression booleanExpr = Utils.CreateExpression(Operand.GREATER, "2", "4", true, true);
            Expression numericExpr = Utils.CreateExpression(Operand.ADD, "2", "4", true, true);
            Expression faultyExpr = Utils.CreateExpression(unaryOperand, requiresBooleanOperands ? numericExpr : booleanExpr , null);
            Block ifBlock = Utils.CreateNonNestedConditionBlock(faultyExpr, new List<Expression>(1) { Utils.CreateSkipExpression() }, new List<Expression>(1) { Utils.CreateSkipExpression() }, faultyExpr);
            module.Body.Expressions.Add(null);
            module.Body.InnerBlocks.Add(ifBlock);

            _userQueryParser.ParseQuery(module.ToString());
            Assert.False(_userQueryParser.IsValidQuery);
            Assert.Equal(2, _userQueryParser.Errors.Count());

            string expectedErrMsg = Constants.BuildMessage(Constants.ERR_TYPE_MISSMATCH, new object[2] { unaryOperand, faultyExpr.RequiresBooleanOperands ? "BOOLEAN" : "NUMERIC"});
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }

        [Fact]
        public void InvalidIndexForSignalParameter()
        {
            int faultyIdx = Constants.DEFAULT_SIGNAL_WIDTH;
            Literal faultyLiteral = Utils.CreateSignalLiteral("a", LiteralValueType.RANGE, 0, Constants.DEFAULT_SIGNAL_WIDTH, faultyIdx, faultyIdx, null, false);
            Expression faultyExpression = Utils.CreateExpression(Operand.GREATER, faultyLiteral, faultyLiteral);
            Expression assignExpr = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(Operand.NONE, "b", null, true, true), faultyExpression);
            Module faultyModule = Utils.CreateNormalModule("adder", new List<string>() { "a", "b" }, new List<ParameterType>() { ParameterType.IN, ParameterType.OUT });
            faultyModule.Body.Expressions.Add(assignExpr);
            _userQueryParser.ParseQuery(faultyModule.ToString());

            Assert.False(_userQueryParser.IsValidQuery);
            Assert.NotEmpty(_userQueryParser.Errors);
            string expectedErrMsg = Constants.BuildMessage(Constants.ERR_IDX_OUT_OF_RANGE, new object[2] { faultyIdx, Constants.DEFAULT_SIGNAL_WIDTH });
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }

        [Theory]
        [InlineData(0, Constants.DEFAULT_SIGNAL_WIDTH)]
        [InlineData(0, Constants.DEFAULT_SIGNAL_WIDTH + 1)]
        public void InvalidRangeEndForSignalParameter(int startIdx, int endIdx)
        {
            Literal faultyLiteral = Utils.CreateSignalLiteral("a", LiteralValueType.RANGE, 0, Constants.DEFAULT_SIGNAL_WIDTH, startIdx, endIdx, null, false);
            Expression faultyExpression = Utils.CreateExpression(Operand.GREATER, faultyLiteral, faultyLiteral);
            Expression assignExpr = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(Operand.NONE, "b", null, true, true), faultyExpression);
            Module faultyModule = Utils.CreateNormalModule("adder", new List<string>() { "a", "b" }, new List<ParameterType>() { ParameterType.IN, ParameterType.OUT });
            faultyModule.Body.Expressions.Add(assignExpr);
            _userQueryParser.ParseQuery(faultyModule.ToString());

            Assert.False(_userQueryParser.IsValidQuery);
            Assert.NotEmpty(_userQueryParser.Errors);
            string expectedErrMsg = Constants.BuildMessage(Constants.ERR_RANGE_OUT_OF_RANGE, new object[3] { startIdx, endIdx, Constants.DEFAULT_SIGNAL_WIDTH});
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }

        [Fact]
        public void RangeEndSmallerThanStartForSignalParameter()
        {
            int startIdx = Constants.DEFAULT_SIGNAL_WIDTH / 2;
            int endIdx = 0;
            Literal faultyLiteral = Utils.CreateSignalLiteral("a", LiteralValueType.RANGE, 0, Constants.DEFAULT_SIGNAL_WIDTH, startIdx, endIdx, null, false);
            Expression faultyExpression = Utils.CreateExpression(Operand.GREATER, faultyLiteral, faultyLiteral);
            Expression assignExpr = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(Operand.NONE, "b", null, true, true), faultyExpression);
            Module faultyModule = Utils.CreateNormalModule("adder", new List<string>() { "a", "b" }, new List<ParameterType>() { ParameterType.IN, ParameterType.OUT });
            faultyModule.Body.Expressions.Add(assignExpr);
            _userQueryParser.ParseQuery(faultyModule.ToString());

            Assert.False(_userQueryParser.IsValidQuery);
            Assert.NotEmpty(_userQueryParser.Errors);
            string expectedErrMsg = Constants.BuildMessage(Constants.ERR_RANGE_INVALID, new object[2] { startIdx, endIdx });
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }

        [Theory]
        [InlineData(3, 3)]
        [InlineData(3, 4)]
        public void ArraySignalInvalidDimension(int expectedDimension, int actualDimension)
        {
            Expression dimensionExpr = Utils.CreateConstantNumericExpression(actualDimension);
            Literal faultyLiteral = Utils.CreateSignalLiteral("a", LiteralValueType.SIGNAL, expectedDimension, Constants.DEFAULT_SIGNAL_WIDTH, 0, 0, dimensionExpr, false);
            Expression faultyExpression = Utils.CreateExpression(Operand.GREATER, faultyLiteral, faultyLiteral);
            Expression assignExpr = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(Operand.NONE, "b", null, true, true), faultyExpression);
            Module faultyModule = Utils.CreateMixedModule("adder", new List<string>() { "a", "b" }, new List<ParameterType>() { ParameterType.IN, ParameterType.OUT },
                new List<int>(), new Dictionary<string, int>() { { "a", expectedDimension } });
            faultyModule.Body.Expressions.Add(assignExpr);
            _userQueryParser.ParseQuery(faultyModule.ToString());

            Assert.False(_userQueryParser.IsValidQuery);
            Assert.NotEmpty(_userQueryParser.Errors);
            string expectedErrMsg = Constants.BuildMessage(Constants.ERR_ARRAY_IDX_OUT_OF_RANGE, new object[2] { actualDimension, expectedDimension });
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }

        [Theory]
        [InlineData(ParameterType.IN)]
        [InlineData(ParameterType.OUT)]
        [InlineData(ParameterType.WIRE)]
        [InlineData(ParameterType.STATE)]
        public void ArraySignalNegativeIndexAccess(ParameterType paramType)
        {
            int dimension = 2;
            string moduleName = "adder";
            IList<string> paramNames = new List<string>(1) { "a" };
            IList<ParameterType> parameterTypes = new List<ParameterType>(1) { paramType };
            IList<int> signalWidths = new List<int>(1) { Constants.DEFAULT_SIGNAL_WIDTH };
            IDictionary<string, int> dimensionDict = new Dictionary<string, int>(1) { { paramNames[0], dimension } };
            Module module = Utils.CreateMixedModule(moduleName, paramNames, parameterTypes, signalWidths, dimensionDict);

            Expression dimExpr = Utils.CreateExpression(Operand.SUB, "2", "4", true, true);
            Literal arrayLiteral = Utils.CreateSignalLiteral(paramNames[0], LiteralValueType.SIGNAL, dimension, signalWidths[0], 0, 0, dimExpr, paramType != ParameterType.STATE);
            Expression addExpr = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(arrayLiteral), Utils.CreateConstantNumericExpression(0));
            module.Body.Expressions.Add(addExpr);

            _userQueryParser.ParseQuery(module.ToString());
            Assert.False(_userQueryParser.IsValidQuery);
            Assert.Single(_userQueryParser.Errors);

            string expectedErrorMsg = Constants.ERR_ARRAY_DIM_NOT_INT;
            Assert.Equal(expectedErrorMsg, _userQueryParser.Errors.First().ErrorMsg);
        }

        [Theory]
        [InlineData(ParameterType.IN)]
        [InlineData(ParameterType.OUT)]
        [InlineData(ParameterType.WIRE)]
        [InlineData(ParameterType.STATE)]
        public void ArraySignalNonNumericIndexAccess(ParameterType paramType)
        {
            int dimension = 2;
            string moduleName = "adder";
            IList<string> paramNames = new List<string>(1) { "a" };
            IList<ParameterType> parameterTypes = new List<ParameterType>(1) { paramType };
            IList<int> signalWidths = new List<int>(1) { Constants.DEFAULT_SIGNAL_WIDTH };
            IDictionary<string, int> dimensionDict = new Dictionary<string, int>(1) { { paramNames[0], dimension } };
            Module module = Utils.CreateMixedModule(moduleName, paramNames, parameterTypes, signalWidths, dimensionDict);

            Expression dimExpr = Utils.CreateExpression(Operand.GREATER, "2", "4", true, true);
            Literal arrayLiteral = Utils.CreateSignalLiteral(paramNames[0], LiteralValueType.SIGNAL, dimension, signalWidths[0], 0, 0, dimExpr, paramType != ParameterType.STATE);
            Expression addExpr = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(arrayLiteral), Utils.CreateConstantNumericExpression(0));
            module.Body.Expressions.Add(addExpr);

            _userQueryParser.ParseQuery(module.ToString());
            Assert.False(_userQueryParser.IsValidQuery);
            Assert.Single(_userQueryParser.Errors);

            string expectedErrorMsg = Constants.ERR_ARRAY_DIM_NOT_INT;
            Assert.Equal(expectedErrorMsg, _userQueryParser.Errors.First().ErrorMsg);
        }

        [Theory]
        [InlineData(3, 0, Constants.DEFAULT_SIGNAL_WIDTH)]
        [InlineData(3, 0, Constants.DEFAULT_SIGNAL_WIDTH+1)]
        public void ArraySignalInvalidRange(int expectedDimension, int startIdx, int endIdx)
        {
            Expression dimensionExpr = Utils.CreateConstantNumericExpression(expectedDimension-1);
            Literal faultyLiteral = Utils.CreateSignalLiteral("a", LiteralValueType.RANGE, expectedDimension, Constants.DEFAULT_SIGNAL_WIDTH, startIdx, endIdx, dimensionExpr, false);
            Expression faultyExpression = Utils.CreateExpression(Operand.GREATER, faultyLiteral, faultyLiteral);
            Expression assignExpr = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(Operand.NONE, "b", null, true, true), faultyExpression);
            Module faultyModule = Utils.CreateMixedModule("adder", new List<string>() { "a", "b" }, new List<ParameterType>() { ParameterType.IN, ParameterType.OUT },
                new List<int>(), new Dictionary<string, int>() { { "a", expectedDimension } });
            faultyModule.Body.Expressions.Add(assignExpr);
            _userQueryParser.ParseQuery(faultyModule.ToString());

            Assert.False(_userQueryParser.IsValidQuery);
            Assert.NotEmpty(_userQueryParser.Errors);
            string expectedErrMsg = Constants.BuildMessage(Constants.ERR_RANGE_OUT_OF_RANGE, new object[3] { startIdx, endIdx, Constants.DEFAULT_SIGNAL_WIDTH });
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }

        [Theory]
        [InlineData(3, Constants.DEFAULT_SIGNAL_WIDTH)]
        [InlineData(3, Constants.DEFAULT_SIGNAL_WIDTH+1)]
        public void ArraySignalIndexOutOfRange(int expectedDimension, int index)
        {
            Expression dimensionExpr = Utils.CreateConstantNumericExpression(expectedDimension - 1);
            Literal faultyLiteral = Utils.CreateSignalLiteral("a", LiteralValueType.RANGE, expectedDimension, Constants.DEFAULT_SIGNAL_WIDTH, index, index, dimensionExpr, false);
            Expression faultyExpression = Utils.CreateExpression(Operand.GREATER, faultyLiteral, faultyLiteral);
            Expression assignExpr = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(Operand.NONE, "b", null, true, true), faultyExpression);
            Module faultyModule = Utils.CreateMixedModule("adder", new List<string>() { "a", "b" }, new List<ParameterType>() { ParameterType.IN, ParameterType.OUT },
                new List<int>(), new Dictionary<string, int>() { { "a", expectedDimension } });
            faultyModule.Body.Expressions.Add(assignExpr);
            _userQueryParser.ParseQuery(faultyModule.ToString());

            Assert.False(_userQueryParser.IsValidQuery);
            Assert.NotEmpty(_userQueryParser.Errors);
            string expectedErrMsg = Constants.BuildMessage(Constants.ERR_IDX_OUT_OF_RANGE, new object[2] { index, Constants.DEFAULT_SIGNAL_WIDTH });
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }
    
        [Theory]
        [InlineData(Operand.ADD_EQ)]
        [InlineData(Operand.SUB_EQ)]
        [InlineData(Operand.XOR_EQ)]
        public void NonNumericAssignStatment(Operand assignOp)
        {
            Literal lLiteral = Utils.CreateSignalLiteral("a", LiteralValueType.SIGNAL, 0, Constants.DEFAULT_SIGNAL_WIDTH, 0, 0, null, false);
            Expression faultyExpression = Utils.CreateExpression(Operand.GREATER, lLiteral, lLiteral);
            Expression assignExpr = Utils.CreateExpression(assignOp, Utils.CreateExpression(Operand.NONE, "b", null, true, true), faultyExpression);
            Module faultyModule = Utils.CreateNormalModule("adder", new List<string>() { "a", "b" }, new List<ParameterType>() { ParameterType.IN, ParameterType.OUT });
            faultyModule.Body.Expressions.Add(assignExpr);
            _userQueryParser.ParseQuery(faultyModule.ToString());

            Assert.False(_userQueryParser.IsValidQuery);
            Assert.NotEmpty(_userQueryParser.Errors);
            string expectedErrMsg = Constants.BuildMessage(Constants.ERR_TYPE_MISSMATCH, new object[2] { assignOp, "NUMERIC" });
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }

        /*
        [Theory]
        [InlineData(Operand.INCR_EQ)]
        [InlineData(Operand.DECR_EQ)]
        [InlineData(Operand.NEG_UNARY_EQ)]
        public void NonNumericUnaryStatement(Operand unaryOp)
        {
            Literal lLiteral = Utils.CreateSignalLiteral("a", LiteralValueType.SIGNAL, 0, Constants.DEFAULT_SIGNAL_WIDTH, 0, 0, null);
            Expression faultyExpression = Utils.CreateExpression(Operand.GREATER, lLiteral, lLiteral);
            Expression assignExpr = Utils.CreateExpression(unaryOp, Utils.CreateExpression(Operand.NONE, "b", null, true, true), faultyExpression);
            Module faultyModule = Utils.CreateNormalModule("adder", new List<string>() { "a", "b" }, new List<ParameterType>() { ParameterType.IN, ParameterType.OUT });
            faultymodule.Body.Expressions.Add(assignExpr);
            _userQueryParser.ParseQuery(faultyModule.ToString());

            Assert.False(_userQueryParser.IsValidQuery);
            Assert.NotEmpty(_userQueryParser.Errors);
            string expectedErrMsg = Constants.BuildMessage(Constants.ERR_TYPE_MISSMATCH, new object[2] { unaryOp, "NUMERIC" });
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }
        */
    
        // TODO: Tests for loop blocks
        // TODO: Tests for if blocks
        // TODO: Tests for module blocks
        // TOOD: Tests for correct expressions creation
    
        [Fact]
        public void UsingUndefinedLiteralBinaryExpression()
        {
            Literal literal = Utils.CreateSignalLiteral("a", LiteralValueType.SIGNAL, 0, Constants.DEFAULT_SIGNAL_WIDTH, 0, 0, null, false);
            Literal faultyLiteral = Utils.CreateSignalLiteral("c", LiteralValueType.SIGNAL, 0, Constants.DEFAULT_SIGNAL_WIDTH, 0, 0, null, false);
            Expression faultyExpression = Utils.CreateExpression(Operand.GREATER, faultyLiteral, literal);
            Expression assignExpr = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(Operand.NONE, "b", null, true, true), faultyExpression);
            Module faultyModule = Utils.CreateNormalModule("adder", new List<string>() { "a", "b" }, new List<ParameterType>() { ParameterType.IN, ParameterType.OUT });
            faultyModule.Body.Expressions.Add(assignExpr);
            _userQueryParser.ParseQuery(faultyModule.ToString());

            Assert.False(_userQueryParser.IsValidQuery);
            Assert.NotEmpty(_userQueryParser.Errors);
            string expectedErrMsg = Constants.BuildMessage(Constants.ERR_ITEM_NOT_IN_SCOPE, new object[1] { "c" });
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }

        [Fact]
        public void UsingUndefinedLiteralUnaryExpression()
        {
            Literal literal = Utils.CreateSignalLiteral("a", LiteralValueType.SIGNAL, 0, Constants.DEFAULT_SIGNAL_WIDTH, 0, 0, null, false);
            Literal faultyLiteral = Utils.CreateSignalLiteral("c", LiteralValueType.SIGNAL, 0, Constants.DEFAULT_SIGNAL_WIDTH, 0, 0, null, false);
            Expression faultyExpression = Utils.CreateExpression(Operand.GREATER, faultyLiteral, literal);
            Expression assignExpr = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(Operand.NONE, "b", null, true, true), faultyExpression);
            Module faultyModule = Utils.CreateNormalModule("adder", new List<string>() { "a", "b" }, new List<ParameterType>() { ParameterType.IN, ParameterType.OUT });
            faultyModule.Body.Expressions.Add(assignExpr);
            _userQueryParser.ParseQuery(faultyModule.ToString());

            Assert.False(_userQueryParser.IsValidQuery);
            Assert.NotEmpty(_userQueryParser.Errors);
            string expectedErrMsg = Constants.BuildMessage(Constants.ERR_ITEM_NOT_IN_SCOPE, new object[1] { "c" });
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }

        /*
        [Fact]
        public void NonBooleanConditionExpr()
        {
            Literal lLiteral = Utils.CreateSignalLiteral("a", LiteralValueType.SIGNAL, 0, Constants.DEFAULT_SIGNAL_WIDTH, 0, 0, null);
            Expression faultyExpression = Utils.CreateExpression(Operand.ADD, lLiteral, lLiteral);
            Block ifBlock = Utils.CreateNonNestedConditionBlock(faultyExpression, new List<Expression>(1) { faultyExpression }, new List<Expression>(), faultyExpression);
            Module faultyModule = Utils.CreateNormalModule("adder", new List<string>() { "a", "b" }, new List<ParameterType>() { ParameterType.IN, ParameterType.OUT });
            faultyModule.Body.Expressions.Add(null);
            faultyModule.Body.InnerBlocks.Add(ifBlock);
            _userQueryParser.ParseQuery(faultyModule.ToString());

            Assert.False(_userQueryParser.IsValidQuery);
            Assert.NotEmpty(_userQueryParser.Errors);
            Assert.Equal(Constants.ERR_IF_CONDITION_NOT_BOOL, _userQueryParser.Errors.First().ErrorMsg);
        }
        */

        [Theory]
        [InlineData(10, 0)]
        public void InvalidLoopRangePositiveStepSize(int startVal, int endVal)
        {
            Literal lLiteral = Utils.CreateSignalLiteral("a", LiteralValueType.SIGNAL, 0, Constants.DEFAULT_SIGNAL_WIDTH, 0, 0, null, false);
            Expression addExpr = Utils.CreateExpression(Operand.ADD, lLiteral, lLiteral);
            Expression loopBodyExpression = Utils.CreateExpression(Operand.INCR_EQ, Utils.CreateExpression(Operand.NONE, "b", null, true, false), addExpr);

            Block loopBlock = Utils.CreateLoopBlock("i", startVal, endVal, 1, true, new List<Expression>(1) { loopBodyExpression });
            Module faultyModule = Utils.CreateNormalModule("adder", new List<string>() { "a", "b" }, new List<ParameterType>() { ParameterType.IN, ParameterType.OUT });
            faultyModule.Body.Expressions.Add(null);
            faultyModule.Body.InnerBlocks.Add(loopBlock);
            _userQueryParser.ParseQuery(faultyModule.ToString());

            Assert.False(_userQueryParser.IsValidQuery);
            Assert.NotEmpty(_userQueryParser.Errors);
            string expectedErrMsg = Constants.BuildMessage(Constants.ERR_LOOP_RANGE_INVALID, new object[2] { startVal, endVal });
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }

        [Theory]
        [InlineData(0, 10)]
        public void InvalidLoopRangeNegativeStepSize(int startVal, int endVal)
        {
            Literal lLiteral = Utils.CreateSignalLiteral("a", LiteralValueType.SIGNAL, 0, Constants.DEFAULT_SIGNAL_WIDTH, 0, 0, null, false);
            Expression addExpr = Utils.CreateExpression(Operand.ADD, lLiteral, lLiteral);
            Expression loopBodyExpression = Utils.CreateExpression(Operand.INCR_EQ, Utils.CreateExpression(Operand.NONE, "b", null, true, false), addExpr);

            Block loopBlock = Utils.CreateLoopBlock("i", startVal, endVal, 1, false, new List<Expression>(1) { loopBodyExpression });
            Module faultyModule = Utils.CreateNormalModule("adder", new List<string>() { "a", "b" }, new List<ParameterType>() { ParameterType.IN, ParameterType.OUT });
            faultyModule.Body.Expressions.Add(null);
            faultyModule.Body.InnerBlocks.Add(loopBlock);
            _userQueryParser.ParseQuery(faultyModule.ToString());

            Assert.False(_userQueryParser.IsValidQuery);
            Assert.NotEmpty(_userQueryParser.Errors);
            string expectedErrMsg = Constants.BuildMessage(Constants.ERR_LOOP_RANGE_INVALID, new object[2] { startVal, endVal });
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }

        [Fact]
        public void InvalidStepSize()
        {
            Literal lLiteral = Utils.CreateSignalLiteral("a", LiteralValueType.SIGNAL, 0, Constants.DEFAULT_SIGNAL_WIDTH, 0, 0, null, false);
            Expression addExpr = Utils.CreateExpression(Operand.ADD, lLiteral, lLiteral);
            Expression loopBodyExpression = Utils.CreateExpression(Operand.INCR_EQ, Utils.CreateExpression(Operand.NONE, "b", null, true, false), addExpr);

            Block loopBlock = Utils.CreateLoopBlock("i", 0, 10, 0, false, new List<Expression>(1) { loopBodyExpression });
            Module faultyModule = Utils.CreateNormalModule("adder", new List<string>() { "a", "b" }, new List<ParameterType>() { ParameterType.IN, ParameterType.OUT });
            faultyModule.Body.Expressions.Add(null);
            faultyModule.Body.InnerBlocks.Add(loopBlock);
            _userQueryParser.ParseQuery(faultyModule.ToString());

            Assert.False(_userQueryParser.IsValidQuery);
            Assert.NotEmpty(_userQueryParser.Errors);
            string expectedErrMsg = Constants.ERR_STEP_SIZE_ZERO;
            Assert.Equal(expectedErrMsg, _userQueryParser.Errors.First().ErrorMsg);
        }
    }
}
