﻿using OptimizerGenerator;
using OptimizerGenerator.ParserEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Xunit;

namespace OptimizerTests
{
    public class OptimizerExpectedBehaviourTests
    {
        private readonly QueryParser _userQueryParser;

        public OptimizerExpectedBehaviourTests()
        {
            _userQueryParser = new QueryParser();
        }

        [Fact]
        public void ModuleWithNoParameters()
        {
            Module module = Utils.CreateNormalModule("adder", new List<string>(0), new List<ParameterType>(0));
            module.Body.Expressions.Add(Utils.CreateSkipExpression());
            _userQueryParser.ParseQuery(module.ToString());

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();
            Assert.Empty(actualModule.FormalInputParameter);
            Assert.Empty(actualModule.FormalOutputParameter);
            Assert.Empty(actualModule.AdditionalSignals);
            Assert.Equal(module,actualModule);
        }

        [Theory]
        [InlineData(new string[] { "a", "b" }, new ParameterType[2] { ParameterType.IN, ParameterType.IN },
            new int[2] { Constants.DEFAULT_SIGNAL_WIDTH, Constants.DEFAULT_SIGNAL_WIDTH })]
        public void ModuleWithOnlyInputParametersDefaultSignalWidth(IEnumerable<string> paramNames, IEnumerable<ParameterType> paramTypes,
            IEnumerable<int> signalParamWidths)
        {
            Module module = Utils.CreateCustomSignalModule("adder", paramNames, paramTypes, signalParamWidths);
            module.Body.Expressions.Add(Utils.CreateSkipExpression());
            _userQueryParser.ParseQuery(module.ToString());

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();

            Assert.True(CollectionsEqual(module.FormalInputParameter, actualModule.FormalInputParameter));
            Assert.Empty(actualModule.FormalOutputParameter);
            Assert.Empty(actualModule.AdditionalSignals);
            Assert.Equal(module, actualModule);

            int numExpectedParams = paramNames.Count();
            Assert.NotNull(actualModule.Scope);
            Assert.Empty(actualModule.Scope.Replacements);
            Assert.Empty(actualModule.Scope.InverseReplacements);
            Assert.Equal(numExpectedParams, actualModule.Scope.Symbols.Count());

            Expression expectedExpr = new Expression(Operand.NONE)
            {
                RequiresBooleanOperands = false,
                HasNumericResult = true,
                Literal = new Literal("", LiteralValueType.SIGNAL, null)
                {
                    LiteralType = LiteralType.PARAMETER
                }
            };
            foreach (var name in paramNames)
            {
                Assert.True(actualModule.Scope.Symbols.ContainsKey(name));
                expectedExpr.Literal.Name = name;
                Assert.Equal(expectedExpr, actualModule.Scope.Symbols[name]);
            }
        }

        [Theory]
        [InlineData(new string[] { "a", "b" }, new ParameterType[2] { ParameterType.IN, ParameterType.IN },
            new int[2] { 5, 10 })]
        public void ModuleWithOnlyInputParametersCustomSignalWidth(IEnumerable<string> paramNames, IEnumerable<ParameterType> paramTypes,
            IEnumerable<int> signalParamWidths)
        {
            Module module = Utils.CreateCustomSignalModule("adder", paramNames, paramTypes, signalParamWidths);
            module.Body.Expressions.Add(Utils.CreateSkipExpression());
            _userQueryParser.ParseQuery(module.ToString());

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();

            Assert.True(CollectionsEqual(module.FormalInputParameter, actualModule.FormalInputParameter));
            Assert.Empty(actualModule.FormalOutputParameter);
            Assert.Empty(actualModule.AdditionalSignals);
            Assert.Equal(module, actualModule);

            int numExpectedParams = paramNames.Count();
            Assert.NotNull(actualModule.Scope);
            Assert.Empty(actualModule.Scope.Replacements);
            Assert.Empty(actualModule.Scope.InverseReplacements);
            Assert.Equal(numExpectedParams, actualModule.Scope.Symbols.Count());

            Expression expectedExpr = new Expression(Operand.NONE)
            {
                RequiresBooleanOperands = false,
                HasNumericResult = true,
                Literal = new Literal("", LiteralValueType.SIGNAL, null)
                {
                    LiteralType = LiteralType.PARAMETER
                }
            };
            IEnumerator<int> customSignalWidths = signalParamWidths.GetEnumerator();
            foreach (var name in paramNames)
            {
                customSignalWidths.MoveNext();
                Assert.True(actualModule.Scope.Symbols.ContainsKey(name));
                expectedExpr.Literal.Name = name;
                expectedExpr.Literal.SignalWidth = customSignalWidths.Current;
                Assert.Equal(expectedExpr, actualModule.Scope.Symbols[name]);
            }
        }


        [Theory]
        [InlineData(new string[] { "a", "b", "c", "d" }, 
            new ParameterType[4] { ParameterType.IN, ParameterType.IN, ParameterType.INOUT, ParameterType.OUT},
            new int[4] { 5, 10, 5, 16 })]
        public void ModuleWithMixedParameters(IEnumerable<string> paramNames, IEnumerable<ParameterType> paramTypes,
            IEnumerable<int> signalParamWidths)
        {
            Module module = Utils.CreateCustomSignalModule("adder", paramNames, paramTypes, signalParamWidths);
            module.Body.Expressions.Add(Utils.CreateSkipExpression());
            _userQueryParser.ParseQuery(module.ToString());

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();

            Assert.True(CollectionsEqual(module.FormalInputParameter, actualModule.FormalInputParameter));
            Assert.True(CollectionsEqual(module.FormalOutputParameter, actualModule.FormalOutputParameter));
            Assert.Empty(actualModule.AdditionalSignals);
            Assert.Equal(module, actualModule);

            int numExpectedParams = paramNames.Count();
            Assert.NotNull(actualModule.Scope);
            Assert.Empty(actualModule.Scope.Replacements);
            Assert.Empty(actualModule.Scope.InverseReplacements);
            Assert.Equal(numExpectedParams, actualModule.Scope.Symbols.Count());

            Expression expectedExpr = new Expression(Operand.NONE)
            {
                RequiresBooleanOperands = false,
                HasNumericResult = true,
                Literal = new Literal("", LiteralValueType.SIGNAL, null)
                {
                    LiteralType = LiteralType.PARAMETER
                }
            };
            IEnumerator<int> customSignalWidths = signalParamWidths.GetEnumerator();
            IEnumerator<ParameterType> paramTypesEnumerator = paramTypes.GetEnumerator();
            foreach (var name in paramNames)
            {
                customSignalWidths.MoveNext();
                paramTypesEnumerator.MoveNext();
                Assert.True(actualModule.Scope.Symbols.ContainsKey(name));
                expectedExpr.Literal.Name = name;
                expectedExpr.Literal.SignalWidth = customSignalWidths.Current;
                if (paramTypesEnumerator.Current == ParameterType.OUT)
                {
                    expectedExpr.Literal.Value = 0;
                }
                else
                {
                    expectedExpr.Literal.Value = null;
                }
                Assert.Equal(expectedExpr, actualModule.Scope.Symbols[name]);
            }
        }

        [Theory]
        [InlineData(new string[] { "a", "b" }, new ParameterType[2] { ParameterType.STATE, ParameterType.STATE },
            new int[2] { Constants.DEFAULT_SIGNAL_WIDTH, 10 })]
        public void ModuleOnlyAdditionalStateParameters(IEnumerable<string> paramNames, IEnumerable<ParameterType> paramTypes,
            IEnumerable<int> signalParamWidths)
        {
            Module module = Utils.CreateCustomSignalModule("adder", paramNames, paramTypes, signalParamWidths);
            module.Body.Expressions.Add(Utils.CreateSkipExpression());
            _userQueryParser.ParseQuery(module.ToString());

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();

            Assert.Empty(actualModule.FormalInputParameter);
            Assert.Empty(actualModule.FormalOutputParameter);
            Assert.True(module.AdditionalSignals.SequenceEqual(actualModule.AdditionalSignals));
            Assert.Equal(module, actualModule);

            int numExpectedParams = paramNames.Count();
            Assert.NotNull(actualModule.Scope);
            Assert.Empty(actualModule.Scope.Replacements);
            Assert.Empty(actualModule.Scope.InverseReplacements);
            Assert.Equal(numExpectedParams, actualModule.Scope.Symbols.Count());

            Expression expectedExpr = new Expression(Operand.NONE)
            {
                RequiresBooleanOperands = false,
                HasNumericResult = true,
                Literal = new Literal("", LiteralValueType.SIGNAL, null)
                {
                    LiteralType = LiteralType.PARAMETER
                }
            };
            IEnumerator<int> customSignalWidths = signalParamWidths.GetEnumerator();
            foreach (var name in paramNames)
            {
                customSignalWidths.MoveNext();
                Assert.True(actualModule.Scope.Symbols.ContainsKey(name));
                expectedExpr.Literal.Name = name;
                expectedExpr.Literal.SignalWidth = customSignalWidths.Current;
                Assert.Equal(expectedExpr, actualModule.Scope.Symbols[name]);
            }
        }

        [Theory]
        [InlineData(new string[] { "a", "b" }, new ParameterType[2] { ParameterType.WIRE, ParameterType.WIRE },
            new int[2] { Constants.DEFAULT_SIGNAL_WIDTH, 10 })]
        public void ModuleOnlyAdditionalWireParameters(IEnumerable<string> paramNames, IEnumerable<ParameterType> paramTypes,
            IEnumerable<int> signalParamWidths)
        {
            Module module = Utils.CreateCustomSignalModule("adder", paramNames, paramTypes, signalParamWidths);
            module.Body.Expressions.Add(Utils.CreateSkipExpression());
            _userQueryParser.ParseQuery(module.ToString());

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();

            Assert.Empty(actualModule.FormalInputParameter);
            Assert.Empty(actualModule.FormalOutputParameter);
            Assert.True(module.AdditionalSignals.SequenceEqual(actualModule.AdditionalSignals));
            Assert.Equal(module, actualModule);

            int numExpectedParams = paramNames.Count();
            Assert.NotNull(actualModule.Scope);
            Assert.Empty(actualModule.Scope.Replacements);
            Assert.Empty(actualModule.Scope.InverseReplacements);
            Assert.Equal(numExpectedParams, actualModule.Scope.Symbols.Count());

            Expression expectedExpr = new Expression(Operand.NONE)
            {
                RequiresBooleanOperands = false,
                HasNumericResult = true,
                Literal = new Literal("", LiteralValueType.SIGNAL, 0)
                {
                    LiteralType = LiteralType.PARAMETER
                }
            };
            IEnumerator<int> customSignalWidths = signalParamWidths.GetEnumerator();
            foreach (var name in paramNames)
            {
                customSignalWidths.MoveNext();
                Assert.True(actualModule.Scope.Symbols.ContainsKey(name));
                expectedExpr.Literal.Name = name;
                expectedExpr.Literal.SignalWidth = customSignalWidths.Current;
                Assert.Equal(expectedExpr, actualModule.Scope.Symbols[name]);
            }
        }

        [Theory]
        [InlineData(new string[7] { "a", "b", "c", "d", "e", "f","g" }, 
            new ParameterType[7] { ParameterType.IN, ParameterType.OUT, ParameterType.INOUT, ParameterType.WIRE, ParameterType.STATE, ParameterType.WIRE, ParameterType.STATE },
            new int[7] { Constants.DEFAULT_SIGNAL_WIDTH, 10, Constants.DEFAULT_SIGNAL_WIDTH, 10, Constants.DEFAULT_SIGNAL_WIDTH, 10, Constants.DEFAULT_SIGNAL_WIDTH })]
        public void ModuleWithMixedIOParamsMixedAdditionalParams(IEnumerable<string> paramNames, IEnumerable<ParameterType> paramTypes,
            IEnumerable<int> signalParamWidths)
        {
            Module module = Utils.CreateCustomSignalModule("adder", paramNames, paramTypes, signalParamWidths);
            module.Body.Expressions.Add(Utils.CreateSkipExpression());
            _userQueryParser.ParseQuery(module.ToString());

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();

            IEnumerator<ParameterType> paramTypesEnumerator = paramTypes.GetEnumerator();
            int outputParamIdx = 0;
            int additionalParamIdx = 0;
            while (paramTypesEnumerator.MoveNext())
            {
                if (paramTypesEnumerator.Current == ParameterType.OUT) { module.FormalOutputParameter[outputParamIdx++].Literal.Value = 0; }
                else if (paramTypesEnumerator.Current == ParameterType.WIRE) { module.AdditionalSignals[additionalParamIdx++].Literal.Value = 0; }
            }

            Assert.True(module.FormalInputParameter.SequenceEqual(actualModule.FormalInputParameter));
            Assert.True(module.FormalOutputParameter.SequenceEqual(actualModule.FormalOutputParameter));
            if (module.AdditionalSignals.Any())
            {
                ParameterType firstAddType = module.AdditionalSignals.First().ParameterType;
                module.AdditionalSignals = module.AdditionalSignals.OrderByDescending(x => x.ParameterType == firstAddType).ToList();
            }
            Assert.True(module.AdditionalSignals.SequenceEqual(actualModule.AdditionalSignals));
            Assert.Equal(module, actualModule);

            int numExpectedParams = paramNames.Count();
            Assert.NotNull(actualModule.Scope);
            Assert.Empty(actualModule.Scope.Replacements);
            Assert.Empty(actualModule.Scope.InverseReplacements);
            Assert.Equal(numExpectedParams, actualModule.Scope.Symbols.Count());

            Expression expectedExpr = new Expression(Operand.NONE)
            {
                RequiresBooleanOperands = false,
                HasNumericResult = true,
                Literal = new Literal("", LiteralValueType.SIGNAL, null)
                {
                    LiteralType = LiteralType.PARAMETER
                }
            };
            IEnumerator<int> customSignalWidths = signalParamWidths.GetEnumerator();
            paramTypesEnumerator.Reset();
            foreach (var name in paramNames)
            {
                customSignalWidths.MoveNext();
                paramTypesEnumerator.MoveNext();
                Assert.True(actualModule.Scope.Symbols.ContainsKey(name));
                if (paramTypesEnumerator.Current == ParameterType.OUT || paramTypesEnumerator.Current == ParameterType.WIRE)
                {
                    expectedExpr.Literal.Value = 0;
                }
                else
                {
                    expectedExpr.Literal.Value = null;
                }
                expectedExpr.Literal.Name = name;
                expectedExpr.Literal.SignalWidth = customSignalWidths.Current;
                Assert.Equal(expectedExpr, actualModule.Scope.Symbols[name]);
            }
        }

        [Theory]
        [InlineData(Operand.ADD)]
        [InlineData(Operand.SUB)]
        [InlineData(Operand.MUL)]
        [InlineData(Operand.DIV)]
        [InlineData(Operand.MOD)]
        [InlineData(Operand.BIT_AND)]
        [InlineData(Operand.BIT_OR)]
        public void ModuleOneArithmeticBinaryExpressions(Operand binaryOperand)
        {
            List<string> paramList = new List<string>(2) { "a", "b" };
            List<ParameterType> paramTypes = new List<ParameterType>(2) { ParameterType.IN, ParameterType.INOUT };
            Module module = Utils.CreateNormalModule("adder", paramList, paramTypes);

            Expression leftExpr = Utils.CreateExpression(Operand.NONE, paramList[0], null, true, true);
            Expression rightExpr = Utils.CreateExpression(Operand.NONE, paramList[1], null, true, true);
            Expression binaryExpr = Utils.CreateExpression(binaryOperand, leftExpr, rightExpr);
            Expression headExpr = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(Operand.NONE, paramList[1], null, true, true), binaryExpr);
            module.Body.Expressions.Add(headExpr);
            _userQueryParser.ParseQuery(module.ToString());

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();
            Assert.Equal(module, actualModule);

            Assert.Equal(module.Body.Expressions[0], actualModule.Body.Expressions[0]);
            Expression expectedLeftExpression = module.Body.Expressions[0].LeftExpression;
            Expression actualLeftExpression = actualModule.Body.Expressions[0].LeftExpression;
            Assert.Equal(expectedLeftExpression, actualLeftExpression);

            Expression expectedRightExpression = module.Body.Expressions[0].RightExpression;
            Expression actualRightExpression = actualModule.Body.Expressions[0].RightExpression;
            Assert.Equal(expectedRightExpression, actualRightExpression);
        }

        [Theory]
        [InlineData(Operand.INCR_EQ)]
        [InlineData(Operand.DECR_EQ)]
        [InlineData(Operand.NEG_UNARY_EQ)]
        public void ModuleUnaryStatement(Operand unaryOperand)
        {
            List<string> paramList = new List<string>(1) { "b" };
            List<ParameterType> paramTypes = new List<ParameterType>(1) { ParameterType.INOUT };
            Module module = Utils.CreateNormalModule("adder", paramList, paramTypes);

            Expression rightExpr = Utils.CreateExpression(Operand.NONE, paramList[0], null, true, true);
            Expression unaryExpr = Utils.CreateExpression(unaryOperand, rightExpr, null);
            module.Body.Expressions.Add(unaryExpr);
            _userQueryParser.ParseQuery(module.ToString());

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();
            Assert.Equal(module, actualModule);

            Assert.Equal(module.Body.Expressions[0], actualModule.Body.Expressions[0]);
            Expression expectedLeftExpression = module.Body.Expressions[0].LeftExpression;
            Expression actualLeftExpression = actualModule.Body.Expressions[0].LeftExpression;
            Assert.Equal(expectedLeftExpression, actualLeftExpression);
        }

        [Theory]
        [InlineData(Operand.ADD, 3, 5, 8)]
        [InlineData(Operand.SUB, 5, 3, 2)]
        [InlineData(Operand.MUL, 3, 5, 15)]
        [InlineData(Operand.DIV, 6, 3, 2)]
        [InlineData(Operand.MOD, 5, 3, 2)]
        [InlineData(Operand.BIT_AND, 3, 5, 1)]
        [InlineData(Operand.BIT_OR, 3, 5, 7)]
        [InlineData(Operand.XOR, 3, 5, 6)]
        public void SimplifyArithmeticExprConstantOperandArithmeticResult(Operand op, int leftConstVal, int rightConstVal, int expectedConstVal)
        {
            Expression leftExpr = Utils.CreateConstantNumericExpression(leftConstVal);
            Expression rightExpr = Utils.CreateConstantNumericExpression(rightConstVal);
            Expression arithmeticExpr = Utils.CreateExpression(op, leftExpr, rightExpr);
            Expression assignExpr = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(Operand.NONE, "a", null, true, true), arithmeticExpr);
            Module module = Utils.CreateNormalModule("adder", new string[1] { "a" }, new ParameterType[1] { ParameterType.OUT });
            module.Body.Expressions.Add(assignExpr);
            _userQueryParser.ParseQuery(module.ToString());

            Expression expectedRightExpr = Utils.CreateConstantNumericExpression(expectedConstVal);
            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();
            Expression actualExpr = actualModule.Body.Expressions.First();
            Assert.Equal(expectedRightExpr, actualExpr.RightExpression);
        }

        [Theory]
        [InlineData(Operand.INCR_EQ, 1)]
        public void UpdateSignalUnaryStmt(Operand unaryOperand, int expectedResultValue)
        {
            List<string> paramList = new List<string>(1) { "b" };
            List<ParameterType> paramTypes = new List<ParameterType>(1) { ParameterType.OUT };
            Module module = Utils.CreateNormalModule("adder", paramList, paramTypes);

            Expression rightExpr = Utils.CreateExpression(Operand.NONE, paramList[0], null, true, true);
            Expression unaryExpr = Utils.CreateExpression(unaryOperand, rightExpr, null);
            module.Body.Expressions.Add(unaryExpr);
            _userQueryParser.ParseQuery(module.ToString());

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();
            Assert.Equal(module, actualModule);
            module.Body.Expressions[0].LeftExpression.Literal.Value = expectedResultValue;

            Assert.Equal(module.Body.Expressions[0], actualModule.Body.Expressions[0]);
            Expression expectedLeftExpression = module.Body.Expressions[0].LeftExpression;
            Expression actualLeftExpression = actualModule.Body.Expressions[0].LeftExpression;
            Assert.Equal(expectedLeftExpression, actualLeftExpression);

            Assert.NotNull(actualModule.Scope);
            Assert.Single(actualModule.Scope.Symbols);
            Assert.True(actualModule.Scope.Symbols.ContainsKey(paramList[0]));

            Expression expectedExpr = new Expression(Operand.NONE)
            {
                RequiresBooleanOperands = false,
                HasNumericResult = true,
                Literal = new Literal(paramList[0], LiteralValueType.SIGNAL, expectedResultValue)
                {
                    LiteralType = LiteralType.PARAMETER
                }
            };
            Expression scopeExpr = actualModule.Scope.Symbols[paramList[0]];
            Assert.Equal(expectedExpr, scopeExpr);
        }

        [Theory]
        [InlineData(ParameterType.OUT, 3, 0, 5)]
        [InlineData(ParameterType.OUT, 3, 1, 5)]
        [InlineData(ParameterType.OUT, 3, 2, 5)]
        [InlineData(ParameterType.WIRE, 3, 0, 5)]
        [InlineData(ParameterType.WIRE, 3, 1, 5)]
        [InlineData(ParameterType.WIRE, 3, 2, 5)]
        public void Increment1DArrayParameterWithDefaultValue(ParameterType paramType, int dimension, int dimensionToUpdate, int constValue)
        {
            string moduleName = "adder";
            IList<string> paramNames = new List<string>(1) { "a" };
            IList<ParameterType> parameterTypes = new List<ParameterType>(1) { paramType };
            IList<int> signalWidths = new List<int>(1) { Constants.DEFAULT_SIGNAL_WIDTH };
            IDictionary<string, int> dimensionDict = new Dictionary<string, int>(1) { { paramNames[0], dimension } };
            Module module = Utils.CreateMixedModule(moduleName, paramNames, parameterTypes, signalWidths, dimensionDict);

            Literal arrayLiteral = Utils.CreateSignalLiteral(paramNames[0], LiteralValueType.SIGNAL, dimension, signalWidths[0], 0, 0, Utils.CreateConstantNumericExpression(dimensionToUpdate), true);
            Expression addExpr = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(arrayLiteral), Utils.CreateConstantNumericExpression(constValue));
            addExpr.LeftExpression.Literal.Values[dimensionToUpdate] = constValue;
            module.Body.Expressions.Add(addExpr);

            _userQueryParser.ParseQuery(module.ToString());
            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();
            Assert.Equal(module, actualModule);

            Expression expectedExpr = addExpr;
            Assert.NotNull(actualModule.Body);
            Assert.Equal(BlockType.MODULE, actualModule.Body.BlockType);
            Assert.Empty(actualModule.Body.InnerBlocks);
            Assert.Empty(actualModule.Body.ElseExpressions);
            Assert.Null(actualModule.Body.GuardExpr);
            Assert.Null(actualModule.Body.OtherExpr);

            Assert.Single(actualModule.Body.Expressions);
            Assert.Equal(expectedExpr, addExpr);
            Assert.True(actualModule.Scope.Symbols.ContainsKey(paramNames[0]));

            Literal scopeLiteral = actualModule.Scope.Symbols[paramNames[0]].Literal;
            Assert.Equal(LiteralValueType.SIGNAL, scopeLiteral.LiteralValueType);
            //Assert.Equal(arrayLiteral.DimensionExpr, scopeLiteral.DimensionExpr);
            foreach (var idx in Enumerable.Range(0, dimension))
            {
                Assert.Equal((idx != dimensionToUpdate ? 0 : constValue), scopeLiteral.Values[idx]);
            }
        }

        [Fact]
        public void CopyPropagationMultipleStmts()
        {
            int expectedNumParams = 3;
            List<string> paramList = new List<string>(expectedNumParams) { "b", "c", "d" };
            List<ParameterType> paramTypes = new List<ParameterType>(expectedNumParams) { ParameterType.OUT, ParameterType.WIRE, ParameterType.WIRE };
            Module module = Utils.CreateNormalModule("adder", paramList, paramTypes);

            Expression bLiteralExpr = Utils.CreateExpression(Operand.NONE, paramList[0], null, true, true);
            Expression cLiteralExpr = Utils.CreateExpression(Operand.NONE, paramList[1], null, true, true);
            Expression dLiteralExpr = Utils.CreateExpression(Operand.NONE, paramList[2], null, true, true);

            Expression incrementExpr = Utils.CreateExpression(Operand.INCR_EQ, bLiteralExpr, null);
            Expression addExpr = Utils.CreateExpression(Operand.ADD, bLiteralExpr, cLiteralExpr);
            Expression otherExpr = Utils.CreateExpression(Operand.ADD_EQ, dLiteralExpr, addExpr);
            Expression decrExpr = Utils.CreateExpression(Operand.SUB_EQ, bLiteralExpr, dLiteralExpr);
            Expression finalDecrExpr = Utils.CreateExpression(Operand.DECR_EQ, dLiteralExpr, null);

            module.Body.Expressions.Add(incrementExpr);
            module.Body.Expressions.Add(otherExpr);
            module.Body.Expressions.Add(decrExpr);
            module.Body.Expressions.Add(finalDecrExpr);

            _userQueryParser.ParseQuery(module.ToString());

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();
            Assert.Equal(module, actualModule);

            HashSet<string> expectedParamDict = paramList.ToHashSet();
            Assert.NotNull(actualModule.Scope);
            Assert.Empty(actualModule.Scope.Replacements);
            Assert.Empty(actualModule.Scope.InverseReplacements);
            Assert.True(actualModule.Scope.Symbols.Count >= expectedNumParams);
            foreach (var symb in actualModule.Scope.Symbols)
            {
                if (expectedParamDict.Contains(symb.Key))
                {
                    Assert.Equal(0, symb.Value.Literal.Value);
                }
            }
        }

        [Fact]
        public void SignalWidthOperator()
        {
            IList<string> paramNames = new List<string>(3) { "a", "b", "c" };
            IList<ParameterType> paramTypes = new List<ParameterType>(3) { ParameterType.IN, ParameterType.OUT, ParameterType.INOUT };
            IList<int> signalParamWidths = new List<int>(3) { Constants.DEFAULT_SIGNAL_WIDTH / 8, Constants.DEFAULT_SIGNAL_WIDTH, Constants.DEFAULT_SIGNAL_WIDTH / 4 };
            Module module = Utils.CreateCustomSignalModule("adder", paramNames, paramTypes, signalParamWidths);

            Expression aLitExpr = Utils.CreateExpression(Operand.NONE, "#" + paramNames[0], null, true, true);
            Expression cLitExpr = Utils.CreateExpression(Operand.NONE, "#" + paramNames[2], null, true, true); 
            Expression bLitExpr = Utils.CreateExpression(Operand.NONE, paramNames[1], null, true, true);

            Expression incrementOp_1 = Utils.CreateExpression(Operand.ADD_EQ, bLitExpr, aLitExpr);
            Expression incrementOp_2 = Utils.CreateExpression(Operand.ADD_EQ, bLitExpr, cLitExpr);
            Expression addExpr = Utils.CreateExpression(Operand.ADD, aLitExpr, cLitExpr);
            Expression decrementOp_2 = Utils.CreateExpression(Operand.SUB_EQ, bLitExpr, addExpr);
            module.Body.Expressions.Add(incrementOp_1);
            module.Body.Expressions.Add(incrementOp_2);
            module.Body.Expressions.Add(decrementOp_2);
            _userQueryParser.ParseQuery(module.ToString());

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();

            Expression expectedLeftExpr_1 = Utils.CreateExpression(Operand.NONE, paramNames[1], null, true, true);
            expectedLeftExpr_1.Literal.Value = signalParamWidths[0];
            Expression expectedExpr_1 = Utils.CreateExpression(Operand.ADD_EQ, expectedLeftExpr_1, Utils.CreateConstantNumericExpression(signalParamWidths[0]));

            Expression expectedLeftExpr_2 = Utils.CreateExpression(Operand.NONE, paramNames[1], null, true, true);
            expectedLeftExpr_2.Literal.Value = signalParamWidths[0] + signalParamWidths[2];
            Expression expectedExpr_2 = Utils.CreateExpression(Operand.ADD_EQ, expectedLeftExpr_2, Utils.CreateConstantNumericExpression(signalParamWidths[2]));// signalParamWidths[2]));

            Expression expectedLeftExpr_3 = Utils.CreateExpression(Operand.NONE, paramNames[1], null, true, true);
            expectedLeftExpr_3.Literal.Value = 0;

            Expression expectedExpr_3 = Utils.CreateExpression(Operand.SUB_EQ, expectedLeftExpr_3, Utils.CreateConstantNumericExpression(signalParamWidths[0] + signalParamWidths[2]));

            IList<Expression> expectedExpr = new List<Expression>(3) { expectedExpr_1, expectedExpr_2, expectedExpr_3 };
            Assert.True(CollectionsEqual<Expression>(expectedExpr, actualModule.Body.Expressions));
        }

        [Theory]
        [InlineData(13, 0, 1)]
        [InlineData(13, 1, 0)]
        [InlineData(13, 2, 1)]
        [InlineData(13, 3, 1)]
        public void BitAccessOfSignalWithDefaultSignalWidth(int referenceValue, int bitPosition, int expectedValue)
        {
            IList<string> paramNames = new List<string>(2) { "a", "b" };
            IList<ParameterType> paramTypes = new List<ParameterType>(2) { ParameterType.OUT, ParameterType.OUT };
            IList<int> signalParamWidths = new List<int>(2) { Constants.DEFAULT_SIGNAL_WIDTH, Constants.DEFAULT_SIGNAL_WIDTH };
            Module module = Utils.CreateCustomSignalModule("adder", paramNames, paramTypes, signalParamWidths);

            Expression bLitExpr = Utils.CreateExpression(Operand.NONE, paramNames[1], null, true, true);

            Expression incrementOp_1 = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(Operand.NONE, paramNames[0], null, true, true), Utils.CreateConstantNumericExpression(referenceValue));
            incrementOp_1.LeftExpression.Literal.Value = referenceValue;
            Expression incrementOp_2 = Utils.CreateExpression(Operand.ADD_EQ, bLitExpr, Utils.CreateExpression(Operand.NONE, paramNames[0], null, true, true));
            incrementOp_2.RightExpression.Literal.LiteralValueType = LiteralValueType.RANGE;
            incrementOp_2.RightExpression.Literal.StartIdx = bitPosition;
            incrementOp_2.RightExpression.Literal.EndIdx = bitPosition;
            incrementOp_2.LeftExpression.Literal.Value = expectedValue;

            module.Body.Expressions.Add(incrementOp_1);
            module.Body.Expressions.Add(incrementOp_2);
            _userQueryParser.ParseQuery(module.ToString());

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();
            Assert.NotNull(actualModule.Body);

            incrementOp_2.RightExpression = Utils.CreateConstantNumericExpression(expectedValue);
            IList<Expression> expectedExpressions = new List<Expression>(2){ incrementOp_1, incrementOp_2};
            Assert.Equal(expectedExpressions.Count(), actualModule.Body.Expressions.Count());
            Assert.True(CollectionsEqual<Expression>(expectedExpressions, actualModule.Body.Expressions));

            (string, int)[] expectedScopeLiterals = new (string, int)[2] { (paramNames[0], referenceValue), (paramNames[1], expectedValue) };
            foreach ((string scopeVarName, int scopeVarVal) in expectedScopeLiterals)
            {
                Assert.True(actualModule.Scope.Symbols.ContainsKey(scopeVarName));
                Assert.Equal(scopeVarVal, (int)actualModule.Scope.Symbols[scopeVarName].Literal.Value);
            }
        }

        [Theory]
        [InlineData(0, 5)]
        [InlineData(0, 15)]
        [InlineData(3, 5)]
        [InlineData(10, 15)]
        [InlineData(5, 5)]
        [InlineData(0, 0)]
        public void RangeAccessOfInputSignalConstantIndizes(int rangeStartIdx, int rangeEndIdx)
        {
            IList<string> paramNames = new List<string>(2) { "a", "b" };
            IList<ParameterType> paramTypes = new List<ParameterType>(2) { ParameterType.IN, ParameterType.IN };
            IList<int> signalParamWidths = new List<int>(2) { Constants.DEFAULT_SIGNAL_WIDTH, Constants.DEFAULT_SIGNAL_WIDTH };
            Module module = Utils.CreateCustomSignalModule("adder", paramNames, paramTypes, signalParamWidths);

            Expression aLitExpr = Utils.CreateExpression(Operand.NONE, paramNames[0], null, true, true);
            aLitExpr.Literal.LiteralValueType = LiteralValueType.RANGE; //rangeStartIdx == rangeEndIdx ? LiteralValueType.INTEGER : LiteralValueType.RANGE;
            aLitExpr.Literal.StartIdx = rangeStartIdx;
            aLitExpr.Literal.EndIdx = rangeEndIdx;

            Expression bLitExpr = Utils.CreateExpression(Operand.NONE, paramNames[1], null, true, true);
            Expression addExpr = Utils.CreateExpression(Operand.ADD_EQ, bLitExpr, aLitExpr);

            module.Body.Expressions.Add(addExpr);
            _userQueryParser.ParseQuery(module.ToString());

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();
            Assert.NotNull(actualModule.Body);
            Assert.Single(actualModule.Body.Expressions);
            Assert.Empty(actualModule.Body.ElseExpressions);
            Assert.Empty(actualModule.Body.InnerBlocks);
            Assert.Null(actualModule.Body.GuardExpr);
            Assert.Null(actualModule.Body.OtherExpr);
            Assert.Equal(addExpr, actualModule.Body.Expressions.First());
        }

        [Theory]
        [InlineData(156, 0, 1, 0)]
        [InlineData(156, 0, 4, 28)]
        [InlineData(156, 2, 4, 7)]
        [InlineData(156, 5, 6, 0)]
        public void RangeAccessOfSignalWithDefaultSignalWidth(int referenceValue, int rangeStartIdx, int rangeEndIdx, int expectedValue)
        {
            IList<string> paramNames = new List<string>(2) { "a", "b" };
            IList<ParameterType> paramTypes = new List<ParameterType>(2) { ParameterType.OUT, ParameterType.OUT };
            IList<int> signalParamWidths = new List<int>(2) { Constants.DEFAULT_SIGNAL_WIDTH, Constants.DEFAULT_SIGNAL_WIDTH };
            Module module = Utils.CreateCustomSignalModule("adder", paramNames, paramTypes, signalParamWidths);

            Expression bLitExpr = Utils.CreateExpression(Operand.NONE, paramNames[1], null, true, true);

            Expression incrementOp_1 = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(Operand.NONE, paramNames[0], null, true, true), Utils.CreateConstantNumericExpression(referenceValue));
            incrementOp_1.LeftExpression.Literal.Value = referenceValue;
            Expression incrementOp_2 = Utils.CreateExpression(Operand.ADD_EQ, bLitExpr, Utils.CreateExpression(Operand.NONE, paramNames[0], null, true, true));
            incrementOp_2.RightExpression.Literal.LiteralValueType = LiteralValueType.RANGE;
            incrementOp_2.RightExpression.Literal.StartIdx = rangeStartIdx;
            incrementOp_2.RightExpression.Literal.EndIdx = rangeEndIdx;
            incrementOp_2.LeftExpression.Literal.Value = expectedValue;

            module.Body.Expressions.Add(incrementOp_1);
            module.Body.Expressions.Add(incrementOp_2);
            _userQueryParser.ParseQuery(module.ToString());

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();
            Assert.NotNull(actualModule.Body);

            incrementOp_2.RightExpression = Utils.CreateConstantNumericExpression(expectedValue);
            IList<Expression> expectedExpressions = new List<Expression>(2) { incrementOp_1, incrementOp_2 };
            Assert.Equal(expectedExpressions.Count(), actualModule.Body.Expressions.Count());
            Assert.True(CollectionsEqual<Expression>(expectedExpressions, actualModule.Body.Expressions));

            (string, int)[] expectedScopeLiterals = new (string, int)[2] { (paramNames[0], referenceValue), (paramNames[1], expectedValue) };
            foreach ((string scopeVarName, int scopeVarVal) in expectedScopeLiterals)
            {
                Assert.True(actualModule.Scope.Symbols.ContainsKey(scopeVarName));
                Assert.Equal(scopeVarVal, (int)actualModule.Scope.Symbols[scopeVarName].Literal.Value);
            }
        }

        [Theory]
        [InlineData(9, 6)]
        [InlineData(7, 0)]
        [InlineData(0, 0)]
        [InlineData(int.MaxValue, 0)]
        public void BitwiseNegationAssignmentOfArithmeticResult(int constValue, int expectedValue)
        {
            IList<string> paramNames = new List<string>(1) { "b" };
            IList<ParameterType> paramTypes = new List<ParameterType>(1) { ParameterType.OUT };
            IList<int> signalParamWidths = new List<int>(1) { Constants.DEFAULT_SIGNAL_WIDTH };
            Module module = Utils.CreateCustomSignalModule("adder", paramNames, paramTypes, signalParamWidths);

            Expression bLiteralExpr = Utils.CreateExpression(Operand.NONE, paramNames[0], null, true, true);
            Expression incrementExpr = Utils.CreateExpression(Operand.ADD_EQ, bLiteralExpr, Utils.CreateConstantNumericExpression(constValue));
            incrementExpr.LeftExpression.Literal.Value = constValue;
            Expression unaryExpr = Utils.CreateExpression(Operand.NEG_UNARY_EQ, bLiteralExpr, null);
            unaryExpr.LeftExpression.Literal.Value = expectedValue;

            module.Body.Expressions.Add(incrementExpr);
            module.Body.Expressions.Add(unaryExpr);
            _userQueryParser.ParseQuery(module.ToString());

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();
            Block actModuleBody = actualModule.Body;
            Assert.NotNull(actModuleBody);
            Assert.Empty(actModuleBody.InnerBlocks);
            Assert.Null(actModuleBody.GuardExpr);
            Assert.Null(actModuleBody.OtherExpr);
            Assert.Empty(actModuleBody.ElseExpressions);
            Assert.Equal(2, actModuleBody.Expressions.Count());
            Expression actualNegationExpr = actModuleBody.Expressions[1];
            Assert.Equal(unaryExpr, actualNegationExpr);
            Assert.True(actualModule.Scope.Symbols.ContainsKey(paramNames[0]));
            Assert.Equal(expectedValue, actualModule.Scope.Symbols[paramNames[0]].Literal.Value);
        }

        /*
        [Theory]
        public void NegationOfExpressions(Operand negationOperand, bool isBooleanValue, object valueToNegate, object expectedNegationResult)
        {

        }
        */

        /*
        [Theory]
        public void ArithmeticOperationWithRangeOperand()
        {

        }
        */

        [Theory]
        [InlineData(0, 10, 1, true)]
        [InlineData(0, 10, 2, true)]
        [InlineData(5, 10, 5, true)]
        [InlineData(10, 0, 1, false)]
        [InlineData(10, 0, 2, false)]
        [InlineData(10, 5, 5, false)]
        public void LoopVariableCopyPropagation(int startVal, int endVal, int stepSize, bool positiveStepSize)
        {

            IList<string> paramNames = new List<string>(2) { "a", "b"};
            IList<ParameterType> paramTypes = new List<ParameterType>(2) { ParameterType.OUT, ParameterType.OUT};
            IList<int> signalParamWidths = Enumerable.Repeat(Constants.DEFAULT_SIGNAL_WIDTH, 3).ToList();
            Module module = Utils.CreateCustomSignalModule("adder", paramNames, paramTypes, signalParamWidths);

            string loopVarName = "loopVar";
            Expression aLitExpr = Utils.CreateExpression(Operand.NONE, paramNames[0], null, true, true);
            Expression bLitExpr = Utils.CreateExpression(Operand.NONE, paramNames[1], null, true, true);

            Expression innerLoopExpr_1 = Utils.CreateExpression(Operand.INCR_EQ, aLitExpr, null);
            Expression innerLoopExpr_2 = Utils.CreateExpression(Operand.ADD_EQ, bLitExpr, Utils.CreateExpression(Operand.NONE, "$" + loopVarName, null, true, true));

            Block loopBlock = Utils.CreateLoopBlock(loopVarName, startVal, endVal, stepSize, positiveStepSize, new List<Expression>(2) { innerLoopExpr_1, innerLoopExpr_2 });
            module.Body.Expressions.Add(null);
            module.Body.InnerBlocks.Add(loopBlock);
            _userQueryParser.ParseQuery(module.ToString());

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();


            Expression expExpr_1 = Utils.CreateExpression(Operand.INCR_EQ, aLitExpr, null);
            int expectedNumStatements = ((positiveStepSize ? (endVal - startVal) : (startVal - endVal)) / stepSize) * 2;
            IList<Expression> expectedExprs = Enumerable.Repeat(expExpr_1, expectedNumStatements).ToList();
            int cnt = 0;
            int currVal = startVal;
            for (int i = 1; i < expectedNumStatements; i += 2)
            {
                expectedExprs[i] = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(Operand.NONE, paramNames[1], null, true, true), 
                    Utils.CreateConstantNumericExpression(cnt));
                expectedExprs[i].LeftExpression.Literal.LiteralType = LiteralType.PARAMETER;
                expectedExprs[i].LeftExpression.Literal.Value = cnt;
                if (cnt >= 1)
                {
                    currVal += (int)expectedExprs[i - 2].LeftExpression.Literal.Value;
                }
                expectedExprs[i].LeftExpression.Literal.Value = currVal;
                //cnt++;
                cnt += stepSize;
                /*
                if (cnt > 2)
                {
                    expectedExprs[i].LeftExpression.Literal.Value = currVal; //(int)expectedExprs[i - 2].LeftExpression.Literal.Value;
                }
                */

                /*
                 * 
                if (cnt > 1)
                {
                    expectedExprs[i].LeftExpression.Literal.Value = (int) expectedExprs[i-2].LeftExpression.Literal.Value + (currVal > 0 ? currVal : 1);
                    //currVal += cnt; //(int) expectedExprs[i].LeftExpression.Literal.Value;
                    currVal += stepSize;
                }
                else
                {
                    expectedExprs[i].LeftExpression.Literal.Value = 0;
                }
                cnt++;
                */
            }
            cnt = 0;
            for (int i = 0; i < expectedNumStatements; i += 2)
            {
                expectedExprs[i].LeftExpression.Literal.Value = cnt++;
            }
            Assert.True(CollectionsEqual<Expression>(expectedExprs, actualModule.Body.Expressions));
        }

        /*
        [Theory]
        [InlineData(Operand.EQ, 3, 5, false)]
        [InlineData(Operand.EQ, 3, 3, true)]
        [InlineData(Operand.NEQ, 3, 5, true)]
        [InlineData(Operand.NEQ, 5, 5, false)]
        [InlineData(Operand.GREATER, 3, 5, false)]
        [InlineData(Operand.GREATER, 5, 3, true)]
        [InlineData(Operand.GREATER_EQ, 3, 5, false)]
        [InlineData(Operand.GREATER_EQ, 5, 3, true)]
        [InlineData(Operand.GREATER_EQ, 5, 5, true)]
        [InlineData(Operand.LESS, 3, 5, true)]
        [InlineData(Operand.LESS, 5, 3, false)]
        [InlineData(Operand.LESS_EQ, 3, 5, true)]
        [InlineData(Operand.LESS_EQ, 5, 5, true)]
        [InlineData(Operand.LESS_EQ, 5, 3, false)]
        public void SimplifyArithmeticExprConstantOperandBooleanResult(Operand op, uint leftConstVal, uint rightConstVal, bool expectedConstVal)
        {
            Expression leftExpr = Utils.CreateConstantNumericExpression(leftConstVal);
            Expression rightExpr = Utils.CreateConstantNumericExpression(rightConstVal);
            Expression arithmeticExpr = Utils.CreateExpression(op, leftExpr, rightExpr);
            Expression assignExpr = Utils.CreateExpression(Operand.ADD_EQ, Utils.CreateExpression(Operand.NONE, "a", null, true, true), arithmeticExpr);
            Module module = Utils.CreateNormalModule("adder", new string[1] { "a" }, new ParameterType[1] { ParameterType.OUT });
            module.Body.Expressions.Add(assignExpr);
            _userQueryParser.ParseQuery(module.ToString());

            Expression expectedRightExpr = Utils.CreateConstantBooleanExpression(expectedConstVal);
            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();
            Expression actualExpr = actualModule.Body.Expressions.First();
            Assert.Equal(expectedRightExpr, actualExpr.RightExpression);
        }
        */

        [Theory]
        [InlineData(Operand.EQ)]
        [InlineData(Operand.NEQ)]
        [InlineData(Operand.GREATER)]
        [InlineData(Operand.GREATER_EQ)]
        [InlineData(Operand.LESS)]
        [InlineData(Operand.LESS_EQ)]
        public void IfBlockSimpleGuardExpressionNoSimplification(Operand guardOperand) 
        {
            Operand trueBranchOperand = Operand.ADD;
            Operand falseBranchOperand = Operand.SUB;

            Module module = Utils.CreateNormalModule("adder", new string[3] { "a", "b", "c" }, new ParameterType[3] { ParameterType.IN, ParameterType.IN, ParameterType.INOUT });
            Expression aLiteralExpr = Utils.CreateExpression(Operand.NONE, "a", null, true, true);
            Expression bLiteralExpr = Utils.CreateExpression(Operand.NONE, "b", null, true, true);
            Expression cLiteralExpr = Utils.CreateExpression(Operand.NONE, "c", null, true, true);

            Expression guardExpr = Utils.CreateExpression(guardOperand, aLiteralExpr, bLiteralExpr);
            Expression trueBranchRightExpr = Utils.CreateExpression(trueBranchOperand, aLiteralExpr, bLiteralExpr);
            Expression falseBranchRightExpr = Utils.CreateExpression(falseBranchOperand, aLiteralExpr, bLiteralExpr);
            Expression otherExpr = guardExpr;

            Expression ifBlockExpr = Utils.CreateExpression(Operand.ADD_EQ, cLiteralExpr, trueBranchRightExpr);
            Expression elseBlockExpr = Utils.CreateExpression(Operand.SUB_EQ, cLiteralExpr, falseBranchRightExpr);

            IEnumerable<Expression> ifBlockStatements = Enumerable.Repeat(ifBlockExpr, 2);
            IEnumerable<Expression> elseBlockStatements = Enumerable.Repeat(elseBlockExpr, 2);

            Block ifBlock = Utils.CreateNonNestedConditionBlock(guardExpr, ifBlockStatements, elseBlockStatements, otherExpr);
            module.Body.Expressions.Add(null);
            module.Body.InnerBlocks.Add(ifBlock);
            _userQueryParser.ParseQuery(module.ToString());

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();
            Assert.Single(actualModule.Body.InnerBlocks);
            Block innerBlock = actualModule.Body.InnerBlocks.First();
            Assert.Equal(BlockType.CONDITION, innerBlock.BlockType);

            Assert.Equal(2, innerBlock.Expressions.Count());
            Assert.True(CollectionsEqual<Expression>(ifBlockStatements.ToList(), innerBlock.Expressions));

            Assert.Equal(2, innerBlock.ElseExpressions.Count());
            Assert.True(CollectionsEqual<Expression>(elseBlockStatements.ToList(), innerBlock.ElseExpressions));
        }

        [Fact]
        public void IfBlockTrueBranchOneLevelNestingBlockAndStatement()
        {
            Operand guardOperand = Operand.GREATER;
            Operand trueBranchOperand = Operand.ADD;
            Operand falseBranchOperand = Operand.SUB;

            Module module = Utils.CreateNormalModule("adder", new string[3] { "a", "b", "c" }, new ParameterType[3] { ParameterType.IN, ParameterType.IN, ParameterType.INOUT });
            Expression aLiteralExpr = Utils.CreateExpression(Operand.NONE, "a", null, true, true);
            Expression bLiteralExpr = Utils.CreateExpression(Operand.NONE, "b", null, true, true);
            Expression cLiteralExpr = Utils.CreateExpression(Operand.NONE, "c", null, true, true);

            Expression guardExpr = Utils.CreateExpression(guardOperand, aLiteralExpr, bLiteralExpr);
            Expression trueBranchRightExpr = Utils.CreateExpression(trueBranchOperand, aLiteralExpr, bLiteralExpr);
            Expression falseBranchRightExpr = Utils.CreateExpression(falseBranchOperand, aLiteralExpr, bLiteralExpr);
            Expression otherExpr = guardExpr;

            Expression ifBlockExpr = Utils.CreateExpression(Operand.ADD_EQ, cLiteralExpr, trueBranchRightExpr);
            Expression elseBlockExpr = Utils.CreateExpression(Operand.SUB_EQ, cLiteralExpr, falseBranchRightExpr);

            Expression childBlockGuardExpr = Utils.CreateExpression(Operand.LESS, aLiteralExpr, cLiteralExpr);
            Block childBlock = Utils.CreateNonNestedConditionBlock(childBlockGuardExpr, new List<Expression>(1) { elseBlockExpr },
                new List<Expression>(1) { ifBlockExpr }, childBlockGuardExpr);

            IList<Expression> ifBlockStmts = new List<Expression>(3)
            {
                ifBlockExpr, null, elseBlockExpr
            };
            IEnumerable<Expression> elseBlockStmts = Enumerable.Repeat(elseBlockExpr, 2);

            Block ifBlock = Utils.CreateNonNestedConditionBlock(guardExpr, ifBlockStmts, elseBlockStmts, otherExpr);
            ifBlock.InnerBlocks.Add(childBlock);
            module.Body.Expressions.Add(null);
            module.Body.InnerBlocks.Add(ifBlock);
            _userQueryParser.ParseQuery(module.ToString());

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();

            // Test first inner block, i.e. the first if expression
            int expectedBlockStmtCnt = ifBlockStmts.Count();
            int expectedBlockElseStmtCnt = elseBlockStmts.Count();

            Assert.Single(actualModule.Body.InnerBlocks);
            Block firstInnerBlock = actualModule.Body.InnerBlocks.First();
            Assert.Single(firstInnerBlock.InnerBlocks);
            Assert.Equal(BlockType.CONDITION, firstInnerBlock.BlockType);

            Assert.Equal(expectedBlockStmtCnt, firstInnerBlock.Expressions.Count());
            Assert.True(CollectionsEqual<Expression>(ifBlockStmts, firstInnerBlock.Expressions));

            Assert.Equal(expectedBlockElseStmtCnt, firstInnerBlock.ElseExpressions.Count());
            Assert.True(CollectionsEqual<Expression>(elseBlockStmts.ToList(), firstInnerBlock.ElseExpressions));

            // Check child inner block
            expectedBlockStmtCnt = 1;
            expectedBlockElseStmtCnt = 1;
            ifBlockStmts = new List<Expression>(1) { elseBlockExpr };
            elseBlockStmts = new List<Expression>(1) { ifBlockExpr };
            Block secondInnerBlock = firstInnerBlock.InnerBlocks.First();
            Assert.Equal(BlockType.CONDITION, secondInnerBlock.BlockType);

            Assert.Equal(expectedBlockStmtCnt, secondInnerBlock.Expressions.Count());
            Assert.True(CollectionsEqual<Expression>(ifBlockStmts, secondInnerBlock.Expressions));

            Assert.Equal(expectedBlockElseStmtCnt, secondInnerBlock.ElseExpressions.Count());
            Assert.True(CollectionsEqual<Expression>(elseBlockStmts.ToList(), secondInnerBlock.ElseExpressions));
        }

        [Theory]
        [InlineData(Operand.EQ, 3, 5, false)]
        [InlineData(Operand.EQ, 3, 3, true)]
        [InlineData(Operand.NEQ, 3, 5, true)]
        [InlineData(Operand.NEQ, 5, 5, false)]
        [InlineData(Operand.GREATER, 3, 5, false)]
        [InlineData(Operand.GREATER, 5, 3, true)]
        [InlineData(Operand.GREATER_EQ, 3, 5, false)]
        [InlineData(Operand.GREATER_EQ, 5, 3, true)]
        [InlineData(Operand.GREATER_EQ, 5, 5, true)]
        [InlineData(Operand.LESS, 3, 5, true)]
        [InlineData(Operand.LESS, 5, 3, false)]
        [InlineData(Operand.LESS_EQ, 3, 5, true)]
        [InlineData(Operand.LESS_EQ, 5, 5, true)]
        [InlineData(Operand.LESS_EQ, 5, 3, false)]
        public void SimplifyIfBlock(Operand compareOperand, int leftConstVal, int rightConstVal, bool expectedConstValue)
        {
            Module module = Utils.CreateNormalModule("adder", new string[1] { "a" }, new ParameterType[1] { ParameterType.IN });

            Expression guardExpr = Utils.CreateExpression(compareOperand, Utils.CreateConstantNumericExpression(leftConstVal), Utils.CreateConstantNumericExpression(rightConstVal));
            Expression ifBlockExpr = Utils.CreateExpression(Operand.INCR_EQ, Utils.CreateExpression(Operand.NONE, "a", null, true, true), null);
            Expression elseBlockExpr = Utils.CreateExpression(Operand.DECR_EQ, Utils.CreateExpression(Operand.NONE, "a", null, true, true), null);
            Expression otherExpr = guardExpr;

            IEnumerable<Expression> ifBlockStatements = Enumerable.Repeat(ifBlockExpr, 2);
            IEnumerable<Expression> elseBlockStatements = Enumerable.Repeat(elseBlockExpr, 2);

            Block ifBlock = Utils.CreateNonNestedConditionBlock(guardExpr, ifBlockStatements, elseBlockStatements, otherExpr);
            module.Body.Expressions.Add(null);
            module.Body.InnerBlocks.Add(ifBlock);
            _userQueryParser.ParseQuery(module.ToString());

            IEnumerable<Expression> expectedStatements = expectedConstValue ? ifBlockStatements : elseBlockStatements;

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();
            Assert.Empty(actualModule.Body.InnerBlocks);
            Assert.Equal(expectedStatements.Count(), actualModule.Body.Expressions.Count());
            Assert.True(CollectionsEqual<Expression>(expectedStatements.ToList(), actualModule.Body.Expressions));
        }

        [Theory]
        [InlineData(ParameterType.OUT, Constants.DEFAULT_SIGNAL_WIDTH, 10, true)]
        [InlineData(ParameterType.OUT, Constants.DEFAULT_SIGNAL_WIDTH, 0, false)]
        [InlineData(ParameterType.WIRE, Constants.DEFAULT_SIGNAL_WIDTH/8, 10, true)]
        [InlineData(ParameterType.WIRE, Constants.DEFAULT_SIGNAL_WIDTH/8, 0, false)]
        [InlineData(ParameterType.WIRE, Constants.DEFAULT_SIGNAL_WIDTH / 3, 10, true)]
        [InlineData(ParameterType.WIRE, Constants.DEFAULT_SIGNAL_WIDTH / 3, 0, false)]
        public void UseNumericVariableAsGuardCondition(ParameterType paramType, int signalWidth, int constVal, bool expectedIfBranch)
        {
            IList<string> paramNames = new List<string>(1) { "a" };
            IList<ParameterType> paramTypes = new List<ParameterType>(1) { paramType };
            IList<int> signalWidths = new List<int>(1) { signalWidth };
            IDictionary<string, int> arrayDict = new Dictionary<string, int>();
            Module module = Utils.CreateMixedModule("adder", paramNames, paramTypes, signalWidths, arrayDict);

            Literal addParamLiteral = new Literal("", LiteralValueType.BOOL, null);
            Literal moduleParamLiteral = paramType == ParameterType.OUT ? module.FormalOutputParameter[0].Literal : module.AdditionalSignals[0].Literal;
            CopyLiteral(ref addParamLiteral, moduleParamLiteral);
            Expression addParamLiteralExpr = Utils.CreateExpression(addParamLiteral);
            Expression addExpr = Utils.CreateExpression(Operand.ADD_EQ, addParamLiteralExpr, Utils.CreateConstantNumericExpression(constVal));
            addExpr.LeftExpression.Literal.Values[0] = constVal;
            addExpr.LeftExpression.Literal.SignalWidth = signalWidth;
            
            Expression guardExpr = addParamLiteralExpr;
            Expression trueBranchParamLiteralExpr = Utils.CreateExpression(Operand.NONE, paramNames[0], null, true, true);
            trueBranchParamLiteralExpr.Literal = paramType == ParameterType.OUT ? module.FormalOutputParameter[0].Literal : module.AdditionalSignals[0].Literal;
            Expression ifBlockExpr = Utils.CreateExpression(Operand.ADD_EQ, trueBranchParamLiteralExpr, Utils.CreateConstantNumericExpression(1));

            Expression falseBranchParamLiteralExpr = Utils.CreateExpression(Operand.NONE, paramNames[0], null, true, true);
            falseBranchParamLiteralExpr.Literal = paramType == ParameterType.OUT ? module.FormalOutputParameter[0].Literal : module.AdditionalSignals[0].Literal; 

            Expression elseBlockExpr = Utils.CreateExpression(Operand.SUB_EQ, falseBranchParamLiteralExpr, Utils.CreateConstantNumericExpression(1));
            Expression otherExpr = guardExpr;

            IEnumerable<Expression> ifBlockStatements = Enumerable.Repeat(ifBlockExpr, 1);
            IEnumerable<Expression> elseBlockStatements = Enumerable.Repeat(elseBlockExpr, 1);

            Block ifBlock = Utils.CreateNonNestedConditionBlock(guardExpr, ifBlockStatements, elseBlockStatements, otherExpr);
            module.Body.Expressions.Add(addExpr);
            module.Body.Expressions.Add(null);
            module.Body.InnerBlocks.Add(ifBlock);
            _userQueryParser.ParseQuery(module.ToString());

            IList<Expression> expectedStatements = new List<Expression>(2) { addExpr, (expectedIfBranch ? ifBlockStatements.First() : elseBlockStatements.First()) };
            expectedStatements[1].LeftExpression.Literal.SignalWidth = signalWidth;
            expectedStatements[1].LeftExpression.Literal.Values[0] = constVal;

            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);
            Module actualModule = _userQueryParser.Modules.First();
            Assert.Empty(actualModule.Body.InnerBlocks);
            Assert.Equal(expectedStatements.Count(), actualModule.Body.Expressions.Count());
            Assert.True(CollectionsEqual<Expression>(expectedStatements.ToList(), actualModule.Body.Expressions));
        }

        [Theory]
        [InlineData(0, 2, true, 1)]
        [InlineData(5, 10, true, 5)]
        [InlineData(0, 10, true, 10)]
        [InlineData(10, 0, false, 1)]
        [InlineData(10, 5, false, 5)]
        [InlineData(10, 0, false, 10)]
        public void NoneNestedLoopBlockConstantStepSize(int startValue, int endValue, bool positiveStepSize, int stepSize)
        {
            Module module = Utils.CreateNormalModule("adder", new string[2] { "a", "b" }, new ParameterType[2] { ParameterType.IN, ParameterType.INOUT });
            Expression aLiteralExpr = Utils.CreateExpression(Operand.NONE, "a", null, true, true);
            Expression bLiteralExpr = Utils.CreateExpression(Operand.NONE, "b", null, true, true);
            Expression loopBodyExpr = Utils.CreateExpression(Operand.ADD_EQ, bLiteralExpr, aLiteralExpr);

            Block loopBlock = Utils.CreateLoopBlock("", startValue, endValue, stepSize, positiveStepSize, new List<Expression>(1) { loopBodyExpr });
            module.Body.Expressions.Add(null);
            module.Body.InnerBlocks.Add(loopBlock);

            _userQueryParser.ParseQuery(module.ToString());
            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);

            Module actualModule = _userQueryParser.Modules.First();
            Block moduleBody = actualModule.Body;
            Assert.Equal(BlockType.MODULE, moduleBody.BlockType);
            Assert.Empty(moduleBody.InnerBlocks);
            Assert.Empty(moduleBody.ElseExpressions);
            Assert.Null(moduleBody.GuardExpr);
            Assert.Null(moduleBody.OtherExpr);
            Assert.Null(moduleBody.LoopVariable);

            int expectedNumStatements = (positiveStepSize ? (endValue - startValue) : (startValue - endValue)) / stepSize;
            Assert.Equal(expectedNumStatements, moduleBody.Expressions.Count());

            IEnumerable<Expression> expectedExprs = Enumerable.Repeat(loopBodyExpr, expectedNumStatements);
            Assert.True(CollectionsEqual<Expression>(expectedExprs.ToList(), moduleBody.Expressions));
        }

        [Fact]
        public void NoneNestedLoopBlockMultipleStmtsAfter()
        {
            int startValue = 0;
            int endValue = 10;
            int stepSize = 1;
            bool positiveStepSize = true;

            Module module = Utils.CreateNormalModule("adder", new string[2] { "a", "b" }, new ParameterType[2] { ParameterType.IN, ParameterType.INOUT });
            Expression aLiteralExpr = Utils.CreateExpression(Operand.NONE, "a", null, true, true);
            Expression bLiteralExpr = Utils.CreateExpression(Operand.NONE, "b", null, true, true);
            Expression loopBodyExpr = Utils.CreateExpression(Operand.ADD_EQ, bLiteralExpr, aLiteralExpr);

            Block loopBlock = Utils.CreateLoopBlock("", startValue, endValue, stepSize, positiveStepSize, new List<Expression>(1) { loopBodyExpr });
            module.Body.Expressions.Add(null);
            module.Body.InnerBlocks.Add(loopBlock);

            Expression followupExpr1 = Utils.CreateExpression(Operand.SUB_EQ, aLiteralExpr, bLiteralExpr);
            Expression followupExpr2 = Utils.CreateExpression(Operand.XOR_EQ, aLiteralExpr, bLiteralExpr);
            module.Body.Expressions.Add(followupExpr1);
            module.Body.Expressions.Add(followupExpr2);

            _userQueryParser.ParseQuery(module.ToString());
            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);

            Module actualModule = _userQueryParser.Modules.First();
            Block moduleBody = actualModule.Body;
            Assert.Equal(BlockType.MODULE, moduleBody.BlockType);
            Assert.Empty(moduleBody.InnerBlocks);
            Assert.Empty(moduleBody.ElseExpressions);
            Assert.Null(moduleBody.GuardExpr);
            Assert.Null(moduleBody.OtherExpr);
            Assert.Null(moduleBody.LoopVariable);

            int expectedNumStatements = (positiveStepSize ? (endValue - startValue) : (startValue - endValue)) / stepSize;
            expectedNumStatements += 2;
            Assert.Equal(expectedNumStatements, moduleBody.Expressions.Count());

            IList<Expression> expectedExprs = Enumerable.Repeat(loopBodyExpr, expectedNumStatements - 2).ToList();
            expectedExprs.Add(followupExpr1);
            expectedExprs.Add(followupExpr2);
            Assert.True(CollectionsEqual<Expression>(expectedExprs.ToList(), moduleBody.Expressions));
        }

        [Theory]
        [InlineData(0, 10, 1, true, 0, 6, 2, true)]
        [InlineData(10, 0, 1, false, 0, 5, 5, true)]
        [InlineData(0, 10, 1, true, 5, 0, 5, false)]
        [InlineData(10, 0, 1, false, 5, 0, 5, false)]
        public void NestedLoopBlockWithMultipleStmtsAfter(int innerStartVal, int innerEndVal, int innerStepSize, bool innerStepSizePositive,
            int outerStartVal, int outerEndVal, int outerStepSize, bool outerStepSizePositive)
        {
            Module module = Utils.CreateNormalModule("adder", new string[2] { "a", "b" }, new ParameterType[2] { ParameterType.IN, ParameterType.INOUT });
            Expression aLiteralExpr = Utils.CreateExpression(Operand.NONE, "a", null, true, true);
            Expression bLiteralExpr = Utils.CreateExpression(Operand.NONE, "b", null, true, true);
            Expression innerLoopBodyExpr = Utils.CreateExpression(Operand.ADD_EQ, bLiteralExpr, aLiteralExpr);

            Block innerLoopBlock = Utils.CreateLoopBlock("", innerStartVal, innerEndVal, innerStepSize, innerStepSizePositive, new List<Expression>(1) { innerLoopBodyExpr });
            Block outerLoopBlock = Utils.CreateLoopBlock("", outerStartVal, outerEndVal, outerStepSize, outerStepSizePositive, new List<Expression>(1) { null });
            outerLoopBlock.InnerBlocks.Add(innerLoopBlock);
            module.Body.Expressions.Add(null);
            module.Body.InnerBlocks.Add(outerLoopBlock);

            int numFollowUpExpr = 2;
            Expression followupExpr1 = Utils.CreateExpression(Operand.SUB_EQ, aLiteralExpr, bLiteralExpr);
            Expression followupExpr2 = Utils.CreateExpression(Operand.XOR_EQ, aLiteralExpr, bLiteralExpr);
            module.Body.Expressions.Add(followupExpr1);
            module.Body.Expressions.Add(followupExpr2);

            _userQueryParser.ParseQuery(module.ToString());
            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);

            Module actualModule = _userQueryParser.Modules.First();
            Block moduleBody = actualModule.Body;
            Assert.Equal(BlockType.MODULE, moduleBody.BlockType);
            Assert.Empty(moduleBody.InnerBlocks);
            Assert.Empty(moduleBody.ElseExpressions);
            Assert.Null(moduleBody.GuardExpr);
            Assert.Null(moduleBody.OtherExpr);
            Assert.Null(moduleBody.LoopVariable);

            int expectedNumStatements = (innerStepSizePositive ? (innerEndVal - innerStartVal)  : (innerStartVal - innerEndVal)) / innerStepSize;
            int outerLoopExptNumStmts = (outerStepSizePositive ? (outerEndVal - outerStartVal) : (outerStartVal - outerEndVal)) / outerStepSize;
            if (outerLoopExptNumStmts > 1)
            {
                expectedNumStatements = outerLoopExptNumStmts * expectedNumStatements;
            }
            expectedNumStatements += numFollowUpExpr;
            Assert.Equal(expectedNumStatements, moduleBody.Expressions.Count());

            IList<Expression> expectedExprs = Enumerable.Repeat(innerLoopBodyExpr, expectedNumStatements - numFollowUpExpr).ToList();
            expectedExprs.Add(followupExpr1);
            expectedExprs.Add(followupExpr2);
            Assert.True(CollectionsEqual<Expression>(expectedExprs.ToList(), moduleBody.Expressions));
        }

        [Fact]
        public void NestedConditionInLoopBlockMultipleStmtsAfter()
        {
            Module module = Utils.CreateNormalModule("adder", new string[2] { "a", "b" }, new ParameterType[2] { ParameterType.IN, ParameterType.INOUT });
            Expression aLiteralExpr = Utils.CreateExpression(Operand.NONE, "a", null, true, true);
            Expression bLiteralExpr = Utils.CreateExpression(Operand.NONE, "b", null, true, true);
            Expression ifGuardExpr = Utils.CreateExpression(Operand.GREATER, aLiteralExpr, bLiteralExpr);
            Expression trueBranchExpr = Utils.CreateExpression(Operand.XOR_EQ, bLiteralExpr, aLiteralExpr);
            Expression falseBranchExpr = Utils.CreateExpression(Operand.ADD_EQ, aLiteralExpr, bLiteralExpr);
            Block conditionBlock = Utils.CreateNonNestedConditionBlock(ifGuardExpr, new List<Expression>(1) { trueBranchExpr }, new List<Expression>(1) { falseBranchExpr }, ifGuardExpr);

            int startValue = 0;
            int endValue = 10;
            int stepSize = 5;
            bool positiveStepSize = true;
            Block loopBlock = Utils.CreateLoopBlock("", startValue, endValue, stepSize, positiveStepSize, new List<Expression>(1) { null });
            loopBlock.InnerBlocks.Add(conditionBlock);
            module.Body.Expressions.Add(null);
            module.Body.InnerBlocks.Add(loopBlock);

            int numFollowUpExpr = 2;
            int numLoopExprCnt = (endValue - startValue) / stepSize;
            Expression followupExpr1 = Utils.CreateExpression(Operand.SUB_EQ, aLiteralExpr, bLiteralExpr);
            Expression followupExpr2 = Utils.CreateExpression(Operand.XOR_EQ, aLiteralExpr, bLiteralExpr);
            module.Body.Expressions.Add(followupExpr1);
            module.Body.Expressions.Add(followupExpr2);

            _userQueryParser.ParseQuery(module.ToString());
            Assert.True(_userQueryParser.IsValidQuery);
            Assert.Empty(_userQueryParser.Errors);
            Assert.Single(_userQueryParser.Modules);

            Module actualModule = _userQueryParser.Modules.First();
            Block moduleBody = actualModule.Body;
            Assert.Equal(BlockType.MODULE, moduleBody.BlockType);
            Assert.Equal(numLoopExprCnt, moduleBody.InnerBlocks.Count);
            Assert.Empty(moduleBody.ElseExpressions);
            Assert.Null(moduleBody.GuardExpr);
            Assert.Null(moduleBody.OtherExpr);
            Assert.Null(moduleBody.LoopVariable);

            Assert.True(CollectionsEqual<Block>(Enumerable.Repeat(conditionBlock, numLoopExprCnt).ToList(), moduleBody.InnerBlocks));
            Assert.Equal(numLoopExprCnt + numFollowUpExpr, moduleBody.Expressions.Count);
            Assert.Equal(followupExpr1, moduleBody.Expressions[numLoopExprCnt]);
            Assert.Equal(followupExpr2, moduleBody.Expressions[numLoopExprCnt+1]);
        }

        private static bool CollectionsEqual<T>(IList<T> collection_1, IList<T> collection_2)
        {
            bool equal = false;
            equal = collection_1 != null && collection_2 != null;
            equal &= collection_1.Count == collection_2.Count;
            
            if ((equal & (!collection_1.Any() && !collection_2.Any())) || !equal) return equal;
            var x = collection_1.Where(x => !collection_2.Contains(x));
            return Enumerable.Range(0, collection_1.Count - 1).All(idx =>
            {
                if (collection_1[idx] == null && collection_2[idx] == null) return true;
                else if (collection_1[idx] != null & collection_2[idx] != null)
                {
                    return collection_1[idx].Equals(collection_2[idx]);
                }
                else return false;
            });
        }

        private void CopyLiteral(ref Literal newLiteral, Literal literalToCopy)
        {
            if (literalToCopy == null) return;
            newLiteral.Name = literalToCopy.Name;
            newLiteral.SignalWidth = literalToCopy.SignalWidth;
            newLiteral.IsArrayType = literalToCopy.IsArrayType;
            newLiteral.Dimension = literalToCopy.Dimension;
            newLiteral.DimensionExpr = literalToCopy.DimensionExpr;
            newLiteral.StartIdx = literalToCopy.StartIdx;
            newLiteral.EndIdx = literalToCopy.EndIdx;
            newLiteral.LiteralType = literalToCopy.LiteralType;
            newLiteral.LiteralValueType = literalToCopy.LiteralValueType;
            newLiteral.StartValue = literalToCopy.StartValue;
            newLiteral.EndValue = literalToCopy.EndValue;
            newLiteral.StepSize = literalToCopy.StepSize;
            newLiteral.PositiveStepSize = literalToCopy.PositiveStepSize;
            newLiteral.Values = new List<object>(literalToCopy.Values).ToArray();
        }
    }
}
