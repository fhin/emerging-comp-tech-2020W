﻿namespace OptimizerGenerator
{
    public class OptGenConstants
    {
        public const string FRAMES_FILE_PATH = @"CocoSources";
        public const string ATG_FILE_PATH = @"REVCOMP_OPTIMIZER.ATG";
        public const string COCO_EXE_FILE_PATH = FRAMES_FILE_PATH + "\\Coco.exe";
    }
}
