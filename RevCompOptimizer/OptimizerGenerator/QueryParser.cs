﻿using OptimizerGenerator.ParserEntities;
using System;
using System.Collections.Generic;
using System.IO;

namespace OptimizerGenerator
{
    public class QueryParser
    {
        public bool IsValidQuery { get; private set; }
        public IList<QueryError> Errors { get; private set; }
        public IList<Module> Modules { get; private set; }
        public QueryParser()
        {
            IsValidQuery = false;
            Errors = new List<QueryError>();
            Modules = new List<Module>();
        }

        public void ParseQuery(string queryString)
        {
            if (string.IsNullOrWhiteSpace(queryString)) throw new ArgumentException("");
            try
            {
                using (var stream = new MemoryStream())
                {
                    using (var writer = new StreamWriter(stream))
                    {
                        writer.Write(queryString);
                        writer.Flush();
                        stream.Position = 0;

                        Scanner s = new Scanner(stream);
                        using (var parserOutputRedirect = new ParserOutputRedirect())
                        {
                            Parser p = new Parser(s);
                            p.errors.errorStream = parserOutputRedirect;
                            p.stream = stream;
                            p.Parse();
                            Errors = parserOutputRedirect.QueryErrors;
                            Modules = p.definedModules;
                        }

                        if (Errors.Count == 0)
                        {
                            IsValidQuery = true;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error parsing query for reason: \n " + e.Message + "\n");
                Console.WriteLine("Stacktrace: " + e.StackTrace);
                IsValidQuery = false;
            }
        }
    }
}
