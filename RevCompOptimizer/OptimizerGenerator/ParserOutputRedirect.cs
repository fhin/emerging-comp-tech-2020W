﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace OptimizerGenerator
{
    public class ParserOutputRedirect : System.IO.TextWriter
    {
        public IList<QueryError> QueryErrors { get; set; }
        private readonly Regex _lineNumberRegex;
        private readonly Regex _columnNumberRegex;
        private readonly string _lineNumberRegexPattern = @"(?<=(^(--)\s(line)\s))\d+";
        private readonly string _columnNumberRegexPattern = @"(?<=(^\s(col)\s))\d+";

        public ParserOutputRedirect()
        {
            QueryErrors = new List<QueryError>();
            _lineNumberRegex = new Regex(_lineNumberRegexPattern);
            _columnNumberRegex = new Regex(_columnNumberRegexPattern);
        }

        public override void WriteLine(string value)
        {
            try
            {
                Match m = _lineNumberRegex.Match(value);
                if (!m.Success)
                {
                    throw new ArgumentException();
                }
                int errLineNum = int.Parse(m.Value);
                value = value.Substring(m.Index + m.Length, value.Length - (m.Index + m.Length));

                m = _columnNumberRegex.Match(value);

                if (!m.Success) throw new ArgumentException();
                int errColNum = int.Parse(m.Value);
                string errMsg = value.Substring(m.Index + m.Length + 2, value.Length - (m.Index + m.Length + 2));
                QueryErrors.Add(new QueryError(errLineNum, errColNum, errMsg));
            }
            catch (Exception e)
            {
                throw new ArgumentException("Could not add query error message for given text !" +
                    "\n Reason: " + e.Message);
            }
        }

        public override Encoding Encoding => Console.Out.Encoding;
    }
}
