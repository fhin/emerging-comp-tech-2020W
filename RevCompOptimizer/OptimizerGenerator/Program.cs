﻿using System;
using System.Diagnostics;
using System.IO;

namespace OptimizerGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            LaunchCommandLineApp();
        }

        static void LaunchCommandLineApp()
        {
            try
            {
                string outputFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"GeneratedEntities");// @"GeneratedEntities";
                PrintLaunchArgs(outputFilePath);
                ProcessStartInfo startInfo = new ProcessStartInfo
                {
                    CreateNoWindow = false,
                    UseShellExecute = false,
                    FileName = OptGenConstants.COCO_EXE_FILE_PATH,
                    WindowStyle = ProcessWindowStyle.Normal,
                    Arguments = OptGenConstants.ATG_FILE_PATH + " -frames " + OptGenConstants.FRAMES_FILE_PATH + " -o " + outputFilePath
                };
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                using (Process exeProcess = Process.Start(startInfo))
                {
                    exeProcess.WaitForExit(15000);
                }
                // TODO: Logging after exit if there where errors
            }
            catch (Exception e)
            {
                // Log error.
                Console.WriteLine(e.Message);
                return;
            }
        }

        static void PrintLaunchArgs(string outputFilePath)
        {
            Console.WriteLine("Using Coco frames located @ {0}", OptGenConstants.FRAMES_FILE_PATH);
            Console.WriteLine("Using Coco executable located @ {0}", OptGenConstants.COCO_EXE_FILE_PATH);
            Console.WriteLine("Using ATG located @ {0}", OptGenConstants.ATG_FILE_PATH);
            Console.WriteLine("Writing generated entities to location @ {0}", outputFilePath);
            Console.WriteLine("=========================================================================");
        }
    }
}
