﻿namespace OptimizerGenerator.ParserEntities
{
    public class Parameter
    {
        public string Name { get; set; }
        public ParameterType ParameterType { get; set; }
        public Literal Literal { get; set; }

        public Parameter(string name)
        {
            // TODO: Check for non empty parameter name
            Name = name;
            ParameterType = ParameterType.INOUT;
            Literal = new Literal(Name, LiteralValueType.SIGNAL, null);
        }

        public Parameter(string name, int dimension, int signalWidth)
            : this(name)
        {
            SetSignalProperties(dimension, signalWidth);
        }

        public void SetSignalProperties(int dimension, int signalWidth)
        {
            Literal.SignalWidth = signalWidth;
            Literal.Dimension = dimension;
        }

        public override int GetHashCode()
        {
            int hashCode = 352033288;
            hashCode *= string.IsNullOrWhiteSpace(Name) ? string.Empty.GetHashCode() : Name.GetHashCode();
            hashCode *= -1521134295 + Literal.SignalWidth.GetHashCode();
            hashCode *= -1521134295 + ParameterType.GetHashCode();
            if (Literal.IsArrayType)
            {
                hashCode *= -1521134295 * Literal.Dimension.GetHashCode();
            }
            return hashCode;
        }

        public override bool Equals(object obj)
        {
            try
            {
                return Equals((Parameter)obj);
            }
            catch (System.Exception)
            {
                return false;
            }
        }


        public bool Equals(Parameter other)
        {
            if (other == null || other.Literal == null || !Name.Equals(other.Name) || Literal.SignalWidth != other.Literal.SignalWidth 
                || ParameterType != other.ParameterType
                || ((Literal.IsArrayType ^ other.Literal.IsArrayType))
                || (Literal.IsArrayType && other.Literal.IsArrayType && Literal.Dimension != other.Literal.Dimension)) return false;
            return true;
        }
    }
}
