﻿using System;
using System.Collections.Generic;

namespace OptimizerGenerator.ParserEntities
{
    public class Scope
    {
        private uint replacementCnt;

        public IDictionary<string, Expression> Symbols { get; set; }
        public IDictionary<string, string> Replacements { get; set; }
        public IDictionary<string, string> InverseReplacements { get; set; }

        public Scope ParentScope { get; set; }

        public Scope()
        {
            Symbols = new Dictionary<string, Expression>();
            Replacements = new Dictionary<string, string>();
            InverseReplacements = new Dictionary<string, string>();
            replacementCnt = 0;
        }

        public Scope(Scope parent)
            :this()
        {
            ParentScope = parent;
        }

        public void AddItem(Literal literal)
        {
            if (Symbols.ContainsKey(literal.Name)) return;
            Expression expr = new Expression()
            {
                HasNumericResult = true,
                Literal = literal
            };
            Symbols.Add(literal.Name, expr);
        }

        private void Update(Expression updScopeExpr)
        {
            Scope currScope = this;
            string name = (updScopeExpr != null && updScopeExpr.Literal != null) ? updScopeExpr.Literal.Name : null;
            // TODO: Could also throw Exception instead
            if (string.IsNullOrWhiteSpace(name)) return;
            while (currScope != null)
            {
                if (InverseReplacements.ContainsKey(name))
                {
                    name = InverseReplacements[name];
                }
                if (currScope.Symbols.ContainsKey(name))
                {
                    try
                    {
                        if (updScopeExpr.Literal.DimensionExpr != null && updScopeExpr.Literal.DimensionExpr.Operand == Operand.NONE)
                        {
                            currScope.Symbols[name].Literal.Values[(int)updScopeExpr.Literal.DimensionExpr.Literal.Value] = updScopeExpr.Literal.Value;
                        }
                        else
                        {
                            currScope.Symbols[name].Literal.Value = updScopeExpr.Literal.Value;
                        }
                        break;
                    }
                    // TODO: Better exception handling
                    catch (Exception) { return; }
                }
                else
                {
                    currScope = currScope.ParentScope;
                }
            }
        }

        public void UpdateValue(Literal literal)
        {
            Update(new Expression(Operand.NONE) { Literal = literal });
        }

        public void AddReplacement(Literal lLiteral, Literal rLiteral)
        {
            string currentLitName = string.IsNullOrWhiteSpace(lLiteral.Name) ? "<null>" : lLiteral.Name;
            string replacementLitName = string.IsNullOrWhiteSpace(rLiteral.Name) ? "<null>" : rLiteral.Name;

            Literal lBackupLiteral = new Literal("", LiteralValueType.SIGNAL, null);
            if (!Symbols.ContainsKey(currentLitName) || !Symbols.ContainsKey(replacementLitName))
            {
                throw new ArgumentException(Constants.BuildMessage(Constants.ERR_UPDT_SCOPE_VAR, new object[2] { currentLitName, replacementLitName }));
            }

            Literal currLLiteral = Symbols[currentLitName].Literal;
            Literal currRLiteral = Symbols[replacementLitName].Literal;
            if (currLLiteral.LiteralValueType != currRLiteral.LiteralValueType)
            {
                throw new ArgumentException(Constants.BuildMessage(Constants.SWAP_TYPE_MISSMATCH, new object[2] { currLLiteral.LiteralValueType, currRLiteral.LiteralValueType }));
            }



            UseLocal(ref lBackupLiteral, Symbols[currentLitName]);

            Literal rBackupLiteral = new Literal("", LiteralValueType.SIGNAL, null);
            if (!Symbols.ContainsKey(replacementLitName))
            {
                throw new ArgumentException(Constants.BuildMessage(Constants.ERR_UPDT_SCOPE_VAR, new object[2] { currentLitName, replacementLitName }));
            }
            UseLocal(ref rBackupLiteral, Symbols[replacementLitName]);

        }

        public void Remove(Literal literal)
        {
            if (Symbols.ContainsKey(literal.Name))
            {
                Symbols.Remove(literal.Name);
            }
        }

        // TODO: Update search to maybe take a literal as parameter as to not drop currently unresolvable expressions
        // (e.g. dimension expr, start expr, end expr, etc.)
        public Expression Search(string name)
        {
            Scope currScope = this;
            Expression scopeExpr = null;

            if (string.IsNullOrWhiteSpace(name)) return scopeExpr;

            while (currScope != null)
            {
                if (InverseReplacements.ContainsKey(name))
                {
                    name = InverseReplacements[name];
                }
                if (currScope.Symbols.ContainsKey(name))
                {
                    scopeExpr = new Expression(Operand.NONE);
                    CopyExpression(ref scopeExpr, currScope.Symbols[name]);
                }
                //scopeExpr = (currScope.Symbols.ContainsKey(name) ? CopyExpression(ref scopeExpr, currScope.Symbols[name]) : null); //currScope.Symbols[name] : null);
                if (scopeExpr != null)
                {
                    if (currScope.ContainsReplacement(name))
                    {
                        scopeExpr.Literal.Name = Replacements[name];
                    }
                    break;
                }
                else
                {
                    currScope = currScope.ParentScope;
                }
            }
            return scopeExpr;
        }

        private bool ContainsReplacement(string literalNameToReplace)
        {
            bool replacementFound = false;
            if (Replacements.ContainsKey(literalNameToReplace))
            {
                replacementFound = true;
            }
            else
            {
                Scope currScope = ParentScope;
                while (currScope != null && !replacementFound)
                {
                   replacementFound = ContainsReplacement(literalNameToReplace);
                }
            }
            return replacementFound;
        }

        public string AddReplacement(string literalNameToReplace)
        {
            if (ContainsReplacement(literalNameToReplace))
            {
                return Replacements[literalNameToReplace];
            }
            else
            {
                Replacements.Add(literalNameToReplace, Constants.REPLACEMENT_PREFIX + (replacementCnt++));
                InverseReplacements.Add(Replacements[literalNameToReplace], literalNameToReplace);
                return Replacements[literalNameToReplace];
            }
        }

        /*
        public void Update(Literal literal, Literal replacement)
        {
            if (literal == null || replacement == null)
            {
                string literalName = (literal == null || string.IsNullOrWhiteSpace(literal.Name)) ? "<null>" : literal.Name;
                string replacementName = (replacement == null || string.IsNullOrWhiteSpace(replacement.Name)) ? "<null>" : replacement.Name;
                throw new ArgumentException(Constants.BuildMessage(Constants.ERR_UPDT_SCOPE_VAR, new object[2] { literalName, replacementName }));
            }

            if (Symbols.ContainsKey(literal.Name))
            {
                Symbols[literal.Name].Literal = replacement;
            }
            else if (ParentScope != null)
            {
                ParentScope.Update(literal, replacement);
            }
            else
            {
                string literalName = (literal == null || string.IsNullOrWhiteSpace(literal.Name)) ? "<null>" : literal.Name;
                string replacementName = (replacement == null || string.IsNullOrWhiteSpace(replacement.Name)) ? "<null>" : replacement.Name;
                throw new ArgumentException(Constants.BuildMessage(Constants.ERR_UPDT_SCOPE_VAR, new object[2] { literalName, replacementName }));
            }
        }
        */

        public void UseLocal(ref Literal newLiteral, Expression scopeExpr)
        {
            if (scopeExpr == null || (scopeExpr.Literal != null && Search(scopeExpr.Literal.Name) == null)) 
            {
                string literalName = (scopeExpr.Literal == null || scopeExpr.Literal.Name == null) ? "null" : scopeExpr.Literal.Name;
                throw new ArgumentException(Constants.BuildMessage(Constants.ERR_ITEM_NOT_IN_SCOPE, literalName));
            }
            // TODO: Rework ugly manual copying of properties to new instance
            Literal scopeLiteral = scopeExpr.Literal;
            /*
            newLiteral.Name = scopeLiteral.Name;
            newLiteral.SignalWidth = scopeLiteral.SignalWidth;
            newLiteral.IsArrayType = scopeLiteral.IsArrayType;
            newLiteral.Dimension = scopeLiteral.Dimension;
            newLiteral.DimensionExpr = scopeLiteral.DimensionExpr;
            newLiteral.StartIdx = scopeLiteral.StartIdx;
            newLiteral.EndIdx = scopeLiteral.EndIdx;
            newLiteral.LiteralType = scopeLiteral.LiteralType;
            newLiteral.LiteralValueType = scopeLiteral.LiteralValueType;
            newLiteral.Values = new List<object>(scopeLiteral.Values).ToArray();
            */
            CopyLiteral(ref newLiteral, scopeLiteral);
        }

        public void IncrementLoopVariable(string loopVarLiteralName)
        {
            Expression literalExpr = Search(loopVarLiteralName);
            // TODO
            if (literalExpr == null || literalExpr.Literal == null
                || literalExpr.Operand != Operand.NONE
                || literalExpr.Literal.LiteralType != LiteralType.LOOP_VARIABLE)
            {
                throw new ArgumentException("No loop var found");
            }
            else
            {
                int increment = literalExpr.Literal.StepSize;
                if (!literalExpr.Literal.PositiveStepSize)
                {
                    increment *= -1;
                }
                literalExpr.Literal.Value = ((int) literalExpr.Literal.Value) + increment;
                Symbols[loopVarLiteralName].Literal.Value = literalExpr.Literal.Value;
            }
        }

        public Scope Close()
        {
            if (ParentScope == null)
            {
                Symbols.Clear();
                return this;
            }
            else
            {
                return ParentScope;
            }
        }
    
        private void CopyLiteral(ref Literal newLiteral, Literal scopeLiteral)
        {
            if (scopeLiteral == null) return;
            newLiteral.Name = scopeLiteral.Name;
            newLiteral.SignalWidth = scopeLiteral.SignalWidth;
            newLiteral.IsArrayType = scopeLiteral.IsArrayType;
            newLiteral.Dimension = scopeLiteral.Dimension;
            newLiteral.DimensionExpr = scopeLiteral.DimensionExpr;
            newLiteral.StartIdx = scopeLiteral.StartIdx;
            newLiteral.EndIdx = scopeLiteral.EndIdx;
            newLiteral.LiteralType = scopeLiteral.LiteralType;
            newLiteral.LiteralValueType = scopeLiteral.LiteralValueType;
            if (scopeLiteral.LiteralType == LiteralType.LOOP_VARIABLE)
            {
                newLiteral.StartValue = scopeLiteral.StartValue;
                newLiteral.EndValue = scopeLiteral.EndValue;
                newLiteral.StepSize = scopeLiteral.StepSize;
                newLiteral.PositiveStepSize = scopeLiteral.PositiveStepSize;
            }
            newLiteral.Values = new List<object>(scopeLiteral.Values).ToArray();
        }

        private void CopyExpression(ref Expression newExpr, Expression scopeExpr)
        {
            Literal newLiteral = newExpr.Literal;
            if (newLiteral == null && scopeExpr.Literal != null)
            {
                newLiteral = new Literal("", LiteralValueType.INTEGER, null);
            }
            if (newLiteral != null)
            {
                CopyLiteral(ref newLiteral, scopeExpr.Literal);
            }
            newExpr.LeftExpression = scopeExpr.LeftExpression;
            newExpr.RightExpression = scopeExpr.RightExpression;
            newExpr.Operand = scopeExpr.Operand;
            newExpr.HasNumericResult = scopeExpr.HasNumericResult;
            newExpr.RequiresBooleanOperands = scopeExpr.RequiresBooleanOperands;
            newExpr.Replacements = scopeExpr.Replacements;
            newExpr.Literal = newLiteral;
        }
    }
}
