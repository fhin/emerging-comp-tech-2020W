﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptimizerGenerator.ParserEntities
{
    public class Module
    {
        public string Name { get; set; }
        public IList<Parameter> FormalInputParameter { get; set; }
        public IList<Parameter> FormalOutputParameter { get; set; }
        public IList<Parameter> AdditionalSignals { get; set; }

        public Block Body { get; set; }
        public Scope Scope { get; set; }
        public Module(string name)
        {
            // TODO: Check for non empty module name
            Name = name;
            FormalInputParameter = new List<Parameter>();
            FormalOutputParameter = new List<Parameter>();
            AdditionalSignals = new List<Parameter>();
        }

        public override bool Equals(object obj)
        {
            try
            {
                return Equals((Module)obj);
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        // TODO: Override get hash code
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public bool Equals(Module other)
        {
            if (other == null || (Name != null && !Name.Equals(other.Name)) 
                || !FormalInputParameter.SequenceEqual(other.FormalInputParameter)
                || !FormalOutputParameter.SequenceEqual(other.FormalOutputParameter)
                || !AdditionalSignals.SequenceEqual(other.AdditionalSignals)) return false;
            return true;
        }

        public override string ToString()
        {
            bool trim = false;
            StringBuilder moduleSB = new StringBuilder(Constants.MODULE_IDENT + Constants.WHITESPACE +
                Name + Constants.R_BRACKET_OPEN);
            if (FormalInputParameter.Any())
            {
                trim = true;
                foreach (var iParam in FormalInputParameter) {
                    if (iParam.ParameterType == ParameterType.INOUT) continue;
                    moduleSB.Append(ParameterAsString(iParam) + Constants.PARAM_DIVIDER);
                }
            }
            if (FormalOutputParameter.Any())
            {
                trim = true;
                foreach (var oParam in FormalOutputParameter)
                {
                    //if (oParam.ParameterType == ParameterType.INOUT) continue;
                    moduleSB.Append(ParameterAsString(oParam) + Constants.PARAM_DIVIDER);
                }
            }
            if (trim)
            {
                moduleSB = moduleSB.Remove(moduleSB.Length-1, 1);
            }
            moduleSB.Append(Constants.R_BRACKET_CLOSE);
            trim = false;
            if (AdditionalSignals.Any())
            {
                var additionalParams = AdditionalSignals.Where(x => x.ParameterType == ParameterType.WIRE).AsEnumerable();
                if (additionalParams.Any())
                {
                    trim = true;
                    moduleSB.Append(Constants.NEWLINE + Constants.WIRE_PARAM_IDENT + Constants.WHITESPACE);
                    foreach (var wParam in additionalParams)
                    {
                        moduleSB.Append(ParameterAsString(wParam) + Constants.PARAM_DIVIDER);
                    }
                    if (trim)
                    {
                        moduleSB = moduleSB.Remove(moduleSB.Length - 1, 1);
                        moduleSB.Append(Constants.STATEMENT_DIVIDER);
                    }
                }
                additionalParams = AdditionalSignals.Where(x => x.ParameterType == ParameterType.STATE).AsEnumerable();
                if (additionalParams.Any())
                {
                    trim = true;
                    moduleSB.Append(Constants.NEWLINE + Constants.STATE_PARAM_IDENT + Constants.WHITESPACE);
                    foreach (var sParam in additionalParams)
                    {
                        moduleSB.Append(ParameterAsString(sParam) + Constants.PARAM_DIVIDER);
                    }
                    if (trim)
                    {
                        moduleSB = moduleSB.Remove(moduleSB.Length - 1, 1);
                        moduleSB.Append(Constants.STATEMENT_DIVIDER);
                    }
                }
            }
            moduleSB.Append(Constants.NEWLINE);
            moduleSB.Append(Body.ToString());
            return moduleSB.ToString();
        }

        private string ParameterAsString(Parameter parameter)
        {
            string parameterString;
            switch (parameter.ParameterType)
            {
                case ParameterType.IN:
                    parameterString = Constants.IN_PARAM_IDENT + Constants.WHITESPACE;
                    break;
                case ParameterType.OUT:
                    parameterString = Constants.OUT_PARAM_IDENT + Constants.WHITESPACE;
                    break;
                case ParameterType.INOUT:
                    parameterString = Constants.INOUT_PARAM_IDENT + Constants.WHITESPACE;
                    break;
                case ParameterType.STATE:
                case ParameterType.WIRE:
                    parameterString = "";
                    break;
                default:
                    // TOOD:
                    throw new System.Exception();
            }
            return parameterString + parameter.Literal.ToString(true);
        }
    }
}
