﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OptimizerGenerator.ParserEntities
{
    public class Expression
    {
        public Operand Operand { get; set; }
        public Expression LeftExpression { get; set; }
        public Expression RightExpression { get; set; }
        public Literal Literal { get; set; }
        public bool RequiresBooleanOperands { get; set; }
        public bool HasNumericResult { get; set; }
        public string ExprAsString { get; set; }
        public IDictionary<string, string> Replacements { get; set; }

        public Expression()
        {
            Operand = Operand.NONE;
            Replacements = new Dictionary<string, string>();
        }

        public Expression(Operand operand)
            : this()
        {
            switch (operand)
            {
                case Operand.AND:
                case Operand.OR:
                case Operand.NEG:
                    RequiresBooleanOperands = true;
                    HasNumericResult = false;
                    break;
                case Operand.GREATER:
                case Operand.GREATER_EQ:
                case Operand.LESS:
                case Operand.LESS_EQ:
                // Equality and Inequality can be used for both numeric as well as boolean values
                // Flags need to be set correctly in parser
                case Operand.EQ:
                case Operand.NEQ:
                case Operand.SWAP:
                case Operand.XOR:
                    RequiresBooleanOperands = false;
                    HasNumericResult = false;
                    break;
                default:
                    RequiresBooleanOperands = false;
                    HasNumericResult = true;
                    break;
            }
            Operand = operand;
        }

        /*
        public void AddReplacement(bool replaceLeftExpr, string replacementKey)
        {
            Expression replacementExpression;
            Expression expressionToReplace = replaceLeftExpr ? LeftExpression : RightExpression;

            replacementExpression = new Expression(Operand.NONE)
            {
                Literal = new Literal(replacementKey, LiteralValueType.SIGNAL, null),
                HasNumericResult = expressionToReplace.HasNumericResult,
                RequiresBooleanOperands = expressionToReplace.RequiresBooleanOperands
            };
            if (Replacements.ContainsKey(replacementKey))
            {
                throw new ArgumentException(Constants.BuildMessage(Constants.ERR_DUPLICATE_REPLACEMENT_KEY, replacementKey));
            }
            Replacements.Add(replacementKey, replaceLeftExpr ? LeftExpression : RightExpression);
            if (replaceLeftExpr)
            {
                LeftExpression = replacementExpression;
            }
            else
            {
                RightExpression = replacementExpression;
            }
        }
        */

        public override string ToString()
        {
            StringBuilder exprAsStringBuilder = new StringBuilder();
            //ConvertExpressionToString(this, ref exprAsStringBuilder, true);
            ConvertExpressionToString(this, ref exprAsStringBuilder);
            return exprAsStringBuilder.ToString();
        }

        private void ConvertExpressionToString(Expression expression, ref StringBuilder sb)
        {
            if (expression.Operand == Operand.SKIP)
            {
                sb.Append(Constants.LookupOperandAsString(Operand.SKIP));
                return;
            }
            if (expression.Operand == Operand.NONE && expression.Literal != null)
            {
                sb.Append(expression.Literal.ToString());
                return;
            }
            // TODO: 
            if (expression.LeftExpression == null) throw new ArgumentException("");
            switch (expression.Operand)
            {
                case Operand.NEG:
                case Operand.BIT_NEG:
                case Operand.INCR_EQ:
                case Operand.DECR_EQ:
                case Operand.NEG_UNARY_EQ:
                    sb.Append(Constants.LookupOperandAsString(expression.Operand));
                    ConvertExpressionToString(expression.LeftExpression, ref sb);
                    return;
                default:
                    break;
            }

            bool operandRequiresBrackets;
            switch (expression.Operand)
            {
                case Operand.NEG:
                case Operand.BIT_NEG:
                case Operand.INCR_EQ:
                case Operand.DECR_EQ:
                case Operand.NEG_UNARY_EQ:
                case Operand.ADD_EQ:
                case Operand.SUB_EQ:
                case Operand.XOR_EQ:
                case Operand.SWAP:
                    operandRequiresBrackets = false;
                    break;
                default:
                    operandRequiresBrackets = true;
                    break;
            }
            if (operandRequiresBrackets)
            {
                sb.Append("(");
            }
            ConvertExpressionToString(expression.LeftExpression, ref sb);
            sb.Append(Constants.LookupOperandAsString(expression.Operand));
            ConvertExpressionToString(expression.RightExpression, ref sb);
            if (operandRequiresBrackets)
            {
                sb.Append(")");
            }
            return;
        }

        public override bool Equals(object obj)
        {
            return obj is Expression expression && 
                   expression != null &&
                   Operand == expression.Operand &&
                   (LeftExpression != null ? LeftExpression.Equals(expression.LeftExpression) : (expression.LeftExpression != null ? false : true)) &&
                   (RightExpression != null ? RightExpression.Equals(expression.RightExpression) : (expression.RightExpression != null ? false : true)) &&
                   (Literal != null ? Literal.Equals(expression.Literal) : (expression.Literal != null ? false : true)) &&
                   RequiresBooleanOperands == expression.RequiresBooleanOperands &&
                   HasNumericResult == expression.HasNumericResult;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Operand, LeftExpression, RightExpression, Literal, RequiresBooleanOperands, HasNumericResult);
        }
    }
}
