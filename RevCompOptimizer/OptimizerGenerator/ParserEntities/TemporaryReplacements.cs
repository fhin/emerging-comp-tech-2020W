﻿using System.Drawing;

namespace OptimizerGenerator.ParserEntities
{
    public class TemporaryReplacements
    {
        public string Key { get; set; }
        public string ReplacedExpression { get; set; }
        public bool HasNumericResult { get; set; }

        public TemporaryReplacements(string key, string replacedExpr, bool hasNumericResult)
        {
            // TODO: Check for correct non-empty ctor parameters
            Key = key;
            ReplacedExpression = replacedExpr;
            HasNumericResult = hasNumericResult;
        }
    }
}
