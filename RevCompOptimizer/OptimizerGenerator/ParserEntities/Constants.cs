﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace OptimizerGenerator.ParserEntities
{
    public enum ParameterType
    {
        IN, OUT, INOUT, WIRE, STATE
    }

    public enum Operand
    {
        NONE, SKIP,
        // Arithmetic
        ADD, SUB, MUL, DIV, MOD, BIT_NEG, BIT_AND, BIT_OR,
        // Logical
        XOR, NEG, AND, OR,
        // Relational
        GREATER, LESS, GREATER_EQ, LESS_EQ, NEQ,
        // Shift
        SHIFT_LEFT, SHIFT_RIGHT,
        // Assignments
        EQ, INCR_EQ, DECR_EQ, SWAP, XOR_EQ, ADD_EQ, SUB_EQ, NEG_UNARY_EQ
    }

    public enum BlockType
    {
        CONDITION, LOOP, MODULE, INNER, TEMPORARY
    }

    public enum LiteralType
    {
        VARIABLE, LOOP_VARIABLE, PARAMETER
    }

    public enum LiteralValueType
    {
        BOOL, INTEGER, SIGNAL, RANGE, NUMERIC_CONSTANT
    }

    public static class Constants
    {
        public const string BITWIDTH_ACCESS_PREFIX = "#";
        public const string LOOP_VAR_ACCESS_PREFIX = "$";
        public const string REPLACEMENT_PREFIX = "$_r";
        public const string UNNAMED_LOOP_VAR_PREFIX = REPLACEMENT_PREFIX;
        public static readonly Regex REPLACEMENT_IDENT_REGEX = new Regex(@"\§(_r)(\d)+");
        public const string MODULE_IDENT = "module";
        public const string IN_PARAM_IDENT = "in";
        public const string OUT_PARAM_IDENT = "out";
        public const string INOUT_PARAM_IDENT = "inout";
        public const string WIRE_PARAM_IDENT = "wire";
        public const string SIGNAL_PARAM_IDENT = "signal";
        public const string STATE_PARAM_IDENT = "state";
        public const string IF_IDENT = "if";
        public const string THEN_IDENT = "then";
        public const string ELSE_IDENT = "else";
        public const string IF_END_IDENT = "fi";
        public const string LOOP_IDENT = "for";
        public const string STEP_IDENT = "step";
        public const string STEP_IDENT_POSTFIX = ":";
        public const string BIT_RANGE_PREFIX = STEP_IDENT_POSTFIX;
        public const string NEG_STEPSIZE = "-";
        public const string POS_STEPSIZE = "";
        public const string TO_IDENT = "to";
        public const string LOOP_HEADER_END = "do";
        public const string LOOP_END_IDENT = "rof";
        public const string WHITESPACE = " ";
        public const string NEWLINE = "\n";
        public const string R_BRACKET_OPEN = "(";
        public const string R_BRACKET_CLOSE = ")";
        public const string S_BRACKET_OPEN = "[";
        public const string S_BRACKET_CLOSE = "]";
        public const string BIT_ACCESS_IDENT = ".";
        public const string PARAM_DIVIDER = ",";
        public const string STATEMENT_DIVIDER = ";";
        public const string TMP_LOOP_DELIMITER = ":endl:";

        public const int DEFAULT_SIGNAL_WIDTH = 16;
        // Only a series of integers is allowed
        public const NumberStyles INTEGER_FORMAT = NumberStyles.None;
        public const string ERR_DUPLICATE_LOOP_VAR = "A loop variable with the given name {0} was already declared !";
        public const string ERR_DUPLICATE_MODULE = "Duplicate module {0} defined";
        public const string ERR_DUPLICATE_PARAMETER = "Duplicate parameter {0} defined !";
        public const string ERR_DUPLICATE_REPLACEMENT_KEY = "Duplicate replacement key {0} given !";
        public const string ERR_SIGNAL_WIDTH = "";
        public const string ERR_SIGNAL_DIMENSION = "";
        public const string ERR_OP_NOT_DEFINED_FOR_ZERO = "Given operation {0} is not defined for the given operand zero !";
        public const string ERR_TYPE_MISSMATCH = "Operation {0} requires operand[s] of type {1}";
        public const string ERR_NOT_INTEGER = "Expected number with format (\\d)+ and a value greater than zero !";
        public const string ERR_NOT_BOOLEAN = "Expected boolean value as either 'true' or 'false'";
        public const string ERR_UPDT_SCOPE_VAR = "Could not replace literal {0} in current scope with {1} !";
        public const string ERR_NO_OP_MAPPING = "There is no string representation for the given operand {0} !";
        public const string ERR_VAR_NOT_SIGNAL = "Given variable {0} is no signal or wire !";
        public const string ERR_VAR_NOT_ARRAY = "Given variable {0} is not of array type !";
        public const string ERR_ARRAY_DIM_ZERO = "An array signal must have a dimension greater zero !";
        public const string ERR_ARRAY_DIM_NOT_INT = "An array index must be a non-negative number !";
        public const string ERR_SIGNAL_WIDTH_ZERO = "A signal must have a width greater zero !";
        public const string ERR_ARRAY_IDX_OUT_OF_RANGE = "Given index {0} was to large for array of length {1} !";
        public const string ERR_IDX_OUT_OF_RANGE = "Given index {0} was out of range for signal with width {1} !";
        public const string ERR_RANGE_OUT_OF_RANGE = "Given range {0}:{1} was out of range for signal with width {2} !";
        public const string ERR_RANGE_INVALID = "Given range {0}:{1} was invalid, end index must be greater equal to the given start index !";
        public const string ERR_ITEM_NOT_IN_SCOPE = "Given item {0} was not declared in the current scope !";
        public const string ERR_IF_CONDITION_NOT_BOOL = "Result of the if condition must be a boolean !";
        public const string ERR_UNKNOWN_VAR = "Use of undeclared variable {0} !";
        public const string ERR_STEP_SIZE_ZERO = "Step size must be a non-zero value !";
        public const string ERR_LOOP_RANGE_INVALID = "Given range {0}:{1} was invalid, end value must be greater or equal to the startValue !";
        public const string ERR_LOOP_VAR_PREFIX = "A loop variable must be prefixed with the character $";
        public const string ERR_VAR_NOT_LOOP_VAR = "Given variable {0} is not a loop variable !";
        public const string ERR_IF_GUARDS_NOT_MATCHING = "Given guard expression ({0}) of if condition does not match its fi counterpart ({1}) !";
        public const string ERR_NOT_NUMBER = "";
        public const string SWAP_TYPE_MISSMATCH = "Can only swap literals of same type, given types where {0} <=> {1}";
        public const string ERR_ONLY_NUMBERS_ALLOWED = "Only numbers or bitwidth prefixed with " + BITWIDTH_ACCESS_PREFIX + " or for loop variables with " + LOOP_VAR_ACCESS_PREFIX;
        public const string ERR_GETTING_BITWIDTH = "Could not determine number of bits needed to store value {0}";
        public static string LookupOperandAsString(Operand operand)
        {
            string operandAsString;
            switch (operand)
            {
                case Operand.SKIP:
                    operandAsString = "skip";
                    break;
                case Operand.ADD:
                    operandAsString = "+";
                    break;
                case Operand.SUB:
                    operandAsString = "-";
                    break;
                case Operand.MUL:
                    operandAsString = "*";
                    break;
                case Operand.DIV:
                    operandAsString = "/";
                    break;
                case Operand.MOD:
                    operandAsString = "%";
                    break;
                case Operand.BIT_NEG:
                    operandAsString = "~";
                    break;
                case Operand.BIT_AND:
                    operandAsString = "&";
                    break;
                case Operand.BIT_OR:
                    operandAsString = "|";
                    break;
                case Operand.AND:
                    operandAsString = "&&";
                    break;
                case Operand.OR:
                    operandAsString = "||";
                    break;
                case Operand.XOR:
                    operandAsString = "^";
                    break;
                case Operand.NEG:
                    operandAsString = "!";
                    break;
                case Operand.GREATER:
                    operandAsString = ">";
                    break;
                case Operand.GREATER_EQ:
                    operandAsString = ">=";
                    break;
                case Operand.LESS:
                    operandAsString = "<";
                    break;
                case Operand.LESS_EQ:
                    operandAsString = "<=";
                    break;
                case Operand.SHIFT_LEFT:
                    operandAsString = "<<";
                    break;
                case Operand.SHIFT_RIGHT:
                    operandAsString = ">>";
                    break;
                case Operand.EQ:
                    operandAsString = "=";
                    break;
                case Operand.NEQ:
                    operandAsString = "!=";
                    break;
                case Operand.INCR_EQ:
                    operandAsString = "++=";
                    break;
                case Operand.DECR_EQ:
                    operandAsString = "--=";
                    break;
                case Operand.NEG_UNARY_EQ:
                    operandAsString = "~=";
                    break;
                case Operand.ADD_EQ:
                    operandAsString = "+=";
                    break;
                case Operand.SUB_EQ:
                    operandAsString = "-=";
                    break;
                case Operand.SWAP:
                    operandAsString = "<=>";
                    break;
                case Operand.XOR_EQ:
                    operandAsString = "^=";
                    break;
                default:
                    throw new ArgumentException(BuildMessage(ERR_NO_OP_MAPPING, operand));
            }
            return operandAsString;
        }

        public static string BuildMessage(string format, object args)
        {
            return string.Format(format, args);
        }

        public static string BuildMessage(string format, object[] args)
        {
            return string.Format(format, args);
        }
    }
}
