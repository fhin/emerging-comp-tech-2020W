﻿using System;
using System.Collections.Generic;

namespace OptimizerGenerator.ParserEntities
{
    public class Literal
    {
        public string Name { get; set; }
        public LiteralValueType LiteralValueType { get; set; }
        public LiteralType LiteralType { get; set; }
        // TODO:
        // Refactor Value property to array of properties for array signals
        // Non-array signals have array of size[1]
        // Integer properties
        public object[] Values { get; set; }

        public object Value
        {
            get
            {
                int idx = 0;
                if (DimensionExpr != null && DimensionExpr.Literal != null
                    && DimensionExpr.Operand == Operand.NONE && DimensionExpr.Literal.LiteralValueType != LiteralValueType.BOOL)
                {

                    idx = DimensionExpr.Literal.Value != null ? (int)DimensionExpr.Literal.Value : -1;
                }
                if (idx == -1) return null;
                return Values[idx];
            }
            set
            {
                int idx = 0;
                if (DimensionExpr != null && DimensionExpr.Literal != null
                    && DimensionExpr.Operand == Operand.NONE && DimensionExpr.Literal.Value != null
                    && DimensionExpr.Literal.LiteralValueType != LiteralValueType.BOOL)
                {
                    idx = (int)DimensionExpr.Literal.Value;
                }

                if (value == null)
                {
                    Values[idx] = null; 
                    return;
                }
                if (LiteralValueType == LiteralValueType.BOOL)
                {
                    try
                    {
                        Values[idx] = Convert.ToBoolean(value);
                    }
                    catch (InvalidCastException) { throw new ArgumentException(Constants.ERR_NOT_BOOLEAN); }
                    catch (FormatException) { throw new ArgumentException(Constants.ERR_NOT_BOOLEAN); }
                }
                else
                {
                    try
                    {
                        int valueAsInteger = Convert.ToInt32(value, null);
                        /* TODO: Uncomment check if only non-negative numeric values are allowed
                        if (valueAsInteger != -1)
                        {
                            Values[idx] = valueAsInteger;
                        }
                        */
                        Values[idx] = valueAsInteger;
                    }
                    catch (InvalidCastException) { throw new ArgumentException(Constants.ERR_NOT_INTEGER); }
                    catch (FormatException) { throw new ArgumentException(Constants.ERR_NOT_INTEGER); }
                    catch (OverflowException) { throw new ArgumentException(Constants.ERR_NOT_INTEGER); }
                }
            }
        }

        // Signal properties
        public bool IsArrayType { get; set; }
        public int Dimension { get; set; }
        public Expression DimensionExpr { get; set; }
        public int SignalWidth { get; set; }

        // Range Properties
        public int StartIdx { get; set; }
        public int EndIdx { get; set; }
        // Unevaluated range properties
        public Expression StartIdxExpr { get; set; }
        public Expression EndIdxExpr { get; set; }

        // Loop variable properties
        public int StartValue { get; set; }
        public Expression StartValueExpr { get; set; }
        public int EndValue { get; set; }
        public Expression EndValueExpr { get; set; }
        public int StepSize { get; set; }
        public Expression StepSizeExpr { get; set; }
        public bool PositiveStepSize { get; set; }
        public Literal(string name, LiteralValueType lType, object value)
        {
            // TODO: Check for correct ctor parameters
            Name = name;
            LiteralValueType = lType;
            SignalWidth = Constants.DEFAULT_SIGNAL_WIDTH;
            Values = new object[1];
            Value = value;
            Dimension = 1;
            StartIdx = -1;
            EndIdx = -1;
        }

        public string ToString(bool printSignalDef)
        {
            string literalAsString = Name;
            switch (LiteralValueType)
            {
                case LiteralValueType.SIGNAL:
                    if (DimensionExpr != null)
                    {
                        literalAsString += Constants.S_BRACKET_OPEN + DimensionExpr.ToString() + Constants.S_BRACKET_CLOSE;
                    }
                    if (printSignalDef)
                    {
                        if (IsArrayType)
                        {
                            literalAsString += Constants.S_BRACKET_OPEN + Dimension.ToString() + Constants.S_BRACKET_CLOSE;
                        }
                        literalAsString += Constants.R_BRACKET_OPEN + SignalWidth + Constants.R_BRACKET_CLOSE;
                    }
                    /*
                    if (IsArrayType)
                    {
                        if (printSignalDef)
                        {
                            literalAsString += Constants.S_BRACKET_OPEN + Dimension.ToString() + Constants.S_BRACKET_CLOSE;
                        }
                        else if (DimensionExpr != null)
                        {
                            literalAsString += Constants.S_BRACKET_OPEN +  DimensionExpr.ToString() + Constants.S_BRACKET_CLOSE;
                        }
                        //literalAsString += "[" + (printSignalDef ? Dimension.ToString() : DimensionExpr.ToString()) + "]";
                    }
                    if (printSignalDef)
                    {
                        literalAsString += Constants.R_BRACKET_OPEN + SignalWidth + Constants.R_BRACKET_CLOSE;
                    }
                    */
                    break;
                case LiteralValueType.RANGE:
                    if (DimensionExpr != null)
                    {
                        literalAsString += Constants.S_BRACKET_OPEN + DimensionExpr.ToString() + Constants.S_BRACKET_CLOSE;
                    }
                    else if (IsArrayType && printSignalDef)
                    {
                        literalAsString += Constants.S_BRACKET_OPEN + Dimension.ToString() + Constants.S_BRACKET_CLOSE;
                    }
                    /*
                    if (IsArrayType)
                    {
                        if (printSignalDef)
                        {
                            literalAsString += Constants.S_BRACKET_OPEN + Dimension.ToString() + Constants.S_BRACKET_CLOSE;
                        }
                        else if (DimensionExpr != null)
                        {
                            literalAsString += Constants.S_BRACKET_OPEN + DimensionExpr.ToString() + Constants.S_BRACKET_CLOSE;
                        }
                        //literalAsString += "[" + (printSignalDef ? Dimension.ToString() : DimensionExpr.ToString()) + "]";
                    }
                    */
                    literalAsString += Constants.BIT_ACCESS_IDENT;// ".";
                    if (StartIdxExpr != null)
                    {
                        literalAsString += StartIdxExpr.ToString() + Constants.BIT_RANGE_PREFIX;
                        literalAsString += (EndIdxExpr != null ? EndIdxExpr.ToString() : EndIdx.ToString());
                    }
                    else
                    {
                        literalAsString += StartIdx.ToString();
                        if (EndIdxExpr != null)
                        {
                            literalAsString += Constants.BIT_RANGE_PREFIX + EndIdxExpr.ToString();
                        }
                        else if (StartIdx != EndIdx)
                        {
                            literalAsString += Constants.BIT_RANGE_PREFIX + EndIdx;
                        }
                    }
                    break;
                case LiteralValueType.NUMERIC_CONSTANT:
                    try
                    {
                        literalAsString = Convert.ToInt32(Value).ToString();
                    }
                    catch (Exception)
                    {
                        literalAsString = "<null>";
                    }
                    break;
                default:
                    if (DimensionExpr != null)
                    {
                        literalAsString += Constants.S_BRACKET_OPEN + DimensionExpr.ToString() + Constants.S_BRACKET_CLOSE;
                    }
                    else if (IsArrayType && printSignalDef)
                    {
                        literalAsString += Constants.S_BRACKET_OPEN + Dimension.ToString() + Constants.S_BRACKET_CLOSE;
                    }

                    if (StartIdx > -1)
                    {
                        literalAsString += Constants.BIT_ACCESS_IDENT + StartIdx.ToString();
                    }
                    else if (StartIdxExpr != null)
                    {
                        literalAsString += Constants.BIT_ACCESS_IDENT + StartIdxExpr.ToString();
                    }
                    //literalAsString = Name;
                    break;
            }
            return literalAsString;
        }

        public override string ToString()
        {
            return ToString(false);
        }

        
        public override bool Equals(object obj)
        {
            bool equal = obj is Literal literal &&
                   Name == literal.Name &&
                   //LiteralValueType == literal.LiteralValueType &&
                   //EqualityComparer<object>.Default.Equals(Value, literal.Value) &&
                   IsArrayType == literal.IsArrayType &&
                   Dimension == literal.Dimension &&
                   EqualityComparer<Expression>.Default.Equals(DimensionExpr, literal.DimensionExpr) &&
                   SignalWidth == literal.SignalWidth &&
                   StartIdx == literal.StartIdx &&
                   EndIdx == literal.EndIdx &&
                   StartValue == literal.StartValue &&
                   EndValue == literal.EndValue &&
                   StepSize == literal.StepSize &&
                   PositiveStepSize == literal.PositiveStepSize;
            if (!equal) return false;
            else
            {
                Literal otherLiteral = (Literal) obj;
                if (Value == null || otherLiteral.Value == null)
                {
                    equal = Value == null & otherLiteral.Value == null;
                }
                else
                {
                    if (LiteralValueType == LiteralValueType.BOOL)
                    {
                        equal = ((bool)Value) == ((bool)otherLiteral.Value);
                    }
                    else
                    {
                        equal = ((int)Value) == ((int)otherLiteral.Value);
                    }
                }
                if (!equal) return false;
                if (LiteralValueType != LiteralValueType.BOOL && otherLiteral.LiteralValueType != LiteralValueType.BOOL) return true;
                else if (LiteralValueType == LiteralValueType.BOOL && otherLiteral.LiteralValueType == LiteralValueType.BOOL) return true;
                else return false;
            }
        }
        

        public override int GetHashCode()
        {
            HashCode hash = new HashCode();
            hash.Add(Name);
            hash.Add(LiteralType);
            hash.Add(Value);
            hash.Add(IsArrayType);
            hash.Add(Dimension);
            hash.Add(DimensionExpr);
            hash.Add(SignalWidth);
            hash.Add(StartIdx);
            hash.Add(EndIdx);
            hash.Add(StartValue);
            hash.Add(EndValue);
            hash.Add(StepSize);
            hash.Add(PositiveStepSize);
            return hash.ToHashCode();
        }
    }
}
