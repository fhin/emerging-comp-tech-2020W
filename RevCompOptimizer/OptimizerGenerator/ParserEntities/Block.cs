﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptimizerGenerator.ParserEntities
{
    public class Block
    {
        public BlockType BlockType { get; set; }
        // Stores expressions of if block, module body
        public IList<Expression> Expressions { get; set; }
        public Expression GuardExpr { get; set; }
        public Expression OtherExpr { get; set; }
        public IList<Expression> ElseExpressions { get; set; }
        public IList<Block> InnerBlocks { get; set; }
        public Literal LoopVariable { get; set; }

        public Block()
        {
            Expressions = new List<Expression>();
            ElseExpressions = new List<Expression>();
            InnerBlocks = new List<Block>();
        }

        public Block(BlockType blockType)
            :this()
        {
            BlockType = blockType;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            BuildStringRepresentation(ref sb, this);
            return sb.ToString();
        }

        public string PrintWithoutHeader()
        {
            if (BlockType == BlockType.CONDITION) return "";
            IEnumerator<Block> blockIterator = InnerBlocks.GetEnumerator();
            StringBuilder sb = new StringBuilder();
            switch (BlockType)
            {
                case BlockType.LOOP:
                case BlockType.MODULE:
                case BlockType.TEMPORARY:
                    PrintExpressions(ref sb, Expressions.ToList(), ref blockIterator);
                    break;
                default:
                    break;
            }
            return sb.ToString();
         }

        public static int GetExpressionCount(Block currentBlock)
        {
            IEnumerator<Block> currblockEnumerator = currentBlock.InnerBlocks.GetEnumerator();
            int cnt = CountExpressions(currentBlock.Expressions, ref currblockEnumerator);

            if (currentBlock.BlockType == BlockType.CONDITION)
            {
                cnt += CountExpressions(currentBlock.ElseExpressions, ref currblockEnumerator);
            }
            return cnt;
        }

        private void BuildStringRepresentation(ref StringBuilder sb, Block blockToPrint)
        {
            IEnumerator<Block> blockIterator = blockToPrint.InnerBlocks.GetEnumerator();
            switch (blockToPrint.BlockType)
            {
                case BlockType.CONDITION:
                    sb.Append(Constants.IF_IDENT + Constants.WHITESPACE +
                        blockToPrint.GuardExpr.ToString() + Constants.WHITESPACE + Constants.THEN_IDENT + Constants.WHITESPACE + Constants.NEWLINE);
                    PrintExpressions(ref sb, blockToPrint.Expressions.ToList(), ref blockIterator);

                    if (blockToPrint.ElseExpressions.Any())
                    {
                        sb.Append(Constants.ELSE_IDENT + Constants.WHITESPACE + Constants.NEWLINE);
                        PrintExpressions(ref sb, blockToPrint.ElseExpressions.ToList(), ref blockIterator);
                    }
                    sb.Append(Constants.NEWLINE + Constants.IF_END_IDENT + Constants.WHITESPACE + blockToPrint.OtherExpr.ToString());
                    break;
                case BlockType.LOOP:
                    sb.Append(Constants.LOOP_IDENT + Constants.WHITESPACE);
                    if (!string.IsNullOrWhiteSpace(blockToPrint.LoopVariable.Name))
                    {
                        // TOOD:
                        sb.Append(" $" + blockToPrint.LoopVariable.Name + " = ");
                    }
                    if (blockToPrint.LoopVariable.StartValue != blockToPrint.LoopVariable.EndValue)
                    {
                        sb.Append(blockToPrint.LoopVariable.StartValue + Constants.WHITESPACE + Constants.TO_IDENT + Constants.WHITESPACE);
                    }
                    sb.Append(blockToPrint.LoopVariable.EndValue);
                    sb.Append(Constants.STEP_IDENT + Constants.WHITESPACE + Constants.STEP_IDENT_POSTFIX
                        + (blockToPrint.LoopVariable.PositiveStepSize ? Constants.POS_STEPSIZE : Constants.NEG_STEPSIZE)
                        + blockToPrint.LoopVariable.StepSize);

                    sb.Append(Constants.WHITESPACE + Constants.LOOP_HEADER_END + Constants.WHITESPACE + Constants.NEWLINE);
                    PrintExpressions(ref sb, blockToPrint.Expressions.ToList(), ref blockIterator);
                    sb.Append(Constants.WHITESPACE + Constants.LOOP_END_IDENT);
                    break;
                case BlockType.MODULE:
                    PrintExpressions(ref sb, blockToPrint.Expressions.ToList(), ref blockIterator);
                    break;
                default:
                    // TODO:
                    throw new ArgumentException("Unknown block type");
            }
        }

        private static int CountExpressions(IEnumerable<Expression> expressions, ref IEnumerator<Block> blockEnumerator)
        {
            int cnt = 0;
            foreach (var expr in expressions)
            {
                if (expr != null)
                {
                    cnt++;
                }
                else if (blockEnumerator.MoveNext())
                {
                    // TODO: Fix correct calculation of number of expressions in block (e.g. after unrolling the loop)
                    //cnt += GetExpressionCount(blockEnumerator.Current);
                    cnt++;
                }
            }
            return cnt;
        }

        private void PrintExpressions(ref StringBuilder sb, List<Expression> expressionList, ref IEnumerator<Block> blockIterator)
        {
            for (int i = 0; i < expressionList.Count; i++)
            {
                if (expressionList[i] != null)
                {
                    sb.Append(expressionList[i].ToString());
                }
                else if (blockIterator.MoveNext())
                {
                    BuildStringRepresentation(ref sb, blockIterator.Current);
                }
                sb.Append(Constants.STATEMENT_DIVIDER);
                if (i+1 < expressionList.Count)
                {
                    sb.Append(Constants.NEWLINE);
                }
            }
        }

        // TODO: Rework to nicer solution
        public override bool Equals(object obj)
        {
            return obj is Block b &&
                BlockType == b.BlockType && Expressions != null && b.Expressions != null
                && Expressions.SequenceEqual<Expression>(b.Expressions)
                && ((ElseExpressions != null & b.ElseExpressions != null) ? ElseExpressions.SequenceEqual(b.ElseExpressions) : !(ElseExpressions != null ^ b.ElseExpressions != null))
                && ((GuardExpr != null & b.GuardExpr != null) ? GuardExpr.Equals(b.GuardExpr) : !(GuardExpr != null ^ b.GuardExpr != null))
                && ((OtherExpr != null & b.OtherExpr != null) ? OtherExpr.Equals(b.OtherExpr) : !(OtherExpr != null ^ b.OtherExpr != null))
                && ((LoopVariable != null & b.LoopVariable != null) ? LoopVariable.Equals(b.LoopVariable) : !(LoopVariable != null ^ b.LoopVariable != null));
        }

        // TODO: Implement correct override
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
