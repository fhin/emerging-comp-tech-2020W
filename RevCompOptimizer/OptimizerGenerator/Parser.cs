using System.IO;
using System.Linq;
using System.Collections.Generic;
using OptimizerGenerator.ParserEntities;



using System;



public class Parser {
	public const int _EOF = 0;
	public const int _ident = 1;
	public const int _number = 2;
	public const int _loopVarPF = 3;
	public const int maxT = 55;

	const bool _T = true;
	const bool _x = false;
	const int minErrDist = 2;
	
	public Scanner scanner;
	public Errors  errors;

	public Token t;    // last recognized token
	public Token la;   // lookahead token
	int errDist = minErrDist;

public IList<Module> definedModules;
private IDictionary<string, IList<Module>> _definedModules;
private Scope _currScope;
private Stack<Block> _blockStack = new Stack<Block>();
private Stack<string> _loopVarStack = new Stack<string>();
private ISet<string> _currLoopVarSet = new HashSet<string>();
private int _unnamedLoopVarCnt = 0;

public MemoryStream stream;

private bool IsSubstitutionDelayed() {
	return _currLoopVarSet.Count > 0;
}

private int GetBitWidthOfValue(int value){
	if (value == 0) return 0;

	try {
		return (int) Math.Round(Math.Log(value, 2));
	} catch (Exception) {
		SemErr(Constants.BuildMessage(Constants.ERR_GETTING_BITWIDTH, value));
	}
	return 0;
}

private string CurrLoopVar() {
	if (_loopVarStack.Count == 0) {
		return string.Empty;
	} else {
		return _loopVarStack.Peek();
	}
}

private void AppendBlockToParent(Block childBlock, ref Block parentBlock, bool isElseBlock)
{
    if (childBlock == null) return;
    IEnumerator<Block> childBlocksEnumerator = childBlock.InnerBlocks.GetEnumerator();
    foreach (var expr in childBlock.Expressions)
    {
        if (isElseBlock)
        {
            parentBlock.ElseExpressions.Add(expr);
        }
        else
        {
            parentBlock.Expressions.Add(expr);
        }
        if (expr == null && childBlocksEnumerator.MoveNext())
        {
            parentBlock.InnerBlocks.Add(childBlocksEnumerator.Current);
        }
    }
    foreach (var expr in childBlock.ElseExpressions)
    {
        if (isElseBlock)
        {
            parentBlock.ElseExpressions.Add(expr);
        }
        else
        {
            parentBlock.Expressions.Add(expr);
        }
        if (expr == null && childBlocksEnumerator.MoveNext())
        {
            parentBlock.InnerBlocks.Add(childBlocksEnumerator.Current);
        }
    }
}

private Block CurrBlock() {	
	if (_blockStack.Count == 0) {
		return null; 
	} else {
		return _blockStack.Peek();		
	}
}

private void WriteToInputStream(Block block, int numIterations) {
	if (numIterations == 0) return;
	string blockRepresentation = block.PrintWithoutHeader() + Constants.TMP_LOOP_DELIMITER + Constants.STATEMENT_DIVIDER;
        string postfix = Constants.WHITESPACE + Constants.LOOP_END_IDENT + Constants.STATEMENT_DIVIDER;
        if (!string.IsNullOrWhiteSpace(blockRepresentation))
        {
            blockRepresentation = new System.Text.StringBuilder(blockRepresentation.Length * numIterations + postfix.Length)
                .Insert(0, blockRepresentation, numIterations)
                .Append(postfix).ToString();
        }
        byte[] blockContent = System.Text.Encoding.ASCII.GetBytes(blockRepresentation);
		long copiedBytes = stream.Length - scanner.buffer.Pos;

		using (MemoryStream backupStream = new MemoryStream())
		{
			stream.Seek(-(stream.Length - scanner.buffer.Pos), SeekOrigin.Current);
			stream.CopyTo(backupStream);
			backupStream.Seek(0, SeekOrigin.Begin);

			stream.Seek(0, SeekOrigin.Begin);
			stream.Write(blockContent, 0, blockContent.Length);
			backupStream.CopyTo(stream);
		}
		long newStreamLength = blockContent.Length + copiedBytes;
		if (newStreamLength < stream.Length)
		{
			for (long i = 0; i < stream.Length - newStreamLength; i++)
			{
				stream.WriteByte(0x0);
			}
		}
		stream.Seek(blockContent.Length, SeekOrigin.Begin);
		stream.SetLength(newStreamLength);
        scanner = new Scanner(stream);
        Get();
}

private Expression GetValueForVariable(){
	int numberVal = 0;
	bool isConstantValue = false;
	string numberLiteralName = "";
	Expression expr = null;
	if (int.TryParse(t.val, Constants.INTEGER_FORMAT, null, out numberVal)){
		numberLiteralName = t.val;
		isConstantValue = true;
	}
	else if (t.val.StartsWith(Constants.BITWIDTH_ACCESS_PREFIX) || t.val.StartsWith(Constants.LOOP_VAR_ACCESS_PREFIX)) {
		string searchIdent = t.val.StartsWith(Constants.BITWIDTH_ACCESS_PREFIX) ? t.val.Substring(1, t.val.Length - 1) : t.val;
		expr = _currScope.Search(searchIdent);
		if (expr != null && expr.Operand == Operand.NONE) {
			if (t.val.StartsWith(Constants.BITWIDTH_ACCESS_PREFIX)) {
				if (expr.Literal.LiteralValueType == LiteralValueType.SIGNAL) {
					numberLiteralName = expr.Literal.SignalWidth.ToString();
					numberVal = expr.Literal.SignalWidth;
					isConstantValue = true;
				}
				else {
					SemErr(Constants.BuildMessage(Constants.ERR_VAR_NOT_SIGNAL, numberLiteralName));
				}
			}
			else {
				if (expr.Literal.LiteralType == LiteralType.LOOP_VARIABLE) {
					if (_currLoopVarSet.Contains(expr.Literal.Name)){ //|| IsSubstitutionDelayed()) {
						expr.Literal.Value = null;
						return expr;
					}
					numberLiteralName = expr.Literal.Value.ToString();
					numberVal = (int) expr.Literal.Value;
					isConstantValue = true;
				}
				else {
					SemErr(Constants.BuildMessage(Constants.ERR_VAR_NOT_LOOP_VAR, numberLiteralName));
				}
			}
		}
		else {
			SemErr(Constants.BuildMessage(Constants.ERR_UNKNOWN_VAR, t.val.Substring(1, t.val.Length-1)));
		}
	}
	else {
		SemErr(Constants.ERR_ONLY_NUMBERS_ALLOWED);
	}
	//if (!isConstantValue) return null;
	if (!isConstantValue) return expr;
	Literal numberLiteral = new Literal(numberLiteralName, LiteralValueType.INTEGER, numberVal);
	expr = _currScope.Search(numberLiteral.Name);
	if (expr == null) {
		_currScope.AddItem(numberLiteral);
		expr = _currScope.Search(numberLiteral.Name);
	}
	return expr;
}

private bool IsLoopVarPrefix() {
	scanner.ResetPeek();
	return (scanner.Peek().val.Equals("="));
}



	public Parser(Scanner scanner) {
		this.scanner = scanner;
		errors = new Errors();
	}

	void SynErr (int n) {
		if (errDist >= minErrDist) errors.SynErr(la.line, la.col, n);
		errDist = 0;
	}

	public void SemErr (string msg) {
		if (errDist >= minErrDist) errors.SemErr(t.line, t.col, msg);
		errDist = 0;
	}
	
	void Get () {
		for (;;) {
			t = la;
			la = scanner.Scan();
			if (la.kind <= maxT) { ++errDist; break; }

			la = t;
		}
	}
	
	void Expect (int n) {
		if (la.kind==n) Get(); else { SynErr(n); }
	}
	
	bool StartOf (int s) {
		return set[s, la.kind];
	}
	
	void ExpectWeak (int n, int follow) {
		if (la.kind == n) Get();
		else {
			SynErr(n);
			while (!StartOf(follow)) Get();
		}
	}


	bool WeakSeparator(int n, int syFol, int repFol) {
		int kind = la.kind;
		if (kind == n) {Get(); return true;}
		else if (StartOf(repFol)) {return false;}
		else {
			SynErr(n);
			while (!(set[syFol, kind] || set[repFol, kind] || set[0, kind])) {
				Get();
				kind = la.kind;
			}
			return StartOf(syFol);
		}
	}

	
	void signal(out Literal signalLiteral, in bool allowSimplification) {
		Expression dimExpr = new Expression(Operand.NONE); bool subSetAccess = false;
		bool isConstantBitAccess = false, isConstantRangeEnd = false;
		
		Expect(1);
		signalLiteral = new Literal(t.val, LiteralValueType.SIGNAL, null);
		Expression scopeExpr = _currScope.Search(signalLiteral.Name); 
		if (scopeExpr != null){
		_currScope.UseLocal(ref signalLiteral, scopeExpr);
		if (signalLiteral.LiteralType == LiteralType.LOOP_VARIABLE 
		&& _currLoopVarSet.Contains(signalLiteral.Name)){
		signalLiteral.Value = null;
		}
		if (signalLiteral.Value != null && IsSubstitutionDelayed()){
		signalLiteral.Value = null;
		}
		}
		else {
		SemErr(Constants.BuildMessage(Constants.ERR_ITEM_NOT_IN_SCOPE, t.val));
		}
		
		while (la.kind == 4) {
			Get();
			if (!signalLiteral.IsArrayType){
			SemErr(Constants.BuildMessage(Constants.ERR_VAR_NOT_ARRAY, signalLiteral.Name));
			}
			
			expression(out dimExpr);
			subSetAccess = true;
			int dimension = 0;
			if (!dimExpr.HasNumericResult){
			SemErr(Constants.ERR_ARRAY_DIM_NOT_INT);
			dimExpr = new Expression(Operand.NONE) {
			Literal = new Literal(0.ToString(), LiteralValueType.INTEGER, 0)
			};
			}
			else if (dimExpr.Operand == Operand.NONE && dimExpr.Literal != null) {
			if (dimExpr.Literal.LiteralValueType == LiteralValueType.INTEGER || dimExpr.Literal.LiteralValueType == LiteralValueType.NUMERIC_CONSTANT){
			if (dimExpr.Literal.Value != null) {
			try {
			dimension = Convert.ToInt32(dimExpr.Literal.Value);
			if (dimension >= signalLiteral.Dimension) {
			SemErr(Constants.BuildMessage(Constants.ERR_ARRAY_IDX_OUT_OF_RANGE, new object[2] {dimension, signalLiteral.Dimension}));
			dimension = 0;
			}
			else if (dimension < 0){
			SemErr(Constants.ERR_ARRAY_DIM_NOT_INT);
			dimension = 0;
			}
			else if (allowSimplification && signalLiteral.Values[dimension] != null){
			dimExpr = null;
			signalLiteral.Value = signalLiteral.Values[dimension];
			}
			} catch (Exception) {
			SemErr(Constants.ERR_NOT_INTEGER);
			}
			if (dimExpr != null) {
			dimExpr = new Expression(Operand.NONE);
			dimExpr.Literal = new Literal(dimension.ToString(), LiteralValueType.INTEGER, dimension);
			}
			}
			}
			
			}
			signalLiteral.LiteralValueType = LiteralValueType.SIGNAL;
			signalLiteral.IsArrayType = false;
			signalLiteral.DimensionExpr = dimExpr;
			
			Expect(5);
		}
		if (la.kind == 6) {
			Get();
			if (signalLiteral.LiteralValueType != LiteralValueType.SIGNAL && signalLiteral.LiteralValueType != LiteralValueType.RANGE){
			SemErr(Constants.BuildMessage(Constants.ERR_VAR_NOT_SIGNAL, signalLiteral.Name));
			}
			
			Expect(2);
			subSetAccess = true;
			int startIdx = 0;
			Expression bitAccessExpr = GetValueForVariable();
			if (bitAccessExpr != null && bitAccessExpr.Literal != null){
			if (bitAccessExpr.Literal.Value != null) {
			try
			{
			startIdx = Convert.ToInt32(bitAccessExpr.Literal.Value, null);
			isConstantBitAccess = true;
			}
			catch (InvalidCastException) { SemErr(Constants.ERR_NOT_INTEGER); }
			catch (FormatException) { SemErr(Constants.ERR_NOT_INTEGER); }
			catch (OverflowException) { SemErr(Constants.ERR_NOT_INTEGER); }
			}
			else {
			signalLiteral.StartIdxExpr = bitAccessExpr;
			startIdx = -1;
			signalLiteral.Value = null;
			}
			}
			if (isConstantBitAccess) {
			if (signalLiteral.LiteralValueType == LiteralValueType.SIGNAL && startIdx >= signalLiteral.SignalWidth){
			SemErr(Constants.BuildMessage(Constants.ERR_IDX_OUT_OF_RANGE, new object[2]{startIdx, signalLiteral.SignalWidth}));
			}
			else if (signalLiteral.LiteralValueType == LiteralValueType.RANGE && startIdx > signalLiteral.EndIdx) {
			SemErr(Constants.BuildMessage(Constants.ERR_IDX_OUT_OF_RANGE, new object[2]{startIdx, signalLiteral.EndIdx - signalLiteral.StartIdx + 1}));
			}
			}
			signalLiteral.LiteralValueType = LiteralValueType.INTEGER;
			signalLiteral.StartIdx = startIdx;
			signalLiteral.EndIdx = startIdx;
			
			if (la.kind == 7) {
				Get();
				Expect(2);
				int endIdx = 0; subSetAccess = true;
				Expression rangeEndAccessExpr = GetValueForVariable();
				if (rangeEndAccessExpr != null && rangeEndAccessExpr.Literal != null){
				if (rangeEndAccessExpr.Literal.Value != null) {
				try
				{
				endIdx = Convert.ToInt32(rangeEndAccessExpr.Literal.Value, null);
				isConstantRangeEnd = true;
				}
				catch (InvalidCastException) { SemErr(Constants.ERR_NOT_INTEGER); }
				catch (FormatException) { SemErr(Constants.ERR_NOT_INTEGER); }
				catch (OverflowException) { SemErr(Constants.ERR_NOT_INTEGER); }
				}
				else {
				signalLiteral.EndIdxExpr = rangeEndAccessExpr;
				endIdx = -1;
				signalLiteral.Value = null;
				}
				}
				
				LiteralValueType newLiteralValueType = LiteralValueType.RANGE;
				if (isConstantBitAccess & isConstantRangeEnd)
				{
				if (endIdx < startIdx)
				{
				SemErr(Constants.BuildMessage(Constants.ERR_RANGE_INVALID, new object[2] { startIdx, endIdx }));
				}
				if (signalLiteral.LiteralValueType == LiteralValueType.RANGE && endIdx > signalLiteral.EndIdx)
				{
				SemErr(Constants.BuildMessage(Constants.ERR_RANGE_OUT_OF_RANGE, new object[3] { startIdx, endIdx, (signalLiteral.EndIdx - signalLiteral.StartIdx + 1) }));
				}
				if ((signalLiteral.LiteralValueType == LiteralValueType.INTEGER || signalLiteral.LiteralValueType == LiteralValueType.SIGNAL)
				&& endIdx >= signalLiteral.SignalWidth)
				{
				SemErr(Constants.BuildMessage(Constants.ERR_RANGE_OUT_OF_RANGE, new object[3] { startIdx, endIdx, signalLiteral.SignalWidth }));
				}
				if (startIdx == endIdx)
				{
				newLiteralValueType = LiteralValueType.INTEGER;
				}
				}
				signalLiteral.EndIdx = endIdx;
				signalLiteral.LiteralValueType = newLiteralValueType;
				
			}
			signalLiteral.StartIdx = startIdx; 
		}
		object signalLiteralValue = signalLiteral.Value;
		
		if (!allowSimplification || !subSetAccess || signalLiteral == null 
		|| signalLiteralValue == null 
		|| (!isConstantBitAccess & !isConstantRangeEnd)) return;
		
		if (signalLiteral.DimensionExpr != null) {
		if (signalLiteral.DimensionExpr.Literal != null && signalLiteral.DimensionExpr.Literal.Value != null){
		signalLiteralValue = signalLiteral.Values[(int) signalLiteral.DimensionExpr.Literal.Value];
		signalLiteral.DimensionExpr = null;
		}
		}
		int valueAsInt = (int) signalLiteralValue;
		switch (signalLiteral.LiteralValueType){
		case LiteralValueType.INTEGER:
		signalLiteral.Value = (valueAsInt & (1 << signalLiteral.StartIdx)) >> signalLiteral.StartIdx;
		signalLiteral.StartIdx = 0;
		signalLiteral.EndIdx = 0;
		break;
		case LiteralValueType.SIGNAL:
		signalLiteral.Value = signalLiteralValue;
		break;
		case LiteralValueType.RANGE:
		uint tmpResult = ((uint)valueAsInt) & ((uint)~(int.MaxValue << (signalLiteral.EndIdx + 1)));
		tmpResult >>= signalLiteral.StartIdx;
		// TODO: Handling exception ?
		valueAsInt = unchecked((int)tmpResult);
		signalLiteral.Value = valueAsInt;
		signalLiteral.LiteralValueType = LiteralValueType.INTEGER;
		signalLiteral.StartIdx = 0;
		signalLiteral.EndIdx = 0;
		break;
		default:
		return;
		}
		
	}

	void expression(out Expression expr) {
		expr = new Expression(Operand.NONE); Expression lExpr; Literal signalLiteral;	
		if (la.kind == 2) {
			Get();
			expr = GetValueForVariable(); 
		} else if (la.kind == 1) {
			signal(out signalLiteral, (true & !IsSubstitutionDelayed()));
			if (signalLiteral.Value != null){
			signalLiteral = new Literal(signalLiteral.Value.ToString(), LiteralValueType.INTEGER, signalLiteral.Value);
			}
			expr = new Expression(Operand.NONE); 
			expr.HasNumericResult = signalLiteral.LiteralValueType != LiteralValueType.BOOL;
			expr.Literal = signalLiteral;
			
		} else if (la.kind == 8) {
			Get();
			expression(out lExpr);
			if (StartOf(1)) {
				binary_expression(lExpr, out expr);
			} else if (la.kind == 29 || la.kind == 30) {
				shift_expression(lExpr, out expr);
			} else SynErr(56);
		} else if (la.kind == 27 || la.kind == 28) {
			unary_expression(out expr);
		} else SynErr(57);
	}

	void binary_expression(in Expression lExpr, out Expression headExpr) {
		Expression rExpr; 
		Operand bOperand = Operand.NONE;
		
		switch (la.kind) {
		case 9: {
			Get();
			bOperand = Operand.ADD; 
			break;
		}
		case 10: {
			Get();
			bOperand = Operand.SUB; 
			break;
		}
		case 11: {
			Get();
			bOperand = Operand.XOR; 
			break;
		}
		case 12: {
			Get();
			bOperand = Operand.MUL; 
			break;
		}
		case 13: {
			Get();
			bOperand = Operand.DIV; 
			break;
		}
		case 14: {
			Get();
			bOperand = Operand.MOD; 
			break;
		}
		case 15: {
			Get();
			bOperand = Operand.NONE; 
			break;
		}
		case 16: {
			Get();
			bOperand = Operand.AND; 
			break;
		}
		case 17: {
			Get();
			bOperand = Operand.OR; 
			break;
		}
		case 18: {
			Get();
			bOperand = Operand.BIT_AND; 
			break;
		}
		case 19: {
			Get();
			bOperand = Operand.BIT_OR; 
			break;
		}
		case 20: {
			Get();
			bOperand = Operand.LESS; 
			break;
		}
		case 21: {
			Get();
			bOperand = Operand.GREATER; 
			break;
		}
		case 22: {
			Get();
			bOperand = Operand.EQ; 
			break;
		}
		case 23: {
			Get();
			bOperand = Operand.NEQ; 
			break;
		}
		case 24: {
			Get();
			bOperand = Operand.LESS_EQ; 
			break;
		}
		case 25: {
			Get();
			bOperand = Operand.GREATER_EQ; 
			break;
		}
		default: SynErr(58); break;
		}
		expression(out rExpr);
		Expect(26);
		headExpr = new Expression(bOperand);
		if (headExpr.Operand == Operand.EQ || headExpr.Operand == Operand.NEQ || headExpr.Operand == Operand.XOR) {
		if (lExpr.HasNumericResult && !rExpr.HasNumericResult) {
		SemErr(Constants.BuildMessage(Constants.ERR_TYPE_MISSMATCH, new object[2] { bOperand, "NUMERIC" }));
		}
		else if (!lExpr.HasNumericResult && rExpr.HasNumericResult) {
		SemErr(Constants.BuildMessage(Constants.ERR_TYPE_MISSMATCH, new object[2] { bOperand, "BOOLEAN" }));
		}
		}
		else {										
		if (headExpr.RequiresBooleanOperands) {
		if ((lExpr.HasNumericResult | rExpr.HasNumericResult)) {
		SemErr(Constants.BuildMessage(Constants.ERR_TYPE_MISSMATCH, new object[2] { bOperand, "BOOLEAN" }));
		}
		}
		else {
		if (!(lExpr.HasNumericResult & rExpr.HasNumericResult)) {
		SemErr(Constants.BuildMessage(Constants.ERR_TYPE_MISSMATCH, new object[2] { bOperand, "NUMERIC" }));	
		}
		}
		}
		// Try short circuit evalution if possible
		if (headExpr.RequiresBooleanOperands) {
		bool leftGiven = false;
		bool rightGiven = false;
		if (lExpr.Operand == Operand.NONE && lExpr.Literal.LiteralValueType == LiteralValueType.BOOL){
		leftGiven = true;
		}
		else if (rExpr.Operand == Operand.NONE && rExpr.Literal.LiteralValueType == LiteralValueType.BOOL){
		rightGiven = true;
		}
		if (leftGiven ^ rightGiven) {
		bool simplifyResult = false;
		switch (bOperand) {
		case Operand.OR:
		if ((leftGiven && ((bool) lExpr.Literal.Value) == true)
		|| (rightGiven && ((bool) rExpr.Literal.Value) == true)){
		bOperand = Operand.NONE;	
		simplifyResult = true;
		}
		else if (leftGiven && rightGiven) {
		bOperand =  Operand.NONE;
		simplifyResult = ((bool) lExpr.Literal.Value) | ((bool) rExpr.Literal.Value);
		}
		break;
		case Operand.AND:
		if ((leftGiven && ((bool) lExpr.Literal.Value) == false)
		|| (rightGiven && ((bool) rExpr.Literal.Value) == false)){
		bOperand = Operand.NONE;	
		simplifyResult = false;
		}
		else if (leftGiven && rightGiven) {
		bOperand =  Operand.NONE;
		simplifyResult = ((bool) lExpr.Literal.Value) & ((bool) rExpr.Literal.Value);
		}
		break;
		default:
		break;
		}
		headExpr = new Expression(Operand.NONE){
		Literal = new Literal(simplifyResult.ToString(), LiteralValueType.BOOL, simplifyResult)
		};
		return;
		}
		}
		else {
		bool leftConstant = false; bool rightConstant = false;
		if (lExpr.Operand == Operand.NONE && lExpr.Literal.Value != null) {
		leftConstant = true;
		}
		if (rExpr.Operand == Operand.NONE && rExpr.Literal.Value != null) {
		rightConstant = true;
		}
		if (leftConstant & rightConstant) {
		bool isResultBoolean = false; int integerSimplifyResult = 0; bool boolSimplifyResult = false;
		int leftVal = (int) lExpr.Literal.Value; int rightVal = (int) rExpr.Literal.Value;
		switch (bOperand) {
		case Operand.ADD:
		integerSimplifyResult = leftVal + rightVal;
		break;
		case Operand.SUB:
		integerSimplifyResult = leftVal - rightVal;
		break;
		case Operand.XOR:
		integerSimplifyResult = leftVal ^ rightVal;
		break;
		case Operand.MUL:
		integerSimplifyResult = leftVal * rightVal;
		break;
		case Operand.DIV:
		if (rightVal == 0) {
		SemErr(Constants.BuildMessage(Constants.ERR_OP_NOT_DEFINED_FOR_ZERO, Constants.LookupOperandAsString(Operand.DIV)));
		}
		else {
		integerSimplifyResult = leftVal / rightVal;
		}
		break;
		case Operand.MOD:
		if (rightVal == 0) {
		SemErr(Constants.BuildMessage(Constants.ERR_OP_NOT_DEFINED_FOR_ZERO, Constants.LookupOperandAsString(Operand.MOD)));
		}
		else {
		integerSimplifyResult = leftVal % rightVal;
		}
		break;
		case Operand.BIT_AND:
		integerSimplifyResult = leftVal & rightVal;
		break;
		case Operand.BIT_OR:
		integerSimplifyResult = leftVal | rightVal;
		break;
		case Operand.EQ:
		isResultBoolean = true;
		boolSimplifyResult = leftVal == rightVal;
		break;
		case Operand.NEQ:
		isResultBoolean = true;
		boolSimplifyResult = leftVal != rightVal;
		break;
		case Operand.GREATER:
		isResultBoolean = true;
		boolSimplifyResult = leftVal > rightVal;
		break;
		case Operand.GREATER_EQ:
		isResultBoolean = true;
		boolSimplifyResult = leftVal >= rightVal;
		break;
		case Operand.LESS:
		isResultBoolean = true;
		boolSimplifyResult = leftVal < rightVal;
		break;
		case Operand.LESS_EQ:
		isResultBoolean = true;
		boolSimplifyResult = leftVal <= rightVal;
		break;
		}
		Literal constantLiteral;
		if (isResultBoolean) {
		constantLiteral = new Literal(boolSimplifyResult.ToString(), LiteralValueType.BOOL, boolSimplifyResult);
		}
		else {
		constantLiteral = new Literal(integerSimplifyResult.ToString(), LiteralValueType.INTEGER, integerSimplifyResult);
		}
		headExpr = new Expression(Operand.NONE){
		Literal = constantLiteral,
		HasNumericResult = !isResultBoolean
		};
		return;
		}
		}
		headExpr.LeftExpression = lExpr; 
		headExpr.RightExpression = rExpr;
		headExpr.Operand = bOperand;
		
	}

	void shift_expression(in Expression exprToShift, out Expression shiftExpr) {
		Operand shiftOperand = Operand.SHIFT_LEFT; Expression shiftAmountExpr;
		
		if (la.kind == 29) {
			Get();
		} else if (la.kind == 30) {
			Get();
			shiftOperand = Operand.SHIFT_RIGHT; 
		} else SynErr(59);
		if (!exprToShift.HasNumericResult) {
		SemErr(Constants.BuildMessage(Constants.ERR_TYPE_MISSMATCH, new object[2] { shiftOperand, "NUMERIC" }));
		}
		shiftExpr = new Expression(shiftOperand);
		
		Expect(2);
		shiftAmountExpr = GetValueForVariable();
		int shiftAmount = 0;
		if (shiftAmountExpr == null) {
		SemErr(Constants.ERR_NOT_INTEGER);
		shiftExpr = new Expression(Operand.SKIP);
		}
		else {
		shiftAmount = (int) shiftAmountExpr.Literal.Value;
		}
		/*
		if (t.val.StartsWith('$') || t.val.StartsWith('#')){
		Expression scopeExpr = _currScope.Search(t.val);
		if (scopeExpr == null){
		SemErr(Constants.BuildMessage(Constants.ERR_ITEM_NOT_IN_SCOPE, t.val));
		}
		else {
		shiftExpr.LeftExpression = exprToShift;
		shiftExpr.RightExpression = scopeExpr;
		shiftExpr.Operand = shiftOperand;
		}
		}
		int shiftAmount = 0;
		if (int.TryParse(t.val, Constants.INTEGER_FORMAT, null, out shiftAmount)) {
		if (shiftAmount == 0) {
		shiftExpr = new Expression(Operand.SKIP);
		}
		else {
		Literal shiftAmountLiteral = new Literal(shiftAmount.ToString(), LiteralValueType.INTEGER, shiftAmount);
		Expression shiftAmountExpr = new Expression(Operand.NONE);
		shiftAmountExpr.Literal = shiftAmountLiteral;
		shiftExpr.LeftExpression = exprToShift;
		shiftExpr.RightExpression = shiftAmountExpr;
		shiftExpr.Operand = shiftOperand;
		}
		}
		else {
		SemErr(Constants.ERR_NOT_INTEGER);
		}
		*/
		
		if (exprToShift.Operand == Operand.NONE && exprToShift.Literal.Value != null) {
		int exprToShiftValue = 0;
		if (!int.TryParse(t.val, Constants.INTEGER_FORMAT, null, out exprToShiftValue)){
		SemErr(Constants.ERR_NOT_INTEGER);
		}
		else {
		if (shiftOperand == Operand.SHIFT_LEFT){
		exprToShift.Literal.Value = exprToShiftValue << shiftAmount;
		}
		else {
		exprToShift.Literal.Value = exprToShiftValue >> shiftAmount;
		}													
		shiftExpr = new Expression(Operand.NONE) {
		Literal = exprToShift.Literal
		};
		}	
		}
		
		Expect(26);
	}

	void unary_expression(out Expression headExpr) {
		Expression unaryExpr = new Expression();
		Operand negOperand = Operand.NEG; 
		
		if (la.kind == 27) {
			Get();
		} else if (la.kind == 28) {
			Get();
			negOperand = Operand.BIT_NEG; 
		} else SynErr(60);
		headExpr = new Expression(negOperand); 
		expression(out unaryExpr);
		if ((headExpr.RequiresBooleanOperands & unaryExpr.HasNumericResult)) {
		SemErr(Constants.BuildMessage(Constants.ERR_TYPE_MISSMATCH, new object[2] { headExpr.Operand, "BOOLEAN" }));
		}
		else if (!(headExpr.RequiresBooleanOperands | unaryExpr.HasNumericResult)) {
		SemErr(Constants.BuildMessage(Constants.ERR_TYPE_MISSMATCH, new object[2] { headExpr.Operand, "NUMERIC" }));			
		}
		else {
		headExpr.LeftExpression = unaryExpr;
		// Try to simplify result if possible
		if (unaryExpr.Operand == Operand.NONE && unaryExpr.Literal != null && unaryExpr.Literal.Value != null){
		headExpr = new Expression(Operand.NONE);
		
		if (unaryExpr.Literal.LiteralValueType == LiteralValueType.BOOL){
		bool negatedResult = !((bool) unaryExpr.Literal.Value);
		headExpr.Literal = new Literal(negatedResult.ToString(), unaryExpr.Literal.LiteralValueType, negatedResult);
		}
		else {
		int bitwiseNegationRes = (int) unaryExpr.Literal.Value;
		int informationBitCnt = GetBitWidthOfValue(bitwiseNegationRes);
		if (informationBitCnt != 0) {
		int mask = ~(int.MaxValue << informationBitCnt);
		unaryExpr.LeftExpression.Literal.Value = (~bitwiseNegationRes & mask);
		}
		else {
		unaryExpr.LeftExpression.Literal.Value = bitwiseNegationRes;
		}
		/*
		int bitwiseNegationRes = ~((int) unaryExpr.Literal.Value);
		*/
		headExpr.Literal = new Literal(bitwiseNegationRes.ToString(), unaryExpr.Literal.LiteralValueType, bitwiseNegationRes);
		}
		}
		}
		
	}

	void statement_list(int numRepetitions) {
		statement();
		Expect(31);
		while (StartOf(2)) {
			statement();
			Expect(31);
		}
		if (numRepetitions > 0) { 
		WriteToInputStream(CurrBlock(), numRepetitions); 
		} 
		
	}

	void statement() {
		Expression expr; Block block; 
		switch (la.kind) {
		case 32: case 33: {
			call_statement();
			break;
		}
		case 35: {
			for_statement(out block);
			break;
		}
		case 40: {
			if_statement(out block);
			if (block != null) { CurrBlock().Expressions.Add(null); CurrBlock().InnerBlocks.Add(block); } 
			break;
		}
		case 28: case 44: case 45: {
			unary_statement(out expr);
			CurrBlock().Expressions.Add(expr);	
			break;
		}
		case 1: {
			assign_statement(out expr);
			CurrBlock().Expressions.Add(expr);	
			break;
		}
		case 47: {
			skip_statement(out expr);
			CurrBlock().Expressions.Add(expr);	
			break;
		}
		case 48: {
			tmp_loop_end();
			
			break;
		}
		default: SynErr(61); break;
		}
	}

	void call_statement() {
		if (la.kind == 32) {
			Get();
		} else if (la.kind == 33) {
			Get();
		} else SynErr(62);
		Expect(1);
		Expect(8);
		Expect(1);
		while (la.kind == 34) {
			Get();
			Expect(1);
		}
		Expect(26);
	}

	void for_statement(out Block loopBlock) {
		loopBlock = new Block(BlockType.TEMPORARY); 
		string loopVarName = Constants.UNNAMED_LOOP_VAR_PREFIX + (_unnamedLoopVarCnt++);
		int startValue = 0, endValue = 0, stepSize = 1; bool positiveIncrement = true;
		Expression numberExpr;
		
		Expect(35);
		if (la.kind == 2) {
			if (IsLoopVarPrefix()) {
				Expect(2);
				if (!t.val.StartsWith('$')){
				SemErr(Constants.ERR_LOOP_VAR_PREFIX);
				}
				else if (_currLoopVarSet.Contains(t.val)){
				SemErr(Constants.BuildMessage(Constants.ERR_DUPLICATE_LOOP_VAR, t.val));
				}
				else {
				//loopVarName = t.val.Substring(1, t.val.Length-1);
				loopVarName = t.val;
				_currLoopVarSet.Add(loopVarName);
				}
				
				Expect(22);
			}
			Expect(2);
			numberExpr = GetValueForVariable();
			if (numberExpr == null || numberExpr.Literal == null || numberExpr.Literal.Value == null) {
			SemErr(Constants.ERR_NOT_NUMBER);
			}
			else {									
			try
			{
			startValue = Convert.ToInt32(numberExpr.Literal.Value, null);
			}
			catch (InvalidCastException) { SemErr(Constants.ERR_NOT_INTEGER); }
			catch (FormatException) { SemErr(Constants.ERR_NOT_INTEGER); }
			catch (OverflowException) { SemErr(Constants.ERR_NOT_INTEGER); }
			}
			
			Expect(36);
		}
		Expect(2);
		numberExpr = GetValueForVariable();
		if (numberExpr == null) {
		SemErr(Constants.ERR_NOT_NUMBER);
		}
		else {
		try
		{
		endValue = Convert.ToInt32(numberExpr.Literal.Value, null);
		}
		catch (InvalidCastException) { SemErr(Constants.ERR_NOT_INTEGER); }
		catch (FormatException) { SemErr(Constants.ERR_NOT_INTEGER); }
		catch (OverflowException) { SemErr(Constants.ERR_NOT_INTEGER); }
		}
		
		if (la.kind == 37) {
			Get();
			Expect(7);
			if (la.kind == 10) {
				Get();
				positiveIncrement = false; 
			}
			Expect(2);
			numberExpr = GetValueForVariable();
			if (numberExpr == null) {
			SemErr(Constants.ERR_NOT_NUMBER);
			}
			else {									
			try
			{
			stepSize = Convert.ToInt32(numberExpr.Literal.Value, null);
			}
			catch (InvalidCastException) { SemErr(Constants.ERR_NOT_INTEGER); }
			catch (FormatException) { SemErr(Constants.ERR_NOT_INTEGER); }
			catch (OverflowException) { SemErr(Constants.ERR_NOT_INTEGER); }
			}
			if (stepSize == 0){
			SemErr(Constants.ERR_STEP_SIZE_ZERO);
			}
			
		}
		if (positiveIncrement && endValue < startValue){
		SemErr(Constants.BuildMessage(Constants.ERR_LOOP_RANGE_INVALID, new object[2] {startValue, endValue}));
		stepSize = 0;
		}
		else if (!positiveIncrement && startValue < endValue) {
		SemErr(Constants.BuildMessage(Constants.ERR_LOOP_RANGE_INVALID, new object[2] {startValue, endValue}));
		stepSize = 0;
		}
		Literal loopVariable = new Literal(loopVarName, LiteralValueType.INTEGER, null) {
		LiteralType = LiteralType.LOOP_VARIABLE,
		StartValue = startValue,
		Value = startValue,
		EndValue = endValue,
		StepSize = stepSize,
		PositiveStepSize = positiveIncrement
		};
		loopBlock.LoopVariable = loopVariable;
		_blockStack.Push(loopBlock);
		
		Expect(38);
		int numIterations = 0;
		if (stepSize > 0) {
		numIterations = (positiveIncrement ? (endValue - startValue) : (startValue - endValue)) / stepSize; 
		}
		_currScope.AddItem(loopVariable);
		/*
		if (numIterations == 0) {
		_currScope.Remove(loopVariable);
		_blockStack.Pop();
		return;
		}
		*/
		
		statement_list(numIterations);
		_loopVarStack.Push(loopVarName);
		_blockStack.Pop();
		_currLoopVarSet.Remove(loopVarName);
		
		if (numIterations > 0) { statement_list(0); } 
		
		Expect(39);
		_currScope.Remove(loopVariable); loopBlock = null;
	}

	void if_statement(out Block ifBlock) {
		ifBlock = new Block(BlockType.CONDITION); Expression guardExpr = null; 
		Expression otherExpr = null; bool guardExprConstant = false;
		bool constGuardExprValue = false;
		_blockStack.Push(ifBlock);
		
		Expect(40);
		expression(out guardExpr);
		if (guardExpr.Operand == Operand.NONE && guardExpr.Literal.Value != null){
		guardExprConstant = true;
		if (guardExpr.Literal.LiteralValueType == LiteralValueType.BOOL) {
			constGuardExprValue = (bool) guardExpr.Literal.Value;
		}
		else {
			constGuardExprValue = ((int) guardExpr.Literal.Value) > 0;
		}
		}
		else {
		ifBlock.GuardExpr = guardExpr;
		}
		
		Expect(41);
		statement_list(0);
		Expect(42);
		_blockStack.Push(new Block(BlockType.TEMPORARY)); 
		
		statement_list(0);
		Expect(43);
		expression(out otherExpr);
		//ifBlock.OtherExpr = otherExpr; 
		ifBlock.OtherExpr = guardExpr;
		// Pop temporary else block
		AppendBlockToParent(_blockStack.Pop(), ref ifBlock, true);
		//ifBlock.ElseExpressions = _blockStack.Pop().Expressions;
		
		// Pop if block
		_blockStack.Pop();
		if (guardExprConstant) {
		Block parentBlock = _blockStack.Peek();
		if (parentBlock == null) return;
		IEnumerator<Block> ifInnerBlocks = ifBlock.InnerBlocks.GetEnumerator();
		foreach (var expr in (constGuardExprValue ? ifBlock.Expressions : ifBlock.ElseExpressions)){
		if (expr != null) {
		parentBlock.Expressions.Add(expr);
		}
		else if (ifInnerBlocks.MoveNext()) {
		parentBlock.Expressions.Add(null);
		parentBlock.InnerBlocks.Add(ifInnerBlocks.Current);
		}
		}
		ifBlock = null;
		}
		/* TODO:
		if (!guardExpr.Equals(otherExpr)){
		SemErr(Constants.BuildMessage(Constants.ERR_IF_GUARDS_NOT_MATCHING, new object[2] {guardExpr.ToString(), otherExpr.ToString()} ));
		}
		*/
		
	}

	void unary_statement(out Expression unaryAssignExpr) {
		Literal unaryLiteral = null; Operand unaryOperand = Operand.NONE; 
		Expression unaryExpr = null;
		
		if (la.kind == 28) {
			Get();
			unaryOperand = Operand.NEG_UNARY_EQ; 
		} else if (la.kind == 44) {
			Get();
			unaryOperand = Operand.INCR_EQ; 
		} else if (la.kind == 45) {
			Get();
			unaryOperand = Operand.DECR_EQ; 
		} else SynErr(63);
		Expect(22);
		signal(out unaryLiteral, false);
		unaryExpr = new Expression(Operand.NONE) {
		Literal = unaryLiteral
		};
		if (unaryLiteral.LiteralValueType == LiteralValueType.BOOL){
		SemErr(Constants.BuildMessage(Constants.ERR_TYPE_MISSMATCH, new object[2]{unaryOperand, "NUMERIC"}));
		}
		unaryAssignExpr = new Expression(unaryOperand){
		LeftExpression = unaryExpr
		};
		if (unaryExpr.Literal.Value != null) {
		int literalVal = -1;
		bool validValue = false;				
		try
		{
		literalVal = Convert.ToInt32(unaryExpr.Literal.Value, null);
		validValue = true;
		}
		catch (InvalidCastException) { SemErr(Constants.ERR_NOT_INTEGER); }
		catch (FormatException) { SemErr(Constants.ERR_NOT_INTEGER); }
		catch (OverflowException) { SemErr(Constants.ERR_NOT_INTEGER); }
		if (!validValue) return;
		
		if (unaryOperand == Operand.INCR_EQ) {
		unaryAssignExpr.LeftExpression.Literal.Value = ++literalVal;
		}
		else if (unaryOperand == Operand.DECR_EQ) {
		unaryAssignExpr.LeftExpression.Literal.Value = --literalVal;
		}
		else {
		int informationBitCnt = GetBitWidthOfValue(literalVal);
		if (informationBitCnt != 0) {
		int mask = ~(int.MaxValue << informationBitCnt);
		unaryAssignExpr.LeftExpression.Literal.Value = (~literalVal & mask);
		}
		else {
		unaryAssignExpr.LeftExpression.Literal.Value = literalVal;
		}
		//unaryAssignExpr.LeftExpression.Literal.Value = ~literalVal & ((~0)>>1);
		}
		_currScope.UpdateValue(unaryAssignExpr.LeftExpression.Literal);
		}
		
	}

	void assign_statement(out Expression assignExpr) {
		Expression rExpr = null; Expression lExpr; 
		Literal assignLiteral; Operand assignOperand = Operand.NONE;
		
		signal(out assignLiteral, false);
		lExpr = new Expression(Operand.NONE){
		Literal = assignLiteral
		};
		
		if (la.kind == 9 || la.kind == 10 || la.kind == 11) {
			if (la.kind == 11) {
				Get();
				assignOperand = Operand.XOR_EQ; 
			} else if (la.kind == 9) {
				Get();
				assignOperand = Operand.ADD_EQ; 
			} else {
				Get();
				assignOperand = Operand.SUB_EQ; 
			}
			Expect(22);
		} else if (la.kind == 46) {
			Get();
			assignOperand = Operand.SWAP; 
		} else SynErr(64);
		expression(out rExpr);
		if (!rExpr.HasNumericResult){
		SemErr(Constants.BuildMessage(Constants.ERR_TYPE_MISSMATCH, new object[2]{assignOperand, "NUMERIC"}));
		}
		assignExpr = new Expression(assignOperand){
		LeftExpression = lExpr,
		RightExpression = rExpr
		};
		
		// Could add type checks if lefthand side is RANGE literal, e.g. if range defined by expression on right hand side is not greater than on the left
		// TODO: Update current entry fo signal on left side with value of right side
		if (rExpr.Operand == Operand.NONE && rExpr.Literal != null && rExpr.Literal.Value != null
		&& assignLiteral.Value != null) {
		int assignLiteralVal = 0;
		int rExprLiteralVal = 0;
		bool validValues = false;
		
		try
		{
		assignLiteralVal = Convert.ToInt32(assignLiteral.Value, null);
		rExprLiteralVal = Convert.ToInt32(rExpr.Literal.Value, null);
		validValues = true;
		}
		catch (InvalidCastException) { SemErr(Constants.ERR_NOT_INTEGER); }
		catch (FormatException) { SemErr(Constants.ERR_NOT_INTEGER); }
		catch (OverflowException) { SemErr(Constants.ERR_NOT_INTEGER); }
		if (!validValues) return;
		
		if (assignOperand == Operand.ADD_EQ){
		assignLiteralVal += rExprLiteralVal;
		}
		else if (assignOperand == Operand.SUB_EQ) {
		assignLiteralVal -= rExprLiteralVal;
		}
		else {
		assignLiteralVal ^= rExprLiteralVal;
		}
		assignExpr.LeftExpression.Literal.Value = assignLiteralVal;
		_currScope.UpdateValue(assignExpr.LeftExpression.Literal);
		}
		
	}

	void skip_statement(out Expression skipExpr) {
		Expect(47);
		skipExpr = new Expression(Operand.SKIP); 
	}

	void tmp_loop_end() {
		Expect(48);
		string loopVarName = CurrLoopVar();
		if (!string.IsNullOrWhiteSpace(loopVarName)){//&&!IsSubstitutionDelayed()){
		_currScope.IncrementLoopVariable(loopVarName); 
		}
		
	}

	void revComp() {
		program();
	}

	void program() {
		_currScope = new Scope();
		definedModules = new List<Module>();
		Module currModule; 
		_definedModules = new Dictionary<string, IList<Module>>();
		
		module(out currModule);
		if (currModule != null) {
		currModule.Scope = _currScope;
		definedModules.Add(currModule);
		}
		//_currScope = _currScope.Close();
		_currScope = new Scope();
		
		while (la.kind == 49) {
			_currScope = new Scope();	
			module(out currModule);
			if (currModule != null) {
			currModule.Scope = _currScope;
			definedModules.Add(currModule);
			}
			//_currScope = _currScope.Close();
			
		}
	}

	void module(out Module currModule) {
		IList<Parameter> parameters = new List<Parameter>();
		IList<Parameter> additionalParams = new List<Parameter>();
		Block moduleBlock = new Block(BlockType.MODULE);
		
		Expect(49);
		Expect(1);
		currModule = new Module(t.val); 
		Expect(8);
		if (la.kind == 50 || la.kind == 51 || la.kind == 52) {
			parameter_list(out parameters);
		}
		Expect(26);
		foreach (var parameter in parameters) {
		switch (parameter.ParameterType) {
		case ParameterType.IN:
		currModule.FormalInputParameter.Add(parameter);
		break;
		case ParameterType.OUT:
		for (int i = 0; i < parameter.Literal.Dimension; i++) {
		parameter.Literal.Values[i] = 0;
		}
		currModule.FormalOutputParameter.Add(parameter);
		break;
		case ParameterType.INOUT:
		currModule.FormalInputParameter.Add(parameter);
		currModule.FormalOutputParameter.Add(parameter);
		break;
		default:
		// TODO: Only allow parameters of type IN, OUT, INOUT
		throw new Exception();
		}
		parameter.Literal.LiteralType = LiteralType.PARAMETER;
		_currScope.AddItem(parameter.Literal);
		}
		
		while (la.kind == 53 || la.kind == 54) {
			ISet<string> currParamSet = parameters.Select(x => x.Name).ToHashSet(); 
			signal_list(ref currParamSet, ref additionalParams);
		}
		foreach (var additionalParam in additionalParams) {
		if (additionalParam.ParameterType == ParameterType.WIRE) {
		for (int i = 0; i < additionalParam.Literal.Dimension; i++) {
		additionalParam.Literal.Values[i] = 0;	
		}
		}
		additionalParam.Literal.LiteralType = LiteralType.VARIABLE;
		currModule.AdditionalSignals.Add(additionalParam);
		_currScope.AddItem(additionalParam.Literal);
		}
		if (!_definedModules.ContainsKey(currModule.Name)) {
		_definedModules.Add(currModule.Name, new List<Module>());
		}
		if (_definedModules[currModule.Name].Contains(currModule)) {
		SemErr(Constants.BuildMessage(Constants.ERR_DUPLICATE_MODULE, currModule.Name));
		}
		_definedModules[currModule.Name].Add(currModule);
		_blockStack.Push(moduleBlock);
		
		statement_list(0);
		currModule.Body = _blockStack.Pop(); 
	}

	void parameter_list(out IList<Parameter> paramList ) {
		paramList = new List<Parameter>(); 
		ISet<string> definedParams = new HashSet<string>();
		Parameter currParam;
		
		parameter(out currParam);
		if (definedParams.Contains(currParam.Name)){
		SemErr(Constants.BuildMessage(Constants.ERR_DUPLICATE_PARAMETER, currParam.Name));
		}
		else {
		definedParams.Add(currParam.Name);	
		paramList.Add(currParam);
		}
		
		while (la.kind == 34) {
			Get();
			parameter(out currParam);
			if (definedParams.Contains(currParam.Name)){
			SemErr(Constants.BuildMessage(Constants.ERR_DUPLICATE_PARAMETER, currParam.Name));
			}
			else {
			definedParams.Add(currParam.Name);	
			paramList.Add(currParam);
			}
			
		}
	}

	void signal_list(ref ISet<string> currParameterSet, ref IList<Parameter> otherParams ) {
		Parameter curr;
		ParameterType paramType = ParameterType.STATE;
		
		if (la.kind == 53) {
			Get();
			paramType = ParameterType.WIRE; 
		} else if (la.kind == 54) {
			Get();
		} else SynErr(65);
		signal_declaration(out curr);
		if (curr == null || currParameterSet.Contains(curr.Name)){
		SemErr(Constants.BuildMessage(Constants.ERR_DUPLICATE_PARAMETER, curr.Name));
		}
		else {
		curr.ParameterType = paramType;
		currParameterSet.Add(curr.Name);
		otherParams.Add(curr);
		}
		
		while (la.kind == 34) {
			Get();
			signal_declaration(out  curr);
			if (curr == null || currParameterSet.Contains(curr.Name)){
			SemErr(Constants.BuildMessage(Constants.ERR_DUPLICATE_PARAMETER, curr.Name));
			}
			else {
			curr.ParameterType = paramType;
			currParameterSet.Add(curr.Name);
			otherParams.Add(curr);
			}	
			
		}
		Expect(31);
	}

	void parameter(out Parameter param) {
		ParameterType type = ParameterType.IN; 
		if (la.kind == 50) {
			Get();
		} else if (la.kind == 51) {
			Get();
			type = ParameterType.OUT; 
		} else if (la.kind == 52) {
			Get();
			type = ParameterType.INOUT; 
		} else SynErr(66);
		signal_declaration(out param);
		param.ParameterType = type; 
	}

	void signal_declaration(out Parameter param) {
		Expect(1);
		param = new Parameter(t.val); 
		int signalWidth = Constants.DEFAULT_SIGNAL_WIDTH;
		int dimension = 1;
		param.Literal = new Literal(t.val, LiteralValueType.SIGNAL, null);
		
		while (la.kind == 4) {
			Get();
			Expect(2);
			param.Literal.IsArrayType = true; 
			if (!int.TryParse(t.val, Constants.INTEGER_FORMAT, null, out dimension)){
			SemErr(Constants.ERR_NOT_INTEGER);
			}
			else if (dimension == 0){
			SemErr(Constants.ERR_ARRAY_DIM_ZERO);
			}
			else {
			param.Literal.Values = new object[dimension];
			}
			
			Expect(5);
		}
		if (la.kind == 8) {
			Get();
			Expect(2);
			if (!int.TryParse(t.val, Constants.INTEGER_FORMAT, null, out signalWidth)){
			SemErr(Constants.ERR_NOT_INTEGER);
			}
			else if (signalWidth == 0) {
			SemErr(Constants.ERR_SIGNAL_WIDTH_ZERO);
			}
			
			Expect(26);
		}
		param.SetSignalProperties(dimension, signalWidth); 
	}



	public void Parse() {
		la = new Token();
		la.val = "";		
		Get();
		revComp();
		Expect(0);

	}
	
	static readonly bool[,] set = {
		{_T,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_x,_x,_x, _x,_x,_x,_x, _x,_T,_T,_T, _T,_T,_T,_T, _T,_T,_T,_T, _T,_T,_T,_T, _T,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x},
		{_x,_T,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _T,_x,_x,_x, _T,_T,_x,_T, _x,_x,_x,_x, _T,_x,_x,_x, _T,_T,_x,_T, _T,_x,_x,_x, _x,_x,_x,_x, _x}

	};
} // end Parser


public class Errors {
	public int count = 0;                                    // number of errors detected
	public System.IO.TextWriter errorStream = Console.Out;   // error messages go to this stream
	public string errMsgFormat = "-- line {0} col {1}: {2}"; // 0=line, 1=column, 2=text

	public virtual void SynErr (int line, int col, int n) {
		string s;
		switch (n) {
			case 0: s = "EOF expected"; break;
			case 1: s = "ident expected"; break;
			case 2: s = "number expected"; break;
			case 3: s = "loopVarPF expected"; break;
			case 4: s = "\"[\" expected"; break;
			case 5: s = "\"]\" expected"; break;
			case 6: s = "\".\" expected"; break;
			case 7: s = "\":\" expected"; break;
			case 8: s = "\"(\" expected"; break;
			case 9: s = "\"+\" expected"; break;
			case 10: s = "\"-\" expected"; break;
			case 11: s = "\"^\" expected"; break;
			case 12: s = "\"*\" expected"; break;
			case 13: s = "\"/\" expected"; break;
			case 14: s = "\"%\" expected"; break;
			case 15: s = "\"*>\" expected"; break;
			case 16: s = "\"&&\" expected"; break;
			case 17: s = "\"||\" expected"; break;
			case 18: s = "\"&\" expected"; break;
			case 19: s = "\"|\" expected"; break;
			case 20: s = "\"<\" expected"; break;
			case 21: s = "\">\" expected"; break;
			case 22: s = "\"=\" expected"; break;
			case 23: s = "\"!=\" expected"; break;
			case 24: s = "\"<=\" expected"; break;
			case 25: s = "\">=\" expected"; break;
			case 26: s = "\")\" expected"; break;
			case 27: s = "\"!\" expected"; break;
			case 28: s = "\"~\" expected"; break;
			case 29: s = "\"<<\" expected"; break;
			case 30: s = "\">>\" expected"; break;
			case 31: s = "\";\" expected"; break;
			case 32: s = "\"call\" expected"; break;
			case 33: s = "\"uncall\" expected"; break;
			case 34: s = "\",\" expected"; break;
			case 35: s = "\"for\" expected"; break;
			case 36: s = "\"to\" expected"; break;
			case 37: s = "\"step\" expected"; break;
			case 38: s = "\"do\" expected"; break;
			case 39: s = "\"rof\" expected"; break;
			case 40: s = "\"if\" expected"; break;
			case 41: s = "\"then\" expected"; break;
			case 42: s = "\"else\" expected"; break;
			case 43: s = "\"fi\" expected"; break;
			case 44: s = "\"++\" expected"; break;
			case 45: s = "\"--\" expected"; break;
			case 46: s = "\"<=>\" expected"; break;
			case 47: s = "\"skip\" expected"; break;
			case 48: s = "\":endl:\" expected"; break;
			case 49: s = "\"module\" expected"; break;
			case 50: s = "\"in\" expected"; break;
			case 51: s = "\"out\" expected"; break;
			case 52: s = "\"inout\" expected"; break;
			case 53: s = "\"wire\" expected"; break;
			case 54: s = "\"state\" expected"; break;
			case 55: s = "??? expected"; break;
			case 56: s = "invalid expression"; break;
			case 57: s = "invalid expression"; break;
			case 58: s = "invalid binary_expression"; break;
			case 59: s = "invalid shift_expression"; break;
			case 60: s = "invalid unary_expression"; break;
			case 61: s = "invalid statement"; break;
			case 62: s = "invalid call_statement"; break;
			case 63: s = "invalid unary_statement"; break;
			case 64: s = "invalid assign_statement"; break;
			case 65: s = "invalid signal_list"; break;
			case 66: s = "invalid parameter"; break;

			default: s = "error " + n; break;
		}
		errorStream.WriteLine(errMsgFormat, line, col, s);
		count++;
	}

	public virtual void SemErr (int line, int col, string s) {
		errorStream.WriteLine(errMsgFormat, line, col, s);
		count++;
	}
	
	public virtual void SemErr (string s) {
		errorStream.WriteLine(s);
		count++;
	}
	
	public virtual void Warning (int line, int col, string s) {
		errorStream.WriteLine(errMsgFormat, line, col, s);
	}
	
	public virtual void Warning(string s) {
		errorStream.WriteLine(s);
	}
} // Errors


public class FatalError: Exception {
	public FatalError(string m): base(m) {}
}
