﻿using System;

namespace OptimizerGenerator
{
    public class QueryError
    {
        private int _line;
        private int _column;
        private string _msg;

        public int Line
        {
            get { return _line; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("QueryError cannot be set at a negative line");
                }
                _line = value;
            }
        }
        public int Column
        {
            get { return _column; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("QueryError cannot be set at a negative column");
                }
                _column = value;
            }
        }

        public string ErrorMsg
        {
            get { return _msg; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException("QueryError message cannot be null or empty !");
                }
                _msg = value;
            }
        }

        private readonly string _outputFormat = "-- line {0} col {1}: {2}";
        public QueryError(int line, int col, string errorMsg)
        {
            Line = line;
            Column = col;
            ErrorMsg = errorMsg;
        }

        public override string ToString()
        {
            return string.Format(_outputFormat, Line, Column, ErrorMsg);
        }
    }
}
