﻿using OptimizerGenerator;
using System;
using System.IO;

namespace Optimizer
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string[] lines = File.ReadAllLines(args[0], System.Text.Encoding.ASCII);
                QueryParser userQueryParser = new QueryParser();
                userQueryParser.ParseQuery(string.Join('\n', lines));
                if (userQueryParser.IsValidQuery)
                {
                    foreach (var module in userQueryParser.Modules)
                    {
                        Console.WriteLine(module.ToString());
                    }
                }
                else
                {
                    foreach (var error in userQueryParser.Errors)
                    {
                        Console.WriteLine(error.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("There was an error parsing the given model !");
                Console.WriteLine("Error [Reason/Message]: " + e.Message);
            }
        }
    }
}
